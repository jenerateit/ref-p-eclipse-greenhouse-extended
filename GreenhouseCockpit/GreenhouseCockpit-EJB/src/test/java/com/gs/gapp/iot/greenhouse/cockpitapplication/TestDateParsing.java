package com.gs.gapp.iot.greenhouse.cockpitapplication;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestDateParsing {

	public static void main(String[] args) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
        String sampleDate = "2015-09-01T13:34:50.288+02:00";
        
        try {
			Date date = sdf.parse(sampleDate);
			System.out.println("successfully parsed '" + date + "'");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

}
