package com.vd;


import java.lang.String;
import java.lang.Object;

public abstract class AbstractDto { // start of class

    /**
     * Keep the internal state of this object. In case one attribute of this dto get changed, the dirty flag should be set.
     */
    //DA-START:com.vd.AbstractDto.dirty:DA-START
    //DA-ELSE:com.vd.AbstractDto.dirty:DA-ELSE
    //DA-END:com.vd.AbstractDto.dirty:DA-END
    private boolean dirty;
    
    
    //DA-START:com.vd.AbstractDto.fields:DA-START
    //DA-ELSE:com.vd.AbstractDto.fields:DA-ELSE
    //DA-END:com.vd.AbstractDto.fields:DA-END
    /**
     * getter for the field dirty
     * 
     * Keep the internal state of this object. In case one attribute of this dto get changed, the dirty flag should be set.
     * 
     * @return
     */
    public boolean isDirty() {
        //DA-START:com.vd.AbstractDto.isDirty.boolean:DA-START
        //DA-ELSE:com.vd.AbstractDto.isDirty.boolean:DA-ELSE
        boolean result = this.dirty;
        return result;
        //DA-END:com.vd.AbstractDto.isDirty.boolean:DA-END
    }
    /**
     * setter for the field dirty
     * 
     * Keep the internal state of this object. In case one attribute of this dto get changed, the dirty flag should be set.
     * 
     * @param dirty  the dirty
     */
    public void setDirty(boolean dirty) {
        //DA-START:com.vd.AbstractDto.setDirty.boolean:DA-START
        //DA-ELSE:com.vd.AbstractDto.setDirty.boolean:DA-ELSE
        this.dirty = dirty;
        //DA-END:com.vd.AbstractDto.setDirty.boolean:DA-END
    }
    /**
     * 
     * @param originalValue  the originalValue
     * @param newValue  the newValue
     * @return
     */
    protected boolean isDifferent(boolean originalValue, boolean newValue) {
        //DA-START:com.vd.AbstractDto.isDifferent.boolean.boolean.boolean:DA-START
        //DA-ELSE:com.vd.AbstractDto.isDifferent.boolean.boolean.boolean:DA-ELSE
        return originalValue != newValue;
        //DA-END:com.vd.AbstractDto.isDifferent.boolean.boolean.boolean:DA-END
    }
    /**
     * 
     * @param originalValue  the originalValue
     * @param newValue  the newValue
     * @return
     */
    protected boolean isDifferent(String originalValue, String newValue) {
        //DA-START:com.vd.AbstractDto.isDifferent.String.String.boolean:DA-START
        //DA-ELSE:com.vd.AbstractDto.isDifferent.String.String.boolean:DA-ELSE
        return originalValue == null && newValue != null || originalValue != null && (newValue == null || !originalValue.equals(newValue));
        //DA-END:com.vd.AbstractDto.isDifferent.String.String.boolean:DA-END
    }
    /**
     * 
     * @param originalValue  the originalValue
     * @param newValue  the newValue
     * @return
     */
    protected boolean isDifferent(Object originalValue, Object newValue) {
        //DA-START:com.vd.AbstractDto.isDifferent.Object.Object.boolean:DA-START
        //DA-ELSE:com.vd.AbstractDto.isDifferent.Object.Object.boolean:DA-ELSE
        return originalValue == null && newValue != null || originalValue != null && (newValue == null || !originalValue.equals(newValue));
        //DA-END:com.vd.AbstractDto.isDifferent.Object.Object.boolean:DA-END
    }
    
    //DA-START:com.vd.AbstractDto.methods:DA-START
    //DA-ELSE:com.vd.AbstractDto.methods:DA-ELSE
    //DA-END:com.vd.AbstractDto.methods:DA-END
    
    //DA-START:com.vd.AbstractDto.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.AbstractDto.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.AbstractDto.additional.elements.in.type:DA-END
} // end of java type