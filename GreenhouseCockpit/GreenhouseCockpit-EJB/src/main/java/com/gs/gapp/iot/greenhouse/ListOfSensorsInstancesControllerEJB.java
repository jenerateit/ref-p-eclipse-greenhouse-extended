package com.gs.gapp.iot.greenhouse;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import com.vd.AbstractEJB;
import com.gs.gapp.iot.greenhouse.cockpitapplication.ListOfSensorsInstancesControllerDto;
import com.vd.SessionDataHolder;
import com.gs.gapp.iot.greenhouse.cockpitapplication.ListOfSensorsInstancesControllerDto.ListOfSensorsInstancesDto;
import java.util.Collection;

/**
 * This list displays all sensor instances.
 * 
 */
//DA-START:greenhouse.ListOfSensorsInstancesControllerEJB:DA-START
//DA-ELSE:greenhouse.ListOfSensorsInstancesControllerEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:greenhouse.ListOfSensorsInstancesControllerEJB:DA-END

public class ListOfSensorsInstancesControllerEJB extends AbstractEJB { // start of class

    /**
     * creates an instance of ListOfSensorsInstancesControllerEJB
     */
    public ListOfSensorsInstancesControllerEJB() {
        //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB:DA-ELSE
        super();
        //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB:DA-END
    }
    
    /**
     * load the initial data that a managed bean then uses to serve one or more xhtml pages to fill it with dynamic data
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.load.ListOfSensorsInstancesControllerDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.load.ListOfSensorsInstancesControllerDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.load.ListOfSensorsInstancesControllerDto.SessionDataHolder.annotations:DA-END
    public ListOfSensorsInstancesControllerDto load(ListOfSensorsInstancesControllerDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.load.ListOfSensorsInstancesControllerDto.SessionDataHolder.ListOfSensorsInstancesControllerDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.load.ListOfSensorsInstancesControllerDto.SessionDataHolder.ListOfSensorsInstancesControllerDto:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.load.ListOfSensorsInstancesControllerDto.SessionDataHolder.ListOfSensorsInstancesControllerDto:DA-END
    }
    /**
     * update the screen's main dto instance
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.update.ListOfSensorsInstancesControllerDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.update.ListOfSensorsInstancesControllerDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.update.ListOfSensorsInstancesControllerDto.SessionDataHolder.annotations:DA-END
    public ListOfSensorsInstancesControllerDto update(ListOfSensorsInstancesControllerDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.update.ListOfSensorsInstancesControllerDto.SessionDataHolder.ListOfSensorsInstancesControllerDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.update.ListOfSensorsInstancesControllerDto.SessionDataHolder.ListOfSensorsInstancesControllerDto:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.update.ListOfSensorsInstancesControllerDto.SessionDataHolder.ListOfSensorsInstancesControllerDto:DA-END
    }
    /**
     * update a single dto instance
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.updateListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.updateListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.updateListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.annotations:DA-END
    public ListOfSensorsInstancesControllerDto.ListOfSensorsInstancesDto updateListOfSensorsInstancesDto(ListOfSensorsInstancesControllerDto.ListOfSensorsInstancesDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.updateListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.ListOfSensorsInstancesDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.updateListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.ListOfSensorsInstancesDto:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.updateListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.ListOfSensorsInstancesDto:DA-END
    }
    /**
     * update a collection of dto instances
     * 
     * @param dtos  the dtos
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.updateListOfSensorsInstancesDto.Collection.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.updateListOfSensorsInstancesDto.Collection.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.updateListOfSensorsInstancesDto.Collection.SessionDataHolder.annotations:DA-END
    public Collection<ListOfSensorsInstancesControllerDto.ListOfSensorsInstancesDto> updateListOfSensorsInstancesDto(Collection<ListOfSensorsInstancesControllerDto.ListOfSensorsInstancesDto> dtos, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.updateListOfSensorsInstancesDto.Collection.SessionDataHolder.Collection:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.updateListOfSensorsInstancesDto.Collection.SessionDataHolder.Collection:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.updateListOfSensorsInstancesDto.Collection.SessionDataHolder.Collection:DA-END
    }
    /**
     * delete a single dto instance
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     */
    //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.deleteListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.deleteListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.deleteListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.annotations:DA-END
    public void deleteListOfSensorsInstancesDto(ListOfSensorsInstancesControllerDto.ListOfSensorsInstancesDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.deleteListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.deleteListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.deleteListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder:DA-END
    }
    /**
     * delete a collection of dto instances
     * 
     * @param dtos  the dtos
     * @param sessionDataHolder  the sessionDataHolder
     */
    //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.deleteListOfSensorsInstancesDto.Collection.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.deleteListOfSensorsInstancesDto.Collection.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.deleteListOfSensorsInstancesDto.Collection.SessionDataHolder.annotations:DA-END
    public void deleteListOfSensorsInstancesDto(Collection<ListOfSensorsInstancesControllerDto.ListOfSensorsInstancesDto> dtos, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.deleteListOfSensorsInstancesDto.Collection.SessionDataHolder:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.deleteListOfSensorsInstancesDto.Collection.SessionDataHolder:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.deleteListOfSensorsInstancesDto.Collection.SessionDataHolder:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB.additional.elements.in.type:DA-END
} // end of java type