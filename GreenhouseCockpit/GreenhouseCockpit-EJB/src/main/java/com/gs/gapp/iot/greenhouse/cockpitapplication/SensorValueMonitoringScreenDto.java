package com.gs.gapp.iot.greenhouse.cockpitapplication;


import com.vd.AbstractDto;

/**
 * Dto class for UIStructuralContainer ('layout' element) 'SensorValueMonitoringScreen' in module 'CockpitApplication'.
 * Support for dirty flag is added by the DTO generator.
 */
public class SensorValueMonitoringScreenDto extends AbstractDto { // start of class

    
    //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.SensorValueMonitoringScreenDto.fields:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.SensorValueMonitoringScreenDto.fields:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.SensorValueMonitoringScreenDto.fields:DA-END
    /**
     * creates an instance of SensorValueMonitoringScreenDto
     */
    public SensorValueMonitoringScreenDto() {
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.SensorValueMonitoringScreenDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.SensorValueMonitoringScreenDto:DA-ELSE
        super();
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.SensorValueMonitoringScreenDto:DA-END
    }
    
    /**
     * 
     * @return
     */
    public boolean isDirty() {
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.SensorValueMonitoringScreenDto.isDirty.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.SensorValueMonitoringScreenDto.isDirty.boolean:DA-ELSE
        boolean result = super.isDirty();
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.SensorValueMonitoringScreenDto.isDirty.boolean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.SensorValueMonitoringScreenDto.methods:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.SensorValueMonitoringScreenDto.methods:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.SensorValueMonitoringScreenDto.methods:DA-END
    
    //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.SensorValueMonitoringScreenDto.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.SensorValueMonitoringScreenDto.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.SensorValueMonitoringScreenDto.additional.elements.in.type:DA-END
} // end of java type