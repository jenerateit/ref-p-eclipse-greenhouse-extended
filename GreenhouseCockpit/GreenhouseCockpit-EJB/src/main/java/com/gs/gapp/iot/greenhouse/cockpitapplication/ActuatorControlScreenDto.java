package com.gs.gapp.iot.greenhouse.cockpitapplication;


import com.vd.AbstractDto;
import java.lang.String;

/**
 * Dto class for UIStructuralContainer ('layout' element) 'ActuatorControlScreen' in module 'CockpitApplication'.
 * Support for dirty flag is added by the DTO generator.
 */
public class ActuatorControlScreenDto extends AbstractDto { // start of class

    /**
     * final instance of the data container named 'ActuatorControl' in UI model 'CockpitApplication'
     */
    //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.actuatorControl:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.actuatorControl:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.actuatorControl:DA-END
    private final ActuatorControlScreenDto.ActuatorControlDto actuatorControl = new ActuatorControlScreenDto.ActuatorControlDto();
    
    
    //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.fields:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.fields:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.fields:DA-END
    /**
     * creates an instance of ActuatorControlScreenDto
     */
    public ActuatorControlScreenDto() {
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto:DA-ELSE
        super();
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto:DA-END
    }
    
    /**
     * getter for the field actuatorControl
     * 
     * final instance of the data container named 'ActuatorControl' in UI model 'CockpitApplication'
     * 
     * @return
     */
    public ActuatorControlScreenDto.ActuatorControlDto getActuatorControl() {
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.getActuatorControl.ActuatorControlDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.getActuatorControl.ActuatorControlDto:DA-ELSE
        return this.actuatorControl;
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.getActuatorControl.ActuatorControlDto:DA-END
    }
    /**
     * 
     * @return
     */
    public boolean isDirty() {
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.isDirty.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.isDirty.boolean:DA-ELSE
        boolean result = super.isDirty();
        result = result || actuatorControl.isDirty();
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.isDirty.boolean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.methods:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.methods:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.methods:DA-END
    
    /**
     * Private inner DTO class for UIDataContainer ('display' element) 'ActuatorControl' within UIStructuralContainer ('layout' element) 'ActuatorControlScreen' in module 'CockpitApplication'.
     * Support for dirty flag is added by the DTO generator.
     */
    public class ActuatorControlDto extends AbstractDto { // start of class
    
    
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.ipOrHostOfCoapServer:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.ipOrHostOfCoapServer:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.ipOrHostOfCoapServer:DA-END
        private String ipOrHostOfCoapServer;
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.lightStatus:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.lightStatus:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.lightStatus:DA-END
        private String lightStatus;
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.temperature:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.temperature:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.temperature:DA-END
        private String temperature;
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.humidity:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.humidity:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.humidity:DA-END
        private String humidity;
        
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.fields:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.fields:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.fields:DA-END
        
        /**
         * creates an instance of ActuatorControlDto
         */
        public ActuatorControlDto() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto:DA-ELSE
            super();
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto:DA-END
        }
        
        
        /**
         * setter for the field ipOrHostOfCoapServer
         * 
         * null
         * 
         * @param ipOrHostOfCoapServer  the ipOrHostOfCoapServer
         */
        public void setIpOrHostOfCoapServer(String ipOrHostOfCoapServer) {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.setIpOrHostOfCoapServer.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.setIpOrHostOfCoapServer.String:DA-ELSE
            if (isDifferent(this.ipOrHostOfCoapServer, ipOrHostOfCoapServer)) {
                this.setDirty(true);
                this.ipOrHostOfCoapServer = ipOrHostOfCoapServer;
            }
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.setIpOrHostOfCoapServer.String:DA-END
        }
        /**
         * getter for the field ipOrHostOfCoapServer
         * 
         * null
         * 
         * @return
         */
        public String getIpOrHostOfCoapServer() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.getIpOrHostOfCoapServer.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.getIpOrHostOfCoapServer.String:DA-ELSE
            return this.ipOrHostOfCoapServer;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.getIpOrHostOfCoapServer.String:DA-END
        }
        /**
         * setter for the field lightStatus
         * 
         * null
         * 
         * @param lightStatus  the lightStatus
         */
        public void setLightStatus(String lightStatus) {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.setLightStatus.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.setLightStatus.String:DA-ELSE
            if (isDifferent(this.lightStatus, lightStatus)) {
                this.setDirty(true);
                this.lightStatus = lightStatus;
            }
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.setLightStatus.String:DA-END
        }
        /**
         * getter for the field lightStatus
         * 
         * null
         * 
         * @return
         */
        public String getLightStatus() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.getLightStatus.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.getLightStatus.String:DA-ELSE
            return this.lightStatus;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.getLightStatus.String:DA-END
        }
        /**
         * setter for the field temperature
         * 
         * null
         * 
         * @param temperature  the temperature
         */
        public void setTemperature(String temperature) {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.setTemperature.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.setTemperature.String:DA-ELSE
            if (isDifferent(this.temperature, temperature)) {
                this.setDirty(true);
                this.temperature = temperature;
            }
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.setTemperature.String:DA-END
        }
        /**
         * getter for the field temperature
         * 
         * null
         * 
         * @return
         */
        public String getTemperature() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.getTemperature.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.getTemperature.String:DA-ELSE
            return this.temperature;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.getTemperature.String:DA-END
        }
        /**
         * setter for the field humidity
         * 
         * null
         * 
         * @param humidity  the humidity
         */
        public void setHumidity(String humidity) {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.setHumidity.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.setHumidity.String:DA-ELSE
            if (isDifferent(this.humidity, humidity)) {
                this.setDirty(true);
                this.humidity = humidity;
            }
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.setHumidity.String:DA-END
        }
        /**
         * getter for the field humidity
         * 
         * null
         * 
         * @return
         */
        public String getHumidity() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.getHumidity.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.getHumidity.String:DA-ELSE
            return this.humidity;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.getHumidity.String:DA-END
        }
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.methods:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.methods:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.methods:DA-END
        
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.additional.elements.in.type:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.additional.elements.in.type:DA-END
} // end of java type