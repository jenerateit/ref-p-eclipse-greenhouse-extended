package com.gs.gapp.iot.greenhouse.cockpitapplication;


import com.vd.AbstractDto;
import java.util.List;
import java.util.ArrayList;
import java.lang.String;
import java.lang.Long;
import java.util.Date;

/**
 * Dto class for UIStructuralContainer ('layout' element) 'HistoryScreen' in module 'CockpitApplication'.
 * Support for dirty flag is added by the DTO generator.
 */
public class HistoryScreenDto extends AbstractDto { // start of class

    /**
     * final instance of the data container named 'SensorInstance' in UI model 'CockpitApplication'
     */
    //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.sensorInstance:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.sensorInstance:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.sensorInstance:DA-END
    private final HistoryScreenDto.SensorInstanceDto sensorInstance = new HistoryScreenDto.SensorInstanceDto();
    
    /**
     * final instance of the data container named 'ListOfSensorData' in UI model 'CockpitApplication'
     */
    //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.listOfSensorData:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.listOfSensorData:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.listOfSensorData:DA-END
    private final List<HistoryScreenDto.ListOfSensorDataDto> listOfSensorData = new ArrayList<HistoryScreenDto.ListOfSensorDataDto>();
    
    
    //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.fields:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.fields:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.fields:DA-END
    /**
     * creates an instance of HistoryScreenDto
     */
    public HistoryScreenDto() {
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto:DA-ELSE
        super();
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto:DA-END
    }
    
    /**
     * getter for the field sensorInstance
     * 
     * final instance of the data container named 'SensorInstance' in UI model 'CockpitApplication'
     * 
     * @return
     */
    public HistoryScreenDto.SensorInstanceDto getSensorInstance() {
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.getSensorInstance.SensorInstanceDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.getSensorInstance.SensorInstanceDto:DA-ELSE
        return this.sensorInstance;
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.getSensorInstance.SensorInstanceDto:DA-END
    }
    /**
     * getter for the field listOfSensorData
     * 
     * final instance of the data container named 'ListOfSensorData' in UI model 'CockpitApplication'
     * 
     * @return
     */
    public List<HistoryScreenDto.ListOfSensorDataDto> getListOfSensorData() {
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.getListOfSensorData.List:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.getListOfSensorData.List:DA-ELSE
        return this.listOfSensorData;
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.getListOfSensorData.List:DA-END
    }
    /**
     * 
     * @return
     */
    public boolean isDirty() {
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.isDirty.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.isDirty.boolean:DA-ELSE
        boolean result = super.isDirty();
        result = result || sensorInstance.isDirty();
        for (AbstractDto dto : listOfSensorData) {
            result = result || dto.isDirty();
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.isDirty.boolean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.methods:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.methods:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.methods:DA-END
    
    /**
     * Private inner DTO class for UIDataContainer ('display' element) 'SensorInstance' within UIStructuralContainer ('layout' element) 'HistoryScreen' in module 'CockpitApplication'.
     * Support for dirty flag is added by the DTO generator.
     */
    public class SensorInstanceDto extends AbstractDto { // start of class
    
    
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.id:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.id:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.id:DA-END
        private String id;
        
        /**
         * PrimaryKey field for Entity 'BaseEntity' and Id 'pk'
         */
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.pkEntityPk:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.pkEntityPk:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.pkEntityPk:DA-END
        private Long pkEntityPk;
        
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.fields:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.fields:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.fields:DA-END
        
        /**
         * creates an instance of SensorInstanceDto
         */
        public SensorInstanceDto() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto:DA-ELSE
            super();
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto:DA-END
        }
        
        
        /**
         * setter for the field id
         * 
         * null
         * 
         * @param id  the id
         */
        public void setId(String id) {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.setId.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.setId.String:DA-ELSE
            if (isDifferent(this.id, id)) {
                this.setDirty(true);
                this.id = id;
            }
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.setId.String:DA-END
        }
        /**
         * getter for the field id
         * 
         * null
         * 
         * @return
         */
        public String getId() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.getId.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.getId.String:DA-ELSE
            return this.id;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.getId.String:DA-END
        }
        /**
         * getter for the field pkEntityPk
         * 
         * PrimaryKey field for Entity 'BaseEntity' and Id 'pk'
         * 
         * @return
         */
        public Long getPkEntityPk() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.getPkEntityPk.Long:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.getPkEntityPk.Long:DA-ELSE
            return this.pkEntityPk;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.getPkEntityPk.Long:DA-END
        }
        /**
         * setter for the field pkEntityPk
         * 
         * PrimaryKey field for Entity 'BaseEntity' and Id 'pk'
         * 
         * @param pkEntityPk  the pkEntityPk
         */
        public void setPkEntityPk(Long pkEntityPk) {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.setPkEntityPk.Long:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.setPkEntityPk.Long:DA-ELSE
            this.pkEntityPk = pkEntityPk;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.setPkEntityPk.Long:DA-END
        }
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.methods:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.methods:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.methods:DA-END
        
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.additional.elements.in.type:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto.additional.elements.in.type:DA-END
    } // end of java type
    
    /**
     * Private inner DTO class for UIDataContainer ('display' element) 'ListOfSensorData' within UIStructuralContainer ('layout' element) 'HistoryScreen' in module 'CockpitApplication'.
     * Support for dirty flag is added by the DTO generator.
     */
    public class ListOfSensorDataDto extends AbstractDto { // start of class
    
    
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.id:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.id:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.id:DA-END
        private String id;
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.timeOfMeasurement:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.timeOfMeasurement:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.timeOfMeasurement:DA-END
        private Date timeOfMeasurement;
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.actualValue:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.actualValue:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.actualValue:DA-END
        private String actualValue;
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.actualUnit:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.actualUnit:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.actualUnit:DA-END
        private String actualUnit;
        
        /**
         * PrimaryKey field for Entity 'BaseEntity' and Id 'pk'
         */
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.pkEntityPk:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.pkEntityPk:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.pkEntityPk:DA-END
        private Long pkEntityPk;
        
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.fields:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.fields:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.fields:DA-END
        
        /**
         * creates an instance of ListOfSensorDataDto
         */
        public ListOfSensorDataDto() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto:DA-ELSE
            super();
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto:DA-END
        }
        
        
        /**
         * setter for the field id
         * 
         * null
         * 
         * @param id  the id
         */
        public void setId(String id) {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.setId.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.setId.String:DA-ELSE
            if (isDifferent(this.id, id)) {
                this.setDirty(true);
                this.id = id;
            }
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.setId.String:DA-END
        }
        /**
         * getter for the field id
         * 
         * null
         * 
         * @return
         */
        public String getId() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.getId.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.getId.String:DA-ELSE
            return this.id;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.getId.String:DA-END
        }
        /**
         * setter for the field timeOfMeasurement
         * 
         * null
         * 
         * @param timeOfMeasurement  the timeOfMeasurement
         */
        public void setTimeOfMeasurement(Date timeOfMeasurement) {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.setTimeOfMeasurement.Date:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.setTimeOfMeasurement.Date:DA-ELSE
            if (isDifferent(this.timeOfMeasurement, timeOfMeasurement)) {
                this.setDirty(true);
                this.timeOfMeasurement = timeOfMeasurement;
            }
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.setTimeOfMeasurement.Date:DA-END
        }
        /**
         * getter for the field timeOfMeasurement
         * 
         * null
         * 
         * @return
         */
        public Date getTimeOfMeasurement() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.getTimeOfMeasurement.Date:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.getTimeOfMeasurement.Date:DA-ELSE
            return this.timeOfMeasurement;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.getTimeOfMeasurement.Date:DA-END
        }
        /**
         * setter for the field actualValue
         * 
         * null
         * 
         * @param actualValue  the actualValue
         */
        public void setActualValue(String actualValue) {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.setActualValue.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.setActualValue.String:DA-ELSE
            if (isDifferent(this.actualValue, actualValue)) {
                this.setDirty(true);
                this.actualValue = actualValue;
            }
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.setActualValue.String:DA-END
        }
        /**
         * getter for the field actualValue
         * 
         * null
         * 
         * @return
         */
        public String getActualValue() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.getActualValue.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.getActualValue.String:DA-ELSE
            return this.actualValue;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.getActualValue.String:DA-END
        }
        /**
         * setter for the field actualUnit
         * 
         * null
         * 
         * @param actualUnit  the actualUnit
         */
        public void setActualUnit(String actualUnit) {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.setActualUnit.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.setActualUnit.String:DA-ELSE
            if (isDifferent(this.actualUnit, actualUnit)) {
                this.setDirty(true);
                this.actualUnit = actualUnit;
            }
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.setActualUnit.String:DA-END
        }
        /**
         * getter for the field actualUnit
         * 
         * null
         * 
         * @return
         */
        public String getActualUnit() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.getActualUnit.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.getActualUnit.String:DA-ELSE
            return this.actualUnit;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.getActualUnit.String:DA-END
        }
        /**
         * getter for the field pkEntityPk
         * 
         * PrimaryKey field for Entity 'BaseEntity' and Id 'pk'
         * 
         * @return
         */
        public Long getPkEntityPk() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.getPkEntityPk.Long:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.getPkEntityPk.Long:DA-ELSE
            return this.pkEntityPk;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.getPkEntityPk.Long:DA-END
        }
        /**
         * setter for the field pkEntityPk
         * 
         * PrimaryKey field for Entity 'BaseEntity' and Id 'pk'
         * 
         * @param pkEntityPk  the pkEntityPk
         */
        public void setPkEntityPk(Long pkEntityPk) {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.setPkEntityPk.Long:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.setPkEntityPk.Long:DA-ELSE
            this.pkEntityPk = pkEntityPk;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.setPkEntityPk.Long:DA-END
        }
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.methods:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.methods:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.methods:DA-END
        
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.additional.elements.in.type:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.additional.elements.in.type:DA-END
} // end of java type