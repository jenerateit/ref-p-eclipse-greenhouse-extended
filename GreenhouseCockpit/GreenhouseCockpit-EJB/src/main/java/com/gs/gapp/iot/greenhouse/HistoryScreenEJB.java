package com.gs.gapp.iot.greenhouse;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import com.vd.AbstractEJB;
import com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto;
import com.vd.SessionDataHolder;
import com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.SensorInstanceDto;
import com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto.ListOfSensorDataDto;
import java.util.Collection;
/*
 * Imports from last generation
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gs.gapp.iot.greenhouse.basic.SensorDataBean;
import com.gs.gapp.iot.greenhouse.basic.SensorDataListBean;
import com.gs.gapp.iot.greenhouse.function.SensorDataService;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;


//DA-START:greenhouse.HistoryScreenEJB:DA-START
//DA-ELSE:greenhouse.HistoryScreenEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:greenhouse.HistoryScreenEJB:DA-END

public class HistoryScreenEJB extends AbstractEJB { // start of class

    /**
     * creates an instance of HistoryScreenEJB
     */
    public HistoryScreenEJB() {
        //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB:DA-ELSE
        super();
        //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB:DA-END
    }
    
    /**
     * load the initial data that a managed bean then uses to serve one or more xhtml pages to fill it with dynamic data
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.load.HistoryScreenDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.load.HistoryScreenDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.load.HistoryScreenDto.SessionDataHolder.annotations:DA-END
    public HistoryScreenDto load(HistoryScreenDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.load.HistoryScreenDto.SessionDataHolder.HistoryScreenDto:DA-START
    	HistoryScreenDto result = new HistoryScreenDto();
    	
    	Gson gson = new GsonBuilder()
    			.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX")
    			.create();
        
    	RestAdapter restAdapter = new RestAdapter.Builder()
    			.setLogLevel(RestAdapter.LogLevel.FULL)
    			.setEndpoint("http://localhost:8080/GreenhouseRestApi-JAX-RS/greenhouse/v1")
    			.setConverter(new GsonConverter(gson))
    			.build();
    	
    	SensorDataService sensorDataService = restAdapter.create(SensorDataService.class);
		SensorDataListBean sensorDataListBean = sensorDataService.getSensorData(dto.getSensorInstance().getPkEntityPk().toString());
    	
    	for (SensorDataBean sensorDataBean : sensorDataListBean.getSensorDataList()) {
    		ListOfSensorDataDto sensorDataDto = result.new ListOfSensorDataDto();
    		sensorDataDto.setPkEntityPk(sensorDataBean.getPk());
    		if (sensorDataBean.getSensorInstance() != null) {
	    		sensorDataDto.setId(sensorDataBean.getSensorInstance().getId());
    		}
    		sensorDataDto.setTimeOfMeasurement(sensorDataBean.getTimeOfMeasurement());
    		sensorDataDto.setActualValue(sensorDataBean.getValue());
    		sensorDataDto.setActualUnit(sensorDataBean.getUnit() == null ? "-" : sensorDataBean.getUnit().getName());
    		result.getListOfSensorData().add(sensorDataDto);
    	}
    	
    	if (result.getListOfSensorData().size() > 0) result.getSensorInstance().setId(result.getListOfSensorData().get(0).getId());
    	
    	return result;
        //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.load.HistoryScreenDto.SessionDataHolder.HistoryScreenDto:DA-ELSE
        //return null;
        //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.load.HistoryScreenDto.SessionDataHolder.HistoryScreenDto:DA-END
    }
    /**
     * update the screen's main dto instance
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.update.HistoryScreenDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.update.HistoryScreenDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.update.HistoryScreenDto.SessionDataHolder.annotations:DA-END
    public HistoryScreenDto update(HistoryScreenDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.update.HistoryScreenDto.SessionDataHolder.HistoryScreenDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.update.HistoryScreenDto.SessionDataHolder.HistoryScreenDto:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.update.HistoryScreenDto.SessionDataHolder.HistoryScreenDto:DA-END
    }
    /**
     * update a single dto instance
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateSensorInstanceDto.SensorInstanceDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateSensorInstanceDto.SensorInstanceDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateSensorInstanceDto.SensorInstanceDto.SessionDataHolder.annotations:DA-END
    public HistoryScreenDto.SensorInstanceDto updateSensorInstanceDto(HistoryScreenDto.SensorInstanceDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateSensorInstanceDto.SensorInstanceDto.SessionDataHolder.SensorInstanceDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateSensorInstanceDto.SensorInstanceDto.SessionDataHolder.SensorInstanceDto:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateSensorInstanceDto.SensorInstanceDto.SessionDataHolder.SensorInstanceDto:DA-END
    }
    /**
     * update a single dto instance
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateListOfSensorDataDto.ListOfSensorDataDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateListOfSensorDataDto.ListOfSensorDataDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateListOfSensorDataDto.ListOfSensorDataDto.SessionDataHolder.annotations:DA-END
    public HistoryScreenDto.ListOfSensorDataDto updateListOfSensorDataDto(HistoryScreenDto.ListOfSensorDataDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateListOfSensorDataDto.ListOfSensorDataDto.SessionDataHolder.ListOfSensorDataDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateListOfSensorDataDto.ListOfSensorDataDto.SessionDataHolder.ListOfSensorDataDto:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateListOfSensorDataDto.ListOfSensorDataDto.SessionDataHolder.ListOfSensorDataDto:DA-END
    }
    /**
     * update a collection of dto instances
     * 
     * @param dtos  the dtos
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateSensorInstanceDto.Collection.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateSensorInstanceDto.Collection.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateSensorInstanceDto.Collection.SessionDataHolder.annotations:DA-END
    public Collection<HistoryScreenDto.SensorInstanceDto> updateSensorInstanceDto(Collection<HistoryScreenDto.SensorInstanceDto> dtos, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateSensorInstanceDto.Collection.SessionDataHolder.Collection:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateSensorInstanceDto.Collection.SessionDataHolder.Collection:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateSensorInstanceDto.Collection.SessionDataHolder.Collection:DA-END
    }
    /**
     * update a collection of dto instances
     * 
     * @param dtos  the dtos
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateListOfSensorDataDto.Collection.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateListOfSensorDataDto.Collection.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateListOfSensorDataDto.Collection.SessionDataHolder.annotations:DA-END
    public Collection<HistoryScreenDto.ListOfSensorDataDto> updateListOfSensorDataDto(Collection<HistoryScreenDto.ListOfSensorDataDto> dtos, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateListOfSensorDataDto.Collection.SessionDataHolder.Collection:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateListOfSensorDataDto.Collection.SessionDataHolder.Collection:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.updateListOfSensorDataDto.Collection.SessionDataHolder.Collection:DA-END
    }
    /**
     * delete a single dto instance
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     */
    //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteSensorInstanceDto.SensorInstanceDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteSensorInstanceDto.SensorInstanceDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteSensorInstanceDto.SensorInstanceDto.SessionDataHolder.annotations:DA-END
    public void deleteSensorInstanceDto(HistoryScreenDto.SensorInstanceDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteSensorInstanceDto.SensorInstanceDto.SessionDataHolder:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteSensorInstanceDto.SensorInstanceDto.SessionDataHolder:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteSensorInstanceDto.SensorInstanceDto.SessionDataHolder:DA-END
    }
    /**
     * delete a single dto instance
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     */
    //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteListOfSensorDataDto.ListOfSensorDataDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteListOfSensorDataDto.ListOfSensorDataDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteListOfSensorDataDto.ListOfSensorDataDto.SessionDataHolder.annotations:DA-END
    public void deleteListOfSensorDataDto(HistoryScreenDto.ListOfSensorDataDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteListOfSensorDataDto.ListOfSensorDataDto.SessionDataHolder:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteListOfSensorDataDto.ListOfSensorDataDto.SessionDataHolder:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteListOfSensorDataDto.ListOfSensorDataDto.SessionDataHolder:DA-END
    }
    /**
     * delete a collection of dto instances
     * 
     * @param dtos  the dtos
     * @param sessionDataHolder  the sessionDataHolder
     */
    //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteSensorInstanceDto.Collection.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteSensorInstanceDto.Collection.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteSensorInstanceDto.Collection.SessionDataHolder.annotations:DA-END
    public void deleteSensorInstanceDto(Collection<HistoryScreenDto.SensorInstanceDto> dtos, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteSensorInstanceDto.Collection.SessionDataHolder:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteSensorInstanceDto.Collection.SessionDataHolder:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteSensorInstanceDto.Collection.SessionDataHolder:DA-END
    }
    /**
     * delete a collection of dto instances
     * 
     * @param dtos  the dtos
     * @param sessionDataHolder  the sessionDataHolder
     */
    //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteListOfSensorDataDto.Collection.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteListOfSensorDataDto.Collection.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteListOfSensorDataDto.Collection.SessionDataHolder.annotations:DA-END
    public void deleteListOfSensorDataDto(Collection<HistoryScreenDto.ListOfSensorDataDto> dtos, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteListOfSensorDataDto.Collection.SessionDataHolder:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteListOfSensorDataDto.Collection.SessionDataHolder:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.deleteListOfSensorDataDto.Collection.SessionDataHolder:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenEJB.additional.elements.in.type:DA-END
} // end of java type