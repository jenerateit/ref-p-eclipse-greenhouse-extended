package com.gs.gapp.iot.greenhouse.cockpitapplication;


import com.vd.AbstractDto;
import java.util.List;
import java.util.ArrayList;
import java.lang.String;
import java.lang.Long;

/**
 * Dto class for UIStructuralContainer ('layout' element) 'MainScreen' in module 'CockpitApplication'.
 * Support for dirty flag is added by the DTO generator.
 */
public class MainScreenDto extends AbstractDto { // start of class

    /**
     * final instance of the data container named 'ListOfSensorsInstances' in UI model 'CockpitApplication'
     */
    //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.listOfSensorsInstances:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.listOfSensorsInstances:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.listOfSensorsInstances:DA-END
    private final List<MainScreenDto.ListOfSensorsInstancesDto> listOfSensorsInstances = new ArrayList<MainScreenDto.ListOfSensorsInstancesDto>();
    
    
    //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.fields:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.fields:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.fields:DA-END
    /**
     * creates an instance of MainScreenDto
     */
    public MainScreenDto() {
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto:DA-ELSE
        super();
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto:DA-END
    }
    
    /**
     * getter for the field listOfSensorsInstances
     * 
     * final instance of the data container named 'ListOfSensorsInstances' in UI model 'CockpitApplication'
     * 
     * @return
     */
    public List<MainScreenDto.ListOfSensorsInstancesDto> getListOfSensorsInstances() {
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.getListOfSensorsInstances.List:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.getListOfSensorsInstances.List:DA-ELSE
        return this.listOfSensorsInstances;
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.getListOfSensorsInstances.List:DA-END
    }
    /**
     * 
     * @return
     */
    public boolean isDirty() {
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.isDirty.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.isDirty.boolean:DA-ELSE
        boolean result = super.isDirty();
        for (AbstractDto dto : listOfSensorsInstances) {
            result = result || dto.isDirty();
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.isDirty.boolean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.methods:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.methods:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.methods:DA-END
    
    /**
     * Private inner DTO class for UIDataContainer ('display' element) 'ListOfSensorsInstances' within UIStructuralContainer ('layout' element) 'MainScreen' in module 'CockpitApplication'.
     * Support for dirty flag is added by the DTO generator.
     */
    public class ListOfSensorsInstancesDto extends AbstractDto { // start of class
    
    
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.id:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.id:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.id:DA-END
        private String id;
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.actualValue:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.actualValue:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.actualValue:DA-END
        private String actualValue;
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.actualUnit:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.actualUnit:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.actualUnit:DA-END
        private String actualUnit;
        
        /**
         * PrimaryKey field for Entity 'BaseEntity' and Id 'pk'
         */
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.pkEntityPk:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.pkEntityPk:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.pkEntityPk:DA-END
        private Long pkEntityPk;
        
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.fields:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.fields:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.fields:DA-END
        
        /**
         * creates an instance of ListOfSensorsInstancesDto
         */
        public ListOfSensorsInstancesDto() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto:DA-ELSE
            super();
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto:DA-END
        }
        
        
        /**
         * setter for the field id
         * 
         * null
         * 
         * @param id  the id
         */
        public void setId(String id) {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.setId.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.setId.String:DA-ELSE
            if (isDifferent(this.id, id)) {
                this.setDirty(true);
                this.id = id;
            }
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.setId.String:DA-END
        }
        /**
         * getter for the field id
         * 
         * null
         * 
         * @return
         */
        public String getId() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.getId.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.getId.String:DA-ELSE
            return this.id;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.getId.String:DA-END
        }
        /**
         * setter for the field actualValue
         * 
         * null
         * 
         * @param actualValue  the actualValue
         */
        public void setActualValue(String actualValue) {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.setActualValue.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.setActualValue.String:DA-ELSE
            if (isDifferent(this.actualValue, actualValue)) {
                this.setDirty(true);
                this.actualValue = actualValue;
            }
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.setActualValue.String:DA-END
        }
        /**
         * getter for the field actualValue
         * 
         * null
         * 
         * @return
         */
        public String getActualValue() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.getActualValue.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.getActualValue.String:DA-ELSE
            return this.actualValue;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.getActualValue.String:DA-END
        }
        /**
         * setter for the field actualUnit
         * 
         * null
         * 
         * @param actualUnit  the actualUnit
         */
        public void setActualUnit(String actualUnit) {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.setActualUnit.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.setActualUnit.String:DA-ELSE
            if (isDifferent(this.actualUnit, actualUnit)) {
                this.setDirty(true);
                this.actualUnit = actualUnit;
            }
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.setActualUnit.String:DA-END
        }
        /**
         * getter for the field actualUnit
         * 
         * null
         * 
         * @return
         */
        public String getActualUnit() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.getActualUnit.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.getActualUnit.String:DA-ELSE
            return this.actualUnit;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.getActualUnit.String:DA-END
        }
        /**
         * getter for the field pkEntityPk
         * 
         * PrimaryKey field for Entity 'BaseEntity' and Id 'pk'
         * 
         * @return
         */
        public Long getPkEntityPk() {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.getPkEntityPk.Long:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.getPkEntityPk.Long:DA-ELSE
            return this.pkEntityPk;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.getPkEntityPk.Long:DA-END
        }
        /**
         * setter for the field pkEntityPk
         * 
         * PrimaryKey field for Entity 'BaseEntity' and Id 'pk'
         * 
         * @param pkEntityPk  the pkEntityPk
         */
        public void setPkEntityPk(Long pkEntityPk) {
            //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.setPkEntityPk.Long:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.setPkEntityPk.Long:DA-ELSE
            this.pkEntityPk = pkEntityPk;
            //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.setPkEntityPk.Long:DA-END
        }
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.methods:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.methods:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.methods:DA-END
        
        
        //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.additional.elements.in.type:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.additional.elements.in.type:DA-END
} // end of java type