package com.gs.gapp.iot.greenhouse;


import java.util.Collection;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Named;

/*
 * Imports from last generation
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean;
import com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean;
import com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto;
import com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto.ListOfSensorsInstancesDto;
import com.gs.gapp.iot.greenhouse.function.SensorInstanceService;
import com.vd.AbstractEJB;
import com.vd.SessionDataHolder;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;


//DA-START:greenhouse.MainScreenEJB:DA-START
//DA-ELSE:greenhouse.MainScreenEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:greenhouse.MainScreenEJB:DA-END

public class MainScreenEJB extends AbstractEJB { // start of class

    /**
     * creates an instance of MainScreenEJB
     */
    public MainScreenEJB() {
        //DA-START:com.gs.gapp.iot.greenhouse.MainScreenEJB:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenEJB:DA-ELSE
        super();
        //DA-END:com.gs.gapp.iot.greenhouse.MainScreenEJB:DA-END
    }
    
    /**
     * load the initial data that a managed bean then uses to serve one or more xhtml pages to fill it with dynamic data
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.MainScreenEJB.load.MainScreenDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenEJB.load.MainScreenDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.MainScreenEJB.load.MainScreenDto.SessionDataHolder.annotations:DA-END
    public MainScreenDto load(MainScreenDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.MainScreenEJB.load.MainScreenDto.SessionDataHolder.MainScreenDto:DA-START
    	MainScreenDto result = getSensorInstances();
    	return result;
        //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenEJB.load.MainScreenDto.SessionDataHolder.MainScreenDto:DA-ELSE
        //return null;
        //DA-END:com.gs.gapp.iot.greenhouse.MainScreenEJB.load.MainScreenDto.SessionDataHolder.MainScreenDto:DA-END
    }
    /**
     * update the screen's main dto instance
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.MainScreenEJB.update.MainScreenDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenEJB.update.MainScreenDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.MainScreenEJB.update.MainScreenDto.SessionDataHolder.annotations:DA-END
    public MainScreenDto update(MainScreenDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.MainScreenEJB.update.MainScreenDto.SessionDataHolder.MainScreenDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenEJB.update.MainScreenDto.SessionDataHolder.MainScreenDto:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.MainScreenEJB.update.MainScreenDto.SessionDataHolder.MainScreenDto:DA-END
    }
    /**
     * update a single dto instance
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.MainScreenEJB.updateListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenEJB.updateListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.MainScreenEJB.updateListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.annotations:DA-END
    public MainScreenDto.ListOfSensorsInstancesDto updateListOfSensorsInstancesDto(MainScreenDto.ListOfSensorsInstancesDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.MainScreenEJB.updateListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.ListOfSensorsInstancesDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenEJB.updateListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.ListOfSensorsInstancesDto:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.MainScreenEJB.updateListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.ListOfSensorsInstancesDto:DA-END
    }
    /**
     * update a collection of dto instances
     * 
     * @param dtos  the dtos
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.MainScreenEJB.updateListOfSensorsInstancesDto.Collection.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenEJB.updateListOfSensorsInstancesDto.Collection.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.MainScreenEJB.updateListOfSensorsInstancesDto.Collection.SessionDataHolder.annotations:DA-END
    public Collection<MainScreenDto.ListOfSensorsInstancesDto> updateListOfSensorsInstancesDto(Collection<MainScreenDto.ListOfSensorsInstancesDto> dtos, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.MainScreenEJB.updateListOfSensorsInstancesDto.Collection.SessionDataHolder.Collection:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenEJB.updateListOfSensorsInstancesDto.Collection.SessionDataHolder.Collection:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.MainScreenEJB.updateListOfSensorsInstancesDto.Collection.SessionDataHolder.Collection:DA-END
    }
    /**
     * delete a single dto instance
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     */
    //DA-START:com.gs.gapp.iot.greenhouse.MainScreenEJB.deleteListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenEJB.deleteListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.MainScreenEJB.deleteListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder.annotations:DA-END
    public void deleteListOfSensorsInstancesDto(MainScreenDto.ListOfSensorsInstancesDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.MainScreenEJB.deleteListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenEJB.deleteListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.MainScreenEJB.deleteListOfSensorsInstancesDto.ListOfSensorsInstancesDto.SessionDataHolder:DA-END
    }
    /**
     * delete a collection of dto instances
     * 
     * @param dtos  the dtos
     * @param sessionDataHolder  the sessionDataHolder
     */
    //DA-START:com.gs.gapp.iot.greenhouse.MainScreenEJB.deleteListOfSensorsInstancesDto.Collection.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenEJB.deleteListOfSensorsInstancesDto.Collection.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.MainScreenEJB.deleteListOfSensorsInstancesDto.Collection.SessionDataHolder.annotations:DA-END
    public void deleteListOfSensorsInstancesDto(Collection<MainScreenDto.ListOfSensorsInstancesDto> dtos, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.MainScreenEJB.deleteListOfSensorsInstancesDto.Collection.SessionDataHolder:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenEJB.deleteListOfSensorsInstancesDto.Collection.SessionDataHolder:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.MainScreenEJB.deleteListOfSensorsInstancesDto.Collection.SessionDataHolder:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.MainScreenEJB.additional.elements.in.type:DA-START
    private MainScreenDto getSensorInstances() {
    	MainScreenDto result = new MainScreenDto();
    	
    	Gson gson = new GsonBuilder()
    			.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX")
    			.create();
    	
    	RestAdapter restAdapter = new RestAdapter.Builder()
    			.setLogLevel(LogLevel.FULL)
    			.setEndpoint("http://localhost:8080/GreenhouseRestApi-JAX-RS/greenhouse/v1")
    			.setConverter(new GsonConverter(gson))
    			.build();
    	
    	SensorInstanceService sensorInstanceService = restAdapter.create(SensorInstanceService.class);
		SensorInstanceListBean sensorInstanceListBean = sensorInstanceService.getSensorInstances();
    	
    	for (SensorInstanceBean sensorInstanceBean : sensorInstanceListBean.getSensorInstanceList()) {
    		ListOfSensorsInstancesDto dto = result.new ListOfSensorsInstancesDto();
    		dto.setPkEntityPk(sensorInstanceBean.getPk());
    		dto.setId(sensorInstanceBean.getId());
    		result.getListOfSensorsInstances().add(dto);
    	}
    	
    	return result;
    }
    
    /**
     * @author mmt
     *
     */
    public class GetSensorInstancesCallback implements Callback<SensorInstanceListBean> {

		@Override
		public void failure(RetrofitError retrofitError) {
			retrofitError.printStackTrace();
		}

		@Override
		public void success(SensorInstanceListBean arg0, Response arg1) {
			System.out.println("successfully retrieved sensor instances through REST API");
		}
    }
    //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenEJB.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.MainScreenEJB.additional.elements.in.type:DA-END
} // end of java type