package com.gs.gapp.iot.greenhouse;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import com.vd.AbstractEJB;
import com.gs.gapp.iot.greenhouse.cockpitapplication.SensorValueMonitoringScreenDto;
import com.vd.SessionDataHolder;

//DA-START:greenhouse.SensorValueMonitoringScreenEJB:DA-START
//DA-ELSE:greenhouse.SensorValueMonitoringScreenEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:greenhouse.SensorValueMonitoringScreenEJB:DA-END

public class SensorValueMonitoringScreenEJB extends AbstractEJB { // start of class

    /**
     * creates an instance of SensorValueMonitoringScreenEJB
     */
    public SensorValueMonitoringScreenEJB() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB:DA-ELSE
        super();
        //DA-END:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB:DA-END
    }
    
    /**
     * load the initial data that a managed bean then uses to serve one or more xhtml pages to fill it with dynamic data
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB.load.SensorValueMonitoringScreenDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB.load.SensorValueMonitoringScreenDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB.load.SensorValueMonitoringScreenDto.SessionDataHolder.annotations:DA-END
    public SensorValueMonitoringScreenDto load(SensorValueMonitoringScreenDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB.load.SensorValueMonitoringScreenDto.SessionDataHolder.SensorValueMonitoringScreenDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB.load.SensorValueMonitoringScreenDto.SessionDataHolder.SensorValueMonitoringScreenDto:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB.load.SensorValueMonitoringScreenDto.SessionDataHolder.SensorValueMonitoringScreenDto:DA-END
    }
    /**
     * update the screen's main dto instance
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB.update.SensorValueMonitoringScreenDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB.update.SensorValueMonitoringScreenDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB.update.SensorValueMonitoringScreenDto.SessionDataHolder.annotations:DA-END
    public SensorValueMonitoringScreenDto update(SensorValueMonitoringScreenDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB.update.SensorValueMonitoringScreenDto.SessionDataHolder.SensorValueMonitoringScreenDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB.update.SensorValueMonitoringScreenDto.SessionDataHolder.SensorValueMonitoringScreenDto:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB.update.SensorValueMonitoringScreenDto.SessionDataHolder.SensorValueMonitoringScreenDto:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB.additional.elements.in.type:DA-END
} // end of java type