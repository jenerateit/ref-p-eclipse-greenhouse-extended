package com.gs.gapp.iot.greenhouse;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import com.vd.AbstractEJB;
import com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto;
import com.vd.SessionDataHolder;
import com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto.ActuatorControlDto;
import java.util.Collection;

//DA-START:greenhouse.ActuatorControlScreenEJB:DA-START
//DA-ELSE:greenhouse.ActuatorControlScreenEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:greenhouse.ActuatorControlScreenEJB:DA-END

public class ActuatorControlScreenEJB extends AbstractEJB { // start of class

    /**
     * creates an instance of ActuatorControlScreenEJB
     */
    public ActuatorControlScreenEJB() {
        //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB:DA-ELSE
        super();
        //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB:DA-END
    }
    
    /**
     * load the initial data that a managed bean then uses to serve one or more xhtml pages to fill it with dynamic data
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.load.ActuatorControlScreenDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.load.ActuatorControlScreenDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.load.ActuatorControlScreenDto.SessionDataHolder.annotations:DA-END
    public ActuatorControlScreenDto load(ActuatorControlScreenDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.load.ActuatorControlScreenDto.SessionDataHolder.ActuatorControlScreenDto:DA-START
    	return new ActuatorControlScreenDto();
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.load.ActuatorControlScreenDto.SessionDataHolder.ActuatorControlScreenDto:DA-ELSE
        //return null;
        //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.load.ActuatorControlScreenDto.SessionDataHolder.ActuatorControlScreenDto:DA-END
    }
    /**
     * update the screen's main dto instance
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.update.ActuatorControlScreenDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.update.ActuatorControlScreenDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.update.ActuatorControlScreenDto.SessionDataHolder.annotations:DA-END
    public ActuatorControlScreenDto update(ActuatorControlScreenDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.update.ActuatorControlScreenDto.SessionDataHolder.ActuatorControlScreenDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.update.ActuatorControlScreenDto.SessionDataHolder.ActuatorControlScreenDto:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.update.ActuatorControlScreenDto.SessionDataHolder.ActuatorControlScreenDto:DA-END
    }
    /**
     * update a single dto instance
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.updateActuatorControlDto.ActuatorControlDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.updateActuatorControlDto.ActuatorControlDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.updateActuatorControlDto.ActuatorControlDto.SessionDataHolder.annotations:DA-END
    public ActuatorControlScreenDto.ActuatorControlDto updateActuatorControlDto(ActuatorControlScreenDto.ActuatorControlDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.updateActuatorControlDto.ActuatorControlDto.SessionDataHolder.ActuatorControlDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.updateActuatorControlDto.ActuatorControlDto.SessionDataHolder.ActuatorControlDto:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.updateActuatorControlDto.ActuatorControlDto.SessionDataHolder.ActuatorControlDto:DA-END
    }
    /**
     * update a collection of dto instances
     * 
     * @param dtos  the dtos
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.updateActuatorControlDto.Collection.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.updateActuatorControlDto.Collection.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.updateActuatorControlDto.Collection.SessionDataHolder.annotations:DA-END
    public Collection<ActuatorControlScreenDto.ActuatorControlDto> updateActuatorControlDto(Collection<ActuatorControlScreenDto.ActuatorControlDto> dtos, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.updateActuatorControlDto.Collection.SessionDataHolder.Collection:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.updateActuatorControlDto.Collection.SessionDataHolder.Collection:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.updateActuatorControlDto.Collection.SessionDataHolder.Collection:DA-END
    }
    /**
     * delete a single dto instance
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     */
    //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.deleteActuatorControlDto.ActuatorControlDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.deleteActuatorControlDto.ActuatorControlDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.deleteActuatorControlDto.ActuatorControlDto.SessionDataHolder.annotations:DA-END
    public void deleteActuatorControlDto(ActuatorControlScreenDto.ActuatorControlDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.deleteActuatorControlDto.ActuatorControlDto.SessionDataHolder:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.deleteActuatorControlDto.ActuatorControlDto.SessionDataHolder:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.deleteActuatorControlDto.ActuatorControlDto.SessionDataHolder:DA-END
    }
    /**
     * delete a collection of dto instances
     * 
     * @param dtos  the dtos
     * @param sessionDataHolder  the sessionDataHolder
     */
    //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.deleteActuatorControlDto.Collection.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.deleteActuatorControlDto.Collection.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.deleteActuatorControlDto.Collection.SessionDataHolder.annotations:DA-END
    public void deleteActuatorControlDto(Collection<ActuatorControlScreenDto.ActuatorControlDto> dtos, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.deleteActuatorControlDto.Collection.SessionDataHolder:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.deleteActuatorControlDto.Collection.SessionDataHolder:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.deleteActuatorControlDto.Collection.SessionDataHolder:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenEJB.additional.elements.in.type:DA-END
} // end of java type