package com.gs.gapp.iot.greenhouse;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import com.vd.AbstractManagedBean;
import com.gs.gapp.iot.greenhouse.cockpitapplication.SensorValueMonitoringScreenDto;
import javax.ejb.EJB;
import com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenEJB;
import javax.annotation.PostConstruct;
import java.lang.Override;
import javax.faces.event.ComponentSystemEvent;

@ManagedBean(name="sensorValueMonitoringScreenBean")
@ViewScoped
public class SensorValueMonitoringScreenBean extends AbstractManagedBean { // start of class

    private SensorValueMonitoringScreenDto dto = new SensorValueMonitoringScreenDto();
    
    @EJB
    private SensorValueMonitoringScreenEJB ejb;
    
    /**
     * getter for the field dto
     * 
     * null
     * 
     * @return
     */
    public SensorValueMonitoringScreenDto getDto() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.getDto.SensorValueMonitoringScreenDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.getDto.SensorValueMonitoringScreenDto:DA-ELSE
        return this.dto;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.getDto.SensorValueMonitoringScreenDto:DA-END
    }
    /**
     * reads data from an EJB or from a different source and provides this data to xhtml pages
     */
    @PostConstruct
    @Override
    public void init() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.init:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.init:DA-ELSE
        dto = ejb.load(dto, getSessionDataHolder());
        //DA-END:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.init:DA-END
    }
    /**
     * method that returns true if the page holds a data record that is not yet in a database
     * 
     * @return
     */
    @Override
    public boolean isNew() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.isNew.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.isNew.boolean:DA-ELSE
        return super.isNew();
        //DA-END:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.isNew.boolean:DA-END
    }
    /**
     * method that returns true if the page holds a data record that comes from a database but has been modified
     * 
     * @return
     */
    @Override
    public boolean isModified() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.isModified.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.isModified.boolean:DA-ELSE
        return super.isModified();
        //DA-END:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.isModified.boolean:DA-END
    }
    /**
     * this method can get called when you define an event inside a f:metadata tag, e.g.: <f:event type="preRenderView" listener="#{myBean.initialize}"/>
     * 
     * @param event  the event
     * @return
     */
    @Override
    public boolean preRenderViewListener(ComponentSystemEvent event) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.preRenderViewListener.ComponentSystemEvent.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.preRenderViewListener.ComponentSystemEvent.boolean:DA-ELSE
        return super.preRenderViewListener(event);
        //DA-END:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.preRenderViewListener.ComponentSystemEvent.boolean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.SensorValueMonitoringScreenBean.additional.elements.in.type:DA-END
} // end of java type