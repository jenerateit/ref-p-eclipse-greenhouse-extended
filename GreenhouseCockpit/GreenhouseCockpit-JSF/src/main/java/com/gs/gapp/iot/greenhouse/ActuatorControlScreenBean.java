package com.gs.gapp.iot.greenhouse;


/*
 * Imports from last generation
 */
import java.net.URI;
import java.net.URISyntaxException;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ComponentSystemEvent;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.Utils;
import org.eclipse.californium.core.coap.MediaTypeRegistry;

import com.gs.gapp.iot.greenhouse.cockpitapplication.ActuatorControlScreenDto;
import com.vd.AbstractManagedBean;


@ManagedBean(name="actuatorControlScreenBean")
@ViewScoped
public class ActuatorControlScreenBean extends AbstractManagedBean { // start of class

    private ActuatorControlScreenDto dto = new ActuatorControlScreenDto();
    
    @EJB
    private ActuatorControlScreenEJB ejb;
    
    /**
     * getter for the field dto
     * 
     * null
     * 
     * @return
     */
    public ActuatorControlScreenDto getDto() {
        //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.getDto.ActuatorControlScreenDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.getDto.ActuatorControlScreenDto:DA-ELSE
        return this.dto;
        //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.getDto.ActuatorControlScreenDto:DA-END
    }
    /**
     * reads data from an EJB or from a different source and provides this data to xhtml pages
     */
    @PostConstruct
    @Override
    public void init() {
        //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.init:DA-START
    	dto = ejb.load(dto, getSessionDataHolder());
    	dto.getActuatorControl().setLightStatus(getLightStatus());
    	dto.getActuatorControl().setTemperature(getTemperature());
    	dto.getActuatorControl().setHumidity(getHumidity());
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.init:DA-ELSE
        //dto = ejb.load(dto, getSessionDataHolder());
        //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.init:DA-END
    }
    /**
     * method that returns true if the page holds a data record that is not yet in a database
     * 
     * @return
     */
    @Override
    public boolean isNew() {
        //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.isNew.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.isNew.boolean:DA-ELSE
        return super.isNew();
        //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.isNew.boolean:DA-END
    }
    /**
     * method that returns true if the page holds a data record that comes from a database but has been modified
     * 
     * @return
     */
    @Override
    public boolean isModified() {
        //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.isModified.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.isModified.boolean:DA-ELSE
        return super.isModified();
        //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.isModified.boolean:DA-END
    }
    /**
     * this method can get called when you define an event inside a f:metadata tag, e.g.: <f:event type="preRenderView" listener="#{myBean.initialize}"/>
     * 
     * @param event  the event
     * @return
     */
    @Override
    public boolean preRenderViewListener(ComponentSystemEvent event) {
        //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.preRenderViewListener.ComponentSystemEvent.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.preRenderViewListener.ComponentSystemEvent.boolean:DA-ELSE
        return super.preRenderViewListener(event);
        //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.preRenderViewListener.ComponentSystemEvent.boolean:DA-END
    }
    /**
     * event handler for action component 'toggleLight' in container 'ActuatorControlToolbar'
     * 
     * @return
     */
    public String toggleLightActuatorControlToolbar() {
        //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.toggleLightActuatorControlToolbar.String:DA-START
    	try {
    		URI uri = new URI("coap://" + getIpOrHost() + ":5683/gh/act/light");  // use PUT and "on" or "off" to toggle the light
			CoapClient client = new CoapClient(uri);

			// use this when you want to discover the available resources on a CoAP server
//			Set<WebLink> discoveredWebLinks = client.discover();

			CoapResponse response = client.put("off".equalsIgnoreCase(getLightStatus()) ? "on" : "off", MediaTypeRegistry.TEXT_PLAIN);
			
			if (response != null) {
				getLightStatusActuatorControlToolbar();  // make sure that the value is in the dto, too
				
				System.out.println(response.getOptions());
				System.out.println(response.getCode());
				System.out.println(response.getResponseText());
				System.out.println(Utils.prettyPrint(response));
				
			} else {
				System.out.println("No response received.");
			}
		} catch (URISyntaxException e) {
			System.err.println("Invalid URI: " + e.getMessage());
		}
    	
    	return null;
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.toggleLightActuatorControlToolbar.String:DA-ELSE
        //return "";
        //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.toggleLightActuatorControlToolbar.String:DA-END
    }
    /**
     * event handler for action component 'getLightStatus' in container 'ActuatorControlToolbar'
     * 
     * @return
     */
    public String getLightStatusActuatorControlToolbar() {
        //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.getLightStatusActuatorControlToolbar.String:DA-START
    	dto.getActuatorControl().setLightStatus(getLightStatus());
    	return null;
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.getLightStatusActuatorControlToolbar.String:DA-ELSE
        //return "";
        //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.getLightStatusActuatorControlToolbar.String:DA-END
    }
    /**
     * event handler for action component 'getTemperature' in container 'ActuatorControlToolbar'
     * 
     * @return
     */
    public String getTemperatureActuatorControlToolbar() {
        //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.getTemperatureActuatorControlToolbar.String:DA-START
    	dto.getActuatorControl().setTemperature(getTemperature());
    	return null;
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.getTemperatureActuatorControlToolbar.String:DA-ELSE
        //return "";
        //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.getTemperatureActuatorControlToolbar.String:DA-END
    }
    /**
     * event handler for action component 'getHumidity' in container 'ActuatorControlToolbar'
     * 
     * @return
     */
    public String getHumidityActuatorControlToolbar() {
        //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.getHumidityActuatorControlToolbar.String:DA-START
    	dto.getActuatorControl().setHumidity(getHumidity());
    	return null;
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.getHumidityActuatorControlToolbar.String:DA-ELSE
        //return "";
        //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.getHumidityActuatorControlToolbar.String:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.additional.elements.in.type:DA-START
    private final String IP_GATEWAY = "192.168.2.110";  // use "californium.eclipse.org" when you want to use Eclipse IoT sample CoAP server
    
    /**
     * @return
     */
    public String getTemperature() {
    	String result = "unknown";
    	
    	if (getIpOrHost() != null && getIpOrHost().length() > 0) {
	    	try {
	    		URI uri = new URI("coap://" + getIpOrHost() + ":5683/gh/sens/temperature");  // use GET to read temperature
				CoapClient client = new CoapClient(uri);
				CoapResponse response = client.get();
				
				if (response != null) {
					result = new String(response.getPayload());
				} else {
					System.out.println("No response received.");
				}
			} catch (URISyntaxException e) {
				System.err.println("Invalid URI: " + e.getMessage());
			}
    	}
    	
    	return result;
    }
    
    /**
     * @return
     */
    public String getHumidity() {
    	String result = "unknown";
    	
    	if (getIpOrHost() != null && getIpOrHost().length() > 0) {
	    	try {
	    		URI uri = new URI("coap://" + getIpOrHost() + ":5683/gh/sens/humidity");  // use GET to read humidity
				CoapClient client = new CoapClient(uri);
				CoapResponse response = client.get();
				
				if (response != null) {
					result = new String(response.getPayload());
				} else {
					System.out.println("No response received.");
				}
			} catch (URISyntaxException e) {
				System.err.println("Invalid URI: " + e.getMessage());
			}
    	}
    	
    	return result;
    }
    
    /**
     * @return
     */
    public String getLightStatus() {
    	String result = "unknown";
    	
    	if (getIpOrHost() != null && getIpOrHost().length() > 0) {
	    	try {
	    		URI uri = new URI("coap://" + getIpOrHost() + ":5683/gh/sens/light");  // use GET to read light status
				CoapClient client = new CoapClient(uri);
				CoapResponse response = client.get();
				
				if (response != null) {
					result = new String(response.getPayload());
				} else {
					System.out.println("No response received.");
				}
			} catch (URISyntaxException e) {
				System.err.println("Invalid URI: " + e.getMessage());
			}
    	}
    	
    	return result;
    }
    
    /**
     * @return
     */
    private String getIpOrHost() {
    	return dto != null && dto.getActuatorControl().getIpOrHostOfCoapServer() != null && dto.getActuatorControl().getIpOrHostOfCoapServer().length() > 0 ?
    			dto.getActuatorControl().getIpOrHostOfCoapServer() : IP_GATEWAY;
    }
    //DA-ELSE:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.ActuatorControlScreenBean.additional.elements.in.type:DA-END
} // end of java type