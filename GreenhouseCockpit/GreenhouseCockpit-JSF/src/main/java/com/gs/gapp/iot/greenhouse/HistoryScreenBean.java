package com.gs.gapp.iot.greenhouse;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import com.vd.AbstractManagedBean;
import com.gs.gapp.iot.greenhouse.cockpitapplication.HistoryScreenDto;
import javax.ejb.EJB;
import com.gs.gapp.iot.greenhouse.HistoryScreenEJB;
import javax.annotation.PostConstruct;
import java.lang.Override;
import javax.faces.event.ComponentSystemEvent;

@ManagedBean(name="historyScreenBean")
@ViewScoped
public class HistoryScreenBean extends AbstractManagedBean { // start of class

    private HistoryScreenDto dto = new HistoryScreenDto();
    
    @EJB
    private HistoryScreenEJB ejb;
    
    /**
     * getter for the field dto
     * 
     * null
     * 
     * @return
     */
    public HistoryScreenDto getDto() {
        //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenBean.getDto.HistoryScreenDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenBean.getDto.HistoryScreenDto:DA-ELSE
        return this.dto;
        //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenBean.getDto.HistoryScreenDto:DA-END
    }
    /**
     * reads data from an EJB or from a different source and provides this data to xhtml pages
     */
    @PostConstruct
    @Override
    public void init() {
        //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenBean.init:DA-START
    	if (this.sensorInstancePk != null) {
	    	dto.getSensorInstance().setPkEntityPk(this.sensorInstancePk);
	    	dto = ejb.load(dto, getSessionDataHolder());
    	}
        //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenBean.init:DA-ELSE
        //dto = ejb.load(dto, getSessionDataHolder());
        //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenBean.init:DA-END
    }
    /**
     * method that returns true if the page holds a data record that is not yet in a database
     * 
     * @return
     */
    @Override
    public boolean isNew() {
        //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenBean.isNew.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenBean.isNew.boolean:DA-ELSE
        return super.isNew();
        //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenBean.isNew.boolean:DA-END
    }
    /**
     * method that returns true if the page holds a data record that comes from a database but has been modified
     * 
     * @return
     */
    @Override
    public boolean isModified() {
        //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenBean.isModified.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenBean.isModified.boolean:DA-ELSE
        return super.isModified();
        //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenBean.isModified.boolean:DA-END
    }
    /**
     * this method can get called when you define an event inside a f:metadata tag, e.g.: <f:event type="preRenderView" listener="#{myBean.initialize}"/>
     * 
     * @param event  the event
     * @return
     */
    @Override
    public boolean preRenderViewListener(ComponentSystemEvent event) {
        //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenBean.preRenderViewListener.ComponentSystemEvent.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenBean.preRenderViewListener.ComponentSystemEvent.boolean:DA-ELSE
        return super.preRenderViewListener(event);
        //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenBean.preRenderViewListener.ComponentSystemEvent.boolean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.HistoryScreenBean.additional.elements.in.type:DA-START
    private Long sensorInstancePk;

    public Long getSensorInstancePk() {
		return sensorInstancePk;
	}
	public void setSensorInstancePk(Long sensorInstancePk) {
  		this.sensorInstancePk = sensorInstancePk;
  	}
    //DA-ELSE:com.gs.gapp.iot.greenhouse.HistoryScreenBean.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.HistoryScreenBean.additional.elements.in.type:DA-END
} // end of java type