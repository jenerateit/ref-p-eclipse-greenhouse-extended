package com.gs.gapp.iot.greenhouse;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import com.vd.AbstractManagedBean;
import com.gs.gapp.iot.greenhouse.cockpitapplication.MainScreenDto;
import javax.ejb.EJB;
import com.gs.gapp.iot.greenhouse.MainScreenEJB;
import javax.annotation.PostConstruct;
import java.lang.Override;
import javax.faces.event.ComponentSystemEvent;
import java.lang.String;

@ManagedBean(name="mainScreenBean")
@ViewScoped
public class MainScreenBean extends AbstractManagedBean { // start of class

    private MainScreenDto dto = new MainScreenDto();
    
    @EJB
    private MainScreenEJB ejb;
    
    /**
     * getter for the field dto
     * 
     * null
     * 
     * @return
     */
    public MainScreenDto getDto() {
        //DA-START:com.gs.gapp.iot.greenhouse.MainScreenBean.getDto.MainScreenDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenBean.getDto.MainScreenDto:DA-ELSE
        return this.dto;
        //DA-END:com.gs.gapp.iot.greenhouse.MainScreenBean.getDto.MainScreenDto:DA-END
    }
    /**
     * reads data from an EJB or from a different source and provides this data to xhtml pages
     */
    @PostConstruct
    @Override
    public void init() {
        //DA-START:com.gs.gapp.iot.greenhouse.MainScreenBean.init:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenBean.init:DA-ELSE
        dto = ejb.load(dto, getSessionDataHolder());
        //DA-END:com.gs.gapp.iot.greenhouse.MainScreenBean.init:DA-END
    }
    /**
     * method that returns true if the page holds a data record that is not yet in a database
     * 
     * @return
     */
    @Override
    public boolean isNew() {
        //DA-START:com.gs.gapp.iot.greenhouse.MainScreenBean.isNew.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenBean.isNew.boolean:DA-ELSE
        return super.isNew();
        //DA-END:com.gs.gapp.iot.greenhouse.MainScreenBean.isNew.boolean:DA-END
    }
    /**
     * method that returns true if the page holds a data record that comes from a database but has been modified
     * 
     * @return
     */
    @Override
    public boolean isModified() {
        //DA-START:com.gs.gapp.iot.greenhouse.MainScreenBean.isModified.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenBean.isModified.boolean:DA-ELSE
        return super.isModified();
        //DA-END:com.gs.gapp.iot.greenhouse.MainScreenBean.isModified.boolean:DA-END
    }
    /**
     * this method can get called when you define an event inside a f:metadata tag, e.g.: <f:event type="preRenderView" listener="#{myBean.initialize}"/>
     * 
     * @param event  the event
     * @return
     */
    @Override
    public boolean preRenderViewListener(ComponentSystemEvent event) {
        //DA-START:com.gs.gapp.iot.greenhouse.MainScreenBean.preRenderViewListener.ComponentSystemEvent.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenBean.preRenderViewListener.ComponentSystemEvent.boolean:DA-ELSE
        return super.preRenderViewListener(event);
        //DA-END:com.gs.gapp.iot.greenhouse.MainScreenBean.preRenderViewListener.ComponentSystemEvent.boolean:DA-END
    }
    /**
     * event handler for component 'showSensorData' in container 'ListOfSensorsInstances'
     * 
     * @return
     */
    public String actionShowSensorDataListOfSensorsInstances() {
        //DA-START:com.gs.gapp.iot.greenhouse.MainScreenBean.actionShowSensorDataListOfSensorsInstances.String:DA-START
    	return "/cockpitapplication/historyScreen.xhtml?sensorInstancePk=" + this.selectedSensorInstance.getPkEntityPk() + "&faces-redirect=true";
        //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenBean.actionShowSensorDataListOfSensorsInstances.String:DA-ELSE
        //return "";
        //DA-END:com.gs.gapp.iot.greenhouse.MainScreenBean.actionShowSensorDataListOfSensorsInstances.String:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.MainScreenBean.additional.elements.in.type:DA-START
    private MainScreenDto.ListOfSensorsInstancesDto selectedSensorInstance;

	public MainScreenDto.ListOfSensorsInstancesDto getSelectedSensorInstance() {
		return selectedSensorInstance;
	}
	public void setSelectedSensorInstance(MainScreenDto.ListOfSensorsInstancesDto selectedSensorInstance) {
		this.selectedSensorInstance = selectedSensorInstance;
	}
    
    //DA-ELSE:com.gs.gapp.iot.greenhouse.MainScreenBean.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.MainScreenBean.additional.elements.in.type:DA-END
} // end of java type