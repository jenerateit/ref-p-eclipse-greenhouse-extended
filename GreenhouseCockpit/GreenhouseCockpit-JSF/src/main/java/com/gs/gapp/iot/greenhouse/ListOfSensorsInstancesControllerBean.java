package com.gs.gapp.iot.greenhouse;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import com.vd.AbstractManagedBean;
import com.gs.gapp.iot.greenhouse.cockpitapplication.ListOfSensorsInstancesControllerDto;
import javax.ejb.EJB;
import com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerEJB;
import javax.annotation.PostConstruct;
import java.lang.Override;
import javax.faces.event.ComponentSystemEvent;
import java.lang.String;

/**
 * This list displays all sensor instances.
 * 
 */
@ManagedBean(name="listOfSensorsInstancesControllerBean")
@ViewScoped
public class ListOfSensorsInstancesControllerBean extends AbstractManagedBean { // start of class

    private ListOfSensorsInstancesControllerDto dto = new ListOfSensorsInstancesControllerDto();
    
    @EJB
    private ListOfSensorsInstancesControllerEJB ejb;
    
    /**
     * getter for the field dto
     * 
     * null
     * 
     * @return
     */
    public ListOfSensorsInstancesControllerDto getDto() {
        //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.getDto.ListOfSensorsInstancesControllerDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.getDto.ListOfSensorsInstancesControllerDto:DA-ELSE
        return this.dto;
        //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.getDto.ListOfSensorsInstancesControllerDto:DA-END
    }
    /**
     * reads data from an EJB or from a different source and provides this data to xhtml pages
     */
    @PostConstruct
    @Override
    public void init() {
        //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.init:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.init:DA-ELSE
        dto = ejb.load(dto, getSessionDataHolder());
        //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.init:DA-END
    }
    /**
     * method that returns true if the page holds a data record that is not yet in a database
     * 
     * @return
     */
    @Override
    public boolean isNew() {
        //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.isNew.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.isNew.boolean:DA-ELSE
        return super.isNew();
        //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.isNew.boolean:DA-END
    }
    /**
     * method that returns true if the page holds a data record that comes from a database but has been modified
     * 
     * @return
     */
    @Override
    public boolean isModified() {
        //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.isModified.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.isModified.boolean:DA-ELSE
        return super.isModified();
        //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.isModified.boolean:DA-END
    }
    /**
     * this method can get called when you define an event inside a f:metadata tag, e.g.: <f:event type="preRenderView" listener="#{myBean.initialize}"/>
     * 
     * @param event  the event
     * @return
     */
    @Override
    public boolean preRenderViewListener(ComponentSystemEvent event) {
        //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.preRenderViewListener.ComponentSystemEvent.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.preRenderViewListener.ComponentSystemEvent.boolean:DA-ELSE
        return super.preRenderViewListener(event);
        //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.preRenderViewListener.ComponentSystemEvent.boolean:DA-END
    }
    /**
     * event handler for component 'showSensorData' in container 'ListOfSensorsInstances'
     * 
     * @return
     */
    public String actionShowSensorDataListOfSensorsInstances() {
        //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.actionShowSensorDataListOfSensorsInstances.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.actionShowSensorDataListOfSensorsInstances.String:DA-ELSE
        return "";
        //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.actionShowSensorDataListOfSensorsInstances.String:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.ListOfSensorsInstancesControllerBean.additional.elements.in.type:DA-END
} // end of java type