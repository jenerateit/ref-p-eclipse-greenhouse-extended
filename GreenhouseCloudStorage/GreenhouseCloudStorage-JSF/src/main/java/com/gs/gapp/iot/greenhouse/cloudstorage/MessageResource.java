package com.gs.gapp.iot.greenhouse.cloudstorage;

import org.primefaces.push.EventBus;
import org.primefaces.push.RemoteEndpoint;
import org.primefaces.push.annotation.OnClose;
import org.primefaces.push.annotation.OnMessage;
import org.primefaces.push.annotation.OnOpen;
import org.primefaces.push.annotation.PushEndpoint;
import org.primefaces.push.annotation.Singleton;
import org.primefaces.push.impl.JSONEncoder;

@PushEndpoint("/messages")
@Singleton
public class MessageResource {
	
	@OnOpen
    public void onOpen(RemoteEndpoint remoteEndpoint, EventBus eventBus) {
		System.out.println("onOpen(), remote endpoint: " + remoteEndpoint + ", event bus:" + eventBus);
	}
 
    @OnClose
    public void onClose(RemoteEndpoint remoteEndpoint, EventBus eventBus) {
    	System.out.println("onClose(), remote endpoint: " + remoteEndpoint + ", event bus:" + eventBus);
    }
 
    @OnMessage(encoders={JSONEncoder.class})
    public Message onMessage(Message message) {
    	System.out.println("onMessage(), message:" + message);
        return message;
    }
}
