package com.gs.gapp.iot.greenhouse.cloudstorage;

import java.util.Date;

public class Message {

	private final String topic;
	private final String content;
	
	private int qos;
	private boolean retained;
	private boolean duplicate;
	private Date messageArrival;
	
	
	public Message(String topic, String content) {
		super();
		this.topic = topic;
		this.content = content;
	}

	public String getTopic() {
		return topic;
	}

	public String getContent() {
		return content;
	}

	public int getQos() {
		return qos;
	}

	public void setQos(int qos) {
		this.qos = qos;
	}

	public boolean isRetained() {
		return retained;
	}

	public void setRetained(boolean retained) {
		this.retained = retained;
	}

	public boolean isDuplicate() {
		return duplicate;
	}

	public void setDuplicate(boolean duplicate) {
		this.duplicate = duplicate;
	}

	public Date getMessageArrival() {
		return messageArrival;
	}

	public void setMessageArrival(Date messageArrival) {
		this.messageArrival = messageArrival;
	}

	@Override
	public String toString() {
		return "Message [topic=" + topic + ", content=" + content + "]";
	}
}
