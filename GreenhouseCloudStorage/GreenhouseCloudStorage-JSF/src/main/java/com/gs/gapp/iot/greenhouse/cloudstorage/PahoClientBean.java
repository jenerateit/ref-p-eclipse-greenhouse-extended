package com.gs.gapp.iot.greenhouse.cloudstorage;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJBException;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.event.AjaxBehaviorEvent;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;

/**
 * Managed bean that creates and manages the Paho client
 */
@ApplicationScoped
@ManagedBean(eager=false, name="pahoClientBean")
public class PahoClientBean implements MqttCallback {

	private EventBus eventBus;
	private MqttClient mqttClient;
	private MqttConnectOptions connectionOptions = new MqttConnectOptions();
	private boolean tryConnecting = true;
	private Date timeOfConnection;
	private boolean connected = false;

	
    /**
     * Default constructor. 
     */
    public PahoClientBean() {}

    /**
     * 
     */
    @PostConstruct
    public void init() {
    	
    	try {
    	    eventBus = EventBusFactory.getDefault().eventBus();
    	} catch (Throwable th) {
    		th.printStackTrace();
    	}
    	
    	try {
	    	initClient();
	    	initConnectionOptions();
	    	connect();
	    	
	    	if (this.mqttClient != null && this.mqttClient.isConnected()) {
	    	    mqttClient.subscribe("gappdemo/greenhouse/#", 2);
	    	}
    	
    	} catch (MqttException me) {
			System.out.println("reason code: " + me.getReasonCode());
            System.out.println("message: " + me.getMessage());
            System.out.println("localized message: " + me.getLocalizedMessage());
            System.out.println("cause: " + me.getCause());
            System.out.println("exception: " + me);
            me.printStackTrace();
		}
    	
    	
    }
    
    @PreDestroy
    public void destroy() {
    	if (this.mqttClient != null) {
    		String serverURI = mqttClient.getServerURI();
    		try {
    			if (this.mqttClient.isConnected()) {
    				this.mqttClient.disconnect();
    				System.out.println("Paho client is disconnected from " + serverURI);
    			}
    		} catch (MqttException e) {
    			// eat this up on purpose
    		} finally {
    			connected = false;
    		}
    	} else {
    		connected = false;
    	}
    }

    /**
     * 
     */
    private void reconnect() {
    	
    	System.out.println("try to reconnect to MQTT broker ...");
    	
		this.tryConnecting = true;
		destroy();
		init();  // do all init operations again
    }
    
    /**
     * 
     */
    private void connect() {

		int attempts = 0;
		while (tryConnecting && attempts < 5) {
			try {
				attempts++;
				mqttClient.connect(connectionOptions);
			} catch (Throwable th) {
				eventBus.publish( "/connection", new ConnectionStatus("off") );
				th.printStackTrace();
				// eat this and ongoingly continue to try to connect to the broker
			}
			
			if (mqttClient.isConnected()) {
				timeOfConnection = new Date();
				tryConnecting = false;
				connected = true;
				eventBus.publish( "/connection", new ConnectionStatus("on") );
				System.out.println("Paho client is connected to " + mqttClient.getServerURI());
			} else {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
			}
		}
    }
    
    /**
     * @throws MqttException 
     * 
     */
    private void initClient() throws MqttException {

//        String broker         = "tcp://iot.eclipse.org:1883";  // Eclipse sample broker
        String broker         = "tcp://localhost:1883";
        String clientId       = "GreenhouseCloudStorage";  // TODO add mac-address to client id
        MemoryPersistence persistence = new MemoryPersistence();
        
		this.mqttClient = new MqttClient(broker, clientId, persistence);
		this.mqttClient.setCallback(this);
    }

    /**
     * 
     */
    private void initConnectionOptions() {
    	connectionOptions.setCleanSession(false);
    	connectionOptions.setKeepAliveInterval(120);
    }

	@Override
	public void connectionLost(Throwable cause) {
		System.out.println("lost MQTT connection due to:" + cause.getMessage());
		cause.printStackTrace();
		connected = false;
		long timeDiff = new Date().getTime() - this.timeOfConnection.getTime();
		System.out.println(timeDiff + " milliseconds since the previous successful connection");
		
		eventBus.publish( "/connection", new ConnectionStatus("off") );
		
		if (timeDiff > 5000) {
			reconnect();
		}
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// TODO Auto-generated method stub
		System.out.println("delivery complete, token message id:" + token.getMessageId());
	}

	/* (non-Javadoc)
	 * @see org.eclipse.paho.client.mqttv3.MqttCallback#messageArrived(java.lang.String, org.eclipse.paho.client.mqttv3.MqttMessage)
	 */
	@Override
	public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("got a new message in PahoClienht:" + mqttMessage);
		
		Message message = new Message(topic, new String(mqttMessage.getPayload()));
		message.setMessageArrival(new Date());
		message.setQos(mqttMessage.getQos());
		message.setDuplicate(mqttMessage.isDuplicate());
		message.setRetained(mqttMessage.isRetained());
		eventBus.publish( "/messages", message );
		
		MQTTMessageProcessor messageProcessor = null;
		try {
			InitialContext initialContext = new InitialContext();
			messageProcessor = (MQTTMessageProcessor) initialContext.lookup("java:global/GreenhouseCloudStorage-JSF/MQTTMessageProcessor");
			if (messageProcessor != null) {
				messageProcessor.processMessage(topic, mqttMessage, message.getMessageArrival());
			} else {
				throw new RuntimeException("was not able to obtain a message processor object to process this message:" + mqttMessage.toString());
			}
		} catch (IllegalArgumentException | NamingException ex) {
			ex.printStackTrace();
			throw new RuntimeException("was not able to obtain a message processor object to process this message:" + mqttMessage.toString(), ex);
		} catch (EJBException ex) {
			ex.printStackTrace();
		}
	}

	public MqttClient getMqttClient() {
		return mqttClient;
	}

	public void setMqttClient(MqttClient mqttClient) {
		this.mqttClient = mqttClient;
	}

	public MqttConnectOptions getConnectionOptions() {
		return connectionOptions;
	}

	public void setConnectionOptions(MqttConnectOptions connectionOptions) {
		this.connectionOptions = connectionOptions;
	}
	
	public boolean isConnected() {
		return this.connected; 
	}
	
	public void setConnected(boolean connected) {
		this.connected = connected;
	}
	
	public void actionChangeConnectionStatus(AjaxBehaviorEvent ajaxBehaviorEvent) {
		if (connected) {
			reconnect();
		} else {
			destroy();
		}
	}
	
	public void refreshConnectionStatus() {}
}
