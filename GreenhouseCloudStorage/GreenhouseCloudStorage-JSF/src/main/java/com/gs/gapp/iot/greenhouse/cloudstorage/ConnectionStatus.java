package com.gs.gapp.iot.greenhouse.cloudstorage;

public class ConnectionStatus {

	private final String status;
	
	
	public ConnectionStatus(String status) {
		super();
		this.status = status;
	}


	public String getStatus() {
		return status;
	}


	@Override
	public String toString() {
		return "Connection [status=" + status + "]";
	}
}
