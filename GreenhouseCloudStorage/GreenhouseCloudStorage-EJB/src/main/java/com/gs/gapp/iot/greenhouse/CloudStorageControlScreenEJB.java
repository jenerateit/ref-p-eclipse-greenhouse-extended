package com.gs.gapp.iot.greenhouse;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import com.vd.AbstractEJB;
import com.gs.gapp.iot.greenhouse.cloudstorageapplication.CloudStorageControlScreenDto;
import com.vd.SessionDataHolder;

//DA-START:greenhouse.CloudStorageControlScreenEJB:DA-START
//DA-ELSE:greenhouse.CloudStorageControlScreenEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:greenhouse.CloudStorageControlScreenEJB:DA-END

public class CloudStorageControlScreenEJB extends AbstractEJB { // start of class

    /**
     * creates an instance of CloudStorageControlScreenEJB
     */
    public CloudStorageControlScreenEJB() {
        //DA-START:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB:DA-ELSE
        super();
        //DA-END:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB:DA-END
    }
    
    /**
     * load the initial data that a managed bean then uses to serve one or more xhtml pages to fill it with dynamic data
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB.load.CloudStorageControlScreenDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB.load.CloudStorageControlScreenDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB.load.CloudStorageControlScreenDto.SessionDataHolder.annotations:DA-END
    public CloudStorageControlScreenDto load(CloudStorageControlScreenDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB.load.CloudStorageControlScreenDto.SessionDataHolder.CloudStorageControlScreenDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB.load.CloudStorageControlScreenDto.SessionDataHolder.CloudStorageControlScreenDto:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB.load.CloudStorageControlScreenDto.SessionDataHolder.CloudStorageControlScreenDto:DA-END
    }
    /**
     * update the screen's main dto instance
     * 
     * @param dto  the dto
     * @param sessionDataHolder  the sessionDataHolder
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB.update.CloudStorageControlScreenDto.SessionDataHolder.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB.update.CloudStorageControlScreenDto.SessionDataHolder.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB.update.CloudStorageControlScreenDto.SessionDataHolder.annotations:DA-END
    public CloudStorageControlScreenDto update(CloudStorageControlScreenDto dto, SessionDataHolder sessionDataHolder) {
        //DA-START:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB.update.CloudStorageControlScreenDto.SessionDataHolder.CloudStorageControlScreenDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB.update.CloudStorageControlScreenDto.SessionDataHolder.CloudStorageControlScreenDto:DA-ELSE
        return null;
        //DA-END:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB.update.CloudStorageControlScreenDto.SessionDataHolder.CloudStorageControlScreenDto:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.CloudStorageControlScreenEJB.additional.elements.in.type:DA-END
} // end of java type