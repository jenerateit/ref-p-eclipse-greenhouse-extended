package com.gs.gapp.iot.greenhouse.cloudstorageapplication;


import com.vd.AbstractDto;

/**
 * Dto class for UIStructuralContainer ('layout' element) 'CloudStorageControlScreen' in module 'CloudStorageApplication'.
 * Support for dirty flag is added by the DTO generator.
 */
public class CloudStorageControlScreenDto extends AbstractDto { // start of class

    
    //DA-START:com.gs.gapp.iot.greenhouse.cloudstorageapplication.CloudStorageControlScreenDto.fields:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cloudstorageapplication.CloudStorageControlScreenDto.fields:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.cloudstorageapplication.CloudStorageControlScreenDto.fields:DA-END
    /**
     * creates an instance of CloudStorageControlScreenDto
     */
    public CloudStorageControlScreenDto() {
        //DA-START:com.gs.gapp.iot.greenhouse.cloudstorageapplication.CloudStorageControlScreenDto:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cloudstorageapplication.CloudStorageControlScreenDto:DA-ELSE
        super();
        //DA-END:com.gs.gapp.iot.greenhouse.cloudstorageapplication.CloudStorageControlScreenDto:DA-END
    }
    
    /**
     * 
     * @return
     */
    public boolean isDirty() {
        //DA-START:com.gs.gapp.iot.greenhouse.cloudstorageapplication.CloudStorageControlScreenDto.isDirty.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.cloudstorageapplication.CloudStorageControlScreenDto.isDirty.boolean:DA-ELSE
        boolean result = super.isDirty();
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.cloudstorageapplication.CloudStorageControlScreenDto.isDirty.boolean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.cloudstorageapplication.CloudStorageControlScreenDto.methods:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cloudstorageapplication.CloudStorageControlScreenDto.methods:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.cloudstorageapplication.CloudStorageControlScreenDto.methods:DA-END
    
    //DA-START:com.gs.gapp.iot.greenhouse.cloudstorageapplication.CloudStorageControlScreenDto.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.cloudstorageapplication.CloudStorageControlScreenDto.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.cloudstorageapplication.CloudStorageControlScreenDto.additional.elements.in.type:DA-END
} // end of java type