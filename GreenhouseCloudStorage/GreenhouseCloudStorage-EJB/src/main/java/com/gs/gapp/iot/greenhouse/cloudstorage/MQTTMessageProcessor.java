package com.gs.gapp.iot.greenhouse.cloudstorage;

import java.util.Date;
import java.util.concurrent.Future;

import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gs.gapp.iot.greenhouse.basic.SensorDataBean;
import com.gs.gapp.iot.greenhouse.function.SensorDataService;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * Session Bean implementation class MQTTMessageProcessor
 */
@Stateless
@Asynchronous
public class MQTTMessageProcessor {

    /**
     * Default constructor. 
     */
    public MQTTMessageProcessor() {}

    /**
     * @param message
     * @return
     */
    public Future<ProcessingResult> processMessage(String topic, MqttMessage message, Date messageArrival) {
    	ProcessingResult processingResult = new ProcessingResult();
    	SensorDataBean sensorData = new SensorDataBean();
    	// TODO assign app id a
    	sensorData.setAppId("greenhouse-kura-app");  // TODO get this from the topic
    	sensorData.setClientId("greenhouse-raspberry-pi");  // TODO get this from the topic
    	sensorData.setResourceId(topic);
    	sensorData.setValue(new String(message.getPayload()));
    	sensorData.setTimeOfMeasurement(messageArrival);
    	
    	Gson gson = new GsonBuilder()
    			.setDateFormat("yyyy-MM-dd HH:mm:ss")
    			.create();
    	
    	RestAdapter restAdapter = new RestAdapter.Builder()
    			.setLogLevel(RestAdapter.LogLevel.FULL)
    			.setEndpoint("http://localhost:8080/GreenhouseRestApi-JAX-RS/greenhouse/v1")
    			.setConverter(new GsonConverter(gson))
    			.build();
    	
    	SensorDataService sensorDataService = restAdapter.create(SensorDataService.class);
    	System.out.println("time of measurement in MQTTMessageProcessor:" + sensorData.getTimeOfMeasurement());
    	sensorDataService.createSensorData(sensorData, new CreateSensorDataCallback());
    	
    	return new AsyncResult<>(processingResult);
    }
    
    /**
     * @author mmt
     *
     */
    public static class ProcessingResult {}
    
    /**
     * @author mmt
     *
     */
    public class CreateSensorDataCallback implements Callback<Object> {

		@Override
		public void failure(RetrofitError retrofitError) {
			retrofitError.printStackTrace();
		}

		@Override
		public void success(Object o, Response response) {
			System.out.println("successfully created sensor data through REST API");
		}
    }
}
