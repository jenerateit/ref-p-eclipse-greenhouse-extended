/**
 * 
 */
package org.eclipse.iot.greenhouse.sensors;

import java.util.HashMap;
import java.util.Map;

/**
 * @author mmt
 *
 */
public enum Actuator {
	
	LIGHT ("light"),
	;
	
	private static Map<String, Actuator> map = new HashMap<>();
	
	public static Actuator forName(String name) {
		return map.get(name.toLowerCase());
	}
	
	static {
		for (Actuator sensor : Actuator.values()) {
			map.put(sensor.getName().toLowerCase(), sensor);
		}
	}
	
	private final String name;
	
	private Actuator(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
