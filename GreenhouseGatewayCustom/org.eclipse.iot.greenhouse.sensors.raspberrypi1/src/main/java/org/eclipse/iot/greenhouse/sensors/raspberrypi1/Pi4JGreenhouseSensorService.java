package org.eclipse.iot.greenhouse.sensors.raspberrypi1;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.eclipse.iot.greenhouse.sensors1.Actuator;
import org.eclipse.iot.greenhouse.sensors1.Sensor;
import org.eclipse.iot.greenhouse.sensors1.SensorChangedListener;
import org.eclipse.iot.greenhouse.sensors1.SensorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalMultipurpose;
import com.pi4j.io.gpio.PinMode;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;

/**
 *
 */
public class Pi4JGreenhouseSensorService implements SensorService {
	
	private static final Logger s_logger = LoggerFactory.getLogger(Pi4JGreenhouseSensorService.class);
	
	private static DecimalFormat TWO_DECIMALS_FORMAT = new DecimalFormat("#.##");

	private List<SensorChangedListener> _listeners = new CopyOnWriteArrayList<>();
	private I2CBus _i2cbus;
	private I2CDevice _temperatureAndHumiditySensor;
	private GpioController _gpioController;
	private GpioPinDigitalMultipurpose _lightActuator;

	private float _temperatureRef = Float.MIN_VALUE;
	private float _humidityRef = Float.MIN_VALUE;

	private ScheduledThreadPoolExecutor _scheduledThreadPoolExecutor;
	private ScheduledFuture<?> _handle;

	/**
	 * 
	 */
	protected void activate() {
		try {
			_gpioController = GpioFactory.getInstance();
			_i2cbus = I2CFactory.getInstance(I2CBus.BUS_1);

			_temperatureAndHumiditySensor = _i2cbus.getDevice(0x40);
			_lightActuator = _gpioController.provisionDigitalMultipurposePin(RaspiPin.GPIO_00, "led", PinMode.DIGITAL_OUTPUT);
			_lightActuator.setShutdownOptions(true); // unexport on shutdown

			// monitor temperature changes
			// every change of more than 0.1C will notify SensorChangedListeners
			_scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1);
			_handle = _scheduledThreadPoolExecutor.scheduleAtFixedRate(
					new Runnable() {
						@Override
						public void run() {
							
							try {
								float newTemperature = readTemperature();
								if (Math.abs(_temperatureRef - newTemperature) > .1f) {
									notifyListeners(Sensor.TEMPERATURE.getName(), newTemperature);
									_temperatureRef = newTemperature;
								}
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							try {
								float newHumidity = readHumidity();
								if (Math.abs(_humidityRef - newHumidity) > 1.0f) {
									notifyListeners(Sensor.HUMIDITY.getName(), newHumidity);
									_humidityRef = newHumidity;
								}
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
						}
					}, 0, 200, TimeUnit.MILLISECONDS);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	protected void deactivate() {
		s_logger.info("Deactivating Pi4JGreenhouseSensorService...");

		if (_gpioController != null) {
			s_logger.info("... unexport all GPIOs");
			_gpioController.unexportAll();
			s_logger.info("... shutdown");
			_gpioController.shutdown();
			s_logger.info("... DONE.");
		}

		if (_handle != null) {
			_handle.cancel(true);
		}

		s_logger.info("Deactivating Pi4JGreenhouseSensorService... Done.");
	}

	/* (non-Javadoc)
	 * @see org.eclipse.iot.greenhouse.sensors.SensorService#getSensorValue(java.lang.String)
	 */
	@Override
	public Object getSensorValue(String sensorName) throws NoSuchSensorOrActuatorException {
		Sensor sensor = Sensor.forName(sensorName);
		if (sensor != null) {
			try {
				switch (sensor) {
				case HUMIDITY:
					return readHumidity();
				case LIGHT:
					return readLightState();
				case TEMPERATURE:
					return readTemperature();
				default:
					throw new SensorService.NoSuchSensorOrActuatorException();
				}
			} catch (IOException e) {
				throw new SensorService.NoSuchSensorOrActuatorException();
			}
		} else {
			throw new SensorService.NoSuchSensorOrActuatorException();
		}
	}

	/*
	 * See sensor documentation here:
	 * http://www.hoperf.cn/upload/sensor/TH02_V1.1.pdf, chapter 3 (Host Interface)
	 * 
	 * @return
	 * @throws IOException
	 */
	private synchronized float readTemperature() throws IOException {
		float TEMP = 0.0f;
		float temperature = 0.0f;
		
		synchronized (_temperatureAndHumiditySensor) {
		
			// Set START (D0) and TEMP (D4) in CONFIG (register 0x03) to begin a
			// new conversion, i.e., write CONFIG with 0x11
			_temperatureAndHumiditySensor.write(0x03, (byte) 0x11);
	
			// Poll RDY (D0) in STATUS (register 0) until it is low (=0)
			int status = -1;
			while ((status & 0x01) != 0) {
				status = _temperatureAndHumiditySensor.read(0x00);
			}
	
			// Read the upper and lower bytes of the temperature value from
			// DATAh and DATAl (registers 0x01 and 0x02), respectively
			byte[] buffer = new byte[3];
			_temperatureAndHumiditySensor.read(buffer, 0, 3);
	
			int dataH = buffer[1] & 0xff;
			int dataL = buffer[2] & 0xff;
			
			TEMP = (dataH * 256 + dataL) >> 2;
			temperature = (TEMP / 32f) - 50f;
		}
		
		return Float.valueOf(TWO_DECIMALS_FORMAT.format(temperature));
	}
	
	/**
	 * See sensor documentation here:
	 * http://www.hoperf.cn/upload/sensor/TH02_V1.1.pdf, chapter 3 (Host Interface)
	 * 
	 * @return
	 * @throws IOException
	 */
	private synchronized float readHumidity() throws IOException {
		float RH = 0.0f;
		float relativeHumidity = 0.0f;
		
		synchronized (_temperatureAndHumiditySensor) {
		
			// Set START (D0) in CONFIG to begin a new conversion
			_temperatureAndHumiditySensor.write(0x03, (byte) 0x01);
	
			// Poll RDY (D0) in STATUS (register 0) until it is low (= 0)
			int status = -1;
			while ((status & 0x01) != 0) {
				status = _temperatureAndHumiditySensor.read(0x00);
			}
	
			// Read the upper and lower bytes of the RH value from
			// DATAh and DATAl (registers 0x01 and 0x02), respectively
			byte[] buffer = new byte[3];
			_temperatureAndHumiditySensor.read(buffer, 0, 3);
	
			int dataH = buffer[1] & 0xff;
			int dataL = buffer[2] & 0xff;
	
			
			RH = (dataH * 256 + dataL) >> 4;
			relativeHumidity = (RH / 16f) - 24f;
		}
		
		return Float.valueOf(TWO_DECIMALS_FORMAT.format(relativeHumidity));
	}

	/**
	 * @return
	 */
	private boolean readLightState() {
		return _lightActuator.getState().isHigh();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.iot.greenhouse.sensors.SensorService#setActuatorValue(java.lang.String, java.lang.Object)
	 */
	@Override
	public void setActuatorValue(String actuatorName, Object value)	throws NoSuchSensorOrActuatorException {
		Actuator actuator = Actuator.forName(actuatorName);
		if (actuator != null) {
			switch (actuator) {
			case LIGHT:
				boolean switchLightOn = "on".equals(value.toString().toLowerCase());
				_lightActuator.setState(switchLightOn);
				notifyListeners(Actuator.LIGHT.getName(), switchLightOn ? "on" : "off");
				break;
			default:
				throw new SensorService.NoSuchSensorOrActuatorException();
			}
		} else {
			throw new SensorService.NoSuchSensorOrActuatorException();
		}
	}

	public void addSensorChangedListener(SensorChangedListener listener) {
		_listeners.add(listener);
	}

	public void removeSensorChangedListener(SensorChangedListener listener) {
		_listeners.remove(listener);
	}

	/**
	 * Notify all listeners that had been added by means of OSGi
	 * 
	 * @param sensorName
	 * @param newValue
	 */
	private void notifyListeners(String sensorName, Object newValue) {
		for (SensorChangedListener listener : _listeners) {
			listener.sensorChanged(sensorName, newValue);
		}
	}
}
