package org.eclipse.iot.greenhouse.sensors1;

public interface SensorChangedListener {
	
	/**
	 * Callback called when the sensor value has been updated do to an
	 * external event
	 * 
	 * @param sensorName name of the sensor that has a changed value
	 * @param newValue new sensor value
	 */
	void sensorChanged(String sensorName, Object newValue);
}