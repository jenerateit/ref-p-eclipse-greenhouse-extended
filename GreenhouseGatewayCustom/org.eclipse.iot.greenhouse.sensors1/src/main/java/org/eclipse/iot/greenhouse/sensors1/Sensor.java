/**
 * 
 */
package org.eclipse.iot.greenhouse.sensors1;

import java.util.HashMap;
import java.util.Map;

/**
 * @author mmt
 *
 */
public enum Sensor {
	
	TEMPERATURE ("temperature"),
	HUMIDITY ("humidity"),
	LIGHT ("light"),
	;
	
	private static Map<String, Sensor> map = new HashMap<>();
	
	public static Sensor forName(String name) {
		return map.get(name.toLowerCase());
	}
	
	static {
		for (Sensor sensor : Sensor.values()) {
			map.put(sensor.getName().toLowerCase(), sensor);
		}
	}
	
	private final String name;
	
	private Sensor(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
