package org.eclipse.iot.greenhouse.sensors1;

/**
 *
 */
public interface SensorService {
	
	class NoSuchSensorOrActuatorException extends Exception {
		private static final long serialVersionUID = 2612352095893222404L;
	};

	/**
	 * @param sensorName
	 * @return current sensor value
	 * @throws NoSuchSensorOrActuatorException
	 */
	Object getSensorValue(String sensorName) throws NoSuchSensorOrActuatorException;

	/**
	 * @param actuatorName
	 * @param value
	 * @throws NoSuchSensorOrActuatorException
	 */
	void setActuatorValue(String actuatorName, Object value) throws NoSuchSensorOrActuatorException;

}
