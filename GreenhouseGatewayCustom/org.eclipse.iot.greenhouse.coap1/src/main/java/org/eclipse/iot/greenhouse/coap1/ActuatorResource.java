package org.eclipse.iot.greenhouse.coap1;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.eclipse.iot.greenhouse.sensors1.SensorService;
import org.eclipse.iot.greenhouse.sensors1.SensorService.NoSuchSensorOrActuatorException;

/**
 *
 */
public class ActuatorResource extends CoapResource {
	
	private SensorService _greenhouseSensorService;
	
	// an actuator might also allow its status to be GET, in that case it has an
	// associated SensorResource
	private SensorResource _sensorResource;

	public ActuatorResource(String name, SensorService sensorService) {
		super(name);
		_greenhouseSensorService = sensorService;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.californium.core.CoapResource#handleGET(org.eclipse.californium.core.server.resources.CoapExchange)
	 */
	@Override
	public void handleGET(CoapExchange exchange) {
		if (_sensorResource != null) {
			_sensorResource.handleGET(exchange);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.californium.core.CoapResource#handlePUT(org.eclipse.californium.core.server.resources.CoapExchange)
	 */
	@Override
	public void handlePUT(CoapExchange exchange) {
		String command = exchange.getRequestText();

		try {
			_greenhouseSensorService.setActuatorValue("light", command);
		} catch (NoSuchSensorOrActuatorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		exchange.respond(ResponseCode.CHANGED);
	}

	/**
	 * @return
	 */
	public SensorResource getSensorResource() {
		return _sensorResource;
	}

	/**
	 * @param sensorResource
	 */
	public void setSensorResource(SensorResource sensorResource) {
		this._sensorResource = sensorResource;
	}
}
