package com.vd;


import java.text.SimpleDateFormat;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Provider
@Produces(value={"application/json"})
@Consumes(value={"application/json"})
public class JacksonConfigurator implements ContextResolver<ObjectMapper> { // start of class

    private ObjectMapper mapper = new ObjectMapper();
    
    /**
     * creates an instance of JacksonConfigurator
     */
    public JacksonConfigurator() {
        //DA-START:com.vd.JacksonConfigurator:DA-START
        //DA-ELSE:com.vd.JacksonConfigurator:DA-ELSE
        initSerializationFeatures();
        initDeserializationFeatures();
        //DA-END:com.vd.JacksonConfigurator:DA-END
    }
    
    /**
     */
    private void initSerializationFeatures() {
        //DA-START:com.vd.JacksonConfigurator.initSerializationFeatures:DA-START
        //DA-ELSE:com.vd.JacksonConfigurator.initSerializationFeatures:DA-ELSE
        String sqliteDateFormat = "yyyy-MM-dd HH:mm:ss";
        mapper.setDateFormat(new SimpleDateFormat(sqliteDateFormat));
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.configure(SerializationFeature.WRITE_ENUMS_USING_INDEX, false);
        mapper.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, false);
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.configure(SerializationFeature.WRITE_BIGDECIMAL_AS_PLAIN, false);
        mapper.configure(SerializationFeature.WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS, false);
        mapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, true);
        // if this is true, collections with only one object will _not_ be serialized as a JSON array 
        mapper.configure(SerializationFeature.WRITE_SINGLE_ELEM_ARRAYS_UNWRAPPED, false);
        //DA-END:com.vd.JacksonConfigurator.initSerializationFeatures:DA-END
    }
    /**
     */
    private void initDeserializationFeatures() {
        //DA-START:com.vd.JacksonConfigurator.initDeserializationFeatures:DA-START
    	mapper.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false);
    	mapper.configure(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS, false);
        mapper.configure(DeserializationFeature.READ_ENUMS_USING_TO_STRING, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);
        mapper.configure(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS, false);
        mapper.configure(DeserializationFeature.USE_BIG_INTEGER_FOR_INTS, false);
        mapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, false);
        mapper.getDeserializationConfig().with(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));  // added in order to try out why date deserialization does not work - no success :(
        //DA-ELSE:com.vd.JacksonConfigurator.initDeserializationFeatures:DA-ELSE
//        mapper.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false);
//        mapper.configure(DeserializationFeature.READ_ENUMS_USING_TO_STRING, false);
//        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);
//        mapper.configure(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS, false);
//        mapper.configure(DeserializationFeature.USE_BIG_INTEGER_FOR_INTS, false);
//        mapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, false);
        //DA-END:com.vd.JacksonConfigurator.initDeserializationFeatures:DA-END
    }
    /**
     * 
     * @param clazz  the clazz
     * @return
     */
    @Override
    public ObjectMapper getContext(Class<?> clazz) {
        //DA-START:com.vd.JacksonConfigurator.getContext.Class.ObjectMapper:DA-START
        //DA-ELSE:com.vd.JacksonConfigurator.getContext.Class.ObjectMapper:DA-ELSE
        return this.mapper;
        //DA-END:com.vd.JacksonConfigurator.getContext.Class.ObjectMapper:DA-END
    }
    
    //DA-START:com.vd.JacksonConfigurator.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.JacksonConfigurator.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.JacksonConfigurator.additional.elements.in.type:DA-END
} // end of java type