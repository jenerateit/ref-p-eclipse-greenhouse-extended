package com.vd;


import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;
import java.lang.Object;
import java.util.HashSet;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import java.lang.Class;
import java.util.LinkedHashSet;

@ApplicationPath(value="greenhouse/v1")
public class RestApplication extends Application { // start of class

    /**
     * 
     * @return
     */
    public Set<Object> getSingletons() {
        //DA-START:com.vd.RestApplication.getSingletons.Class:DA-START
        //DA-ELSE:com.vd.RestApplication.getSingletons.Class:DA-ELSE
        Set<Object> singletons = new HashSet<Object>();
        singletons.add(new JacksonJaxbJsonProvider());
        return singletons;
        //DA-END:com.vd.RestApplication.getSingletons.Class:DA-END
    }
    /**
     * 
     * @return
     */
    public Set<Class<?>> getClasses() {
        //DA-START:com.vd.RestApplication.getClasses.Class:DA-START
        //DA-ELSE:com.vd.RestApplication.getClasses.Class:DA-ELSE
        Set<Class<?>> result = new LinkedHashSet<Class<?>>();
        result.add(com.gs.gapp.iot.greenhouse.function.SensorDataResource.class);
        result.add(com.gs.gapp.iot.greenhouse.function.SensorResource.class);
        result.add(com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.class);
        result.add(com.gs.gapp.iot.greenhouse.function.UnitResource.class);
        result.add(JacksonConfigurator.class);
        return result;
        //DA-END:com.vd.RestApplication.getClasses.Class:DA-END
    }
    
    //DA-START:com.vd.RestApplication.additional.elements.in.type:DA-START
    //DA-ELSE:com.vd.RestApplication.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.vd.RestApplication.additional.elements.in.type:DA-END
} // end of java type