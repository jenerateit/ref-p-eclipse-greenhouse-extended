package com.gs.gapp.iot.greenhouse.function;


import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Request;
import javax.ws.rs.ext.Providers;
import javax.ejb.EJB;
import com.gs.gapp.iot.greenhouse.function.SensorEJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import java.lang.String;
import javax.ws.rs.GET;
import com.gs.gapp.iot.greenhouse.basic.SensorBean;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.WebApplicationException;
import com.gs.gapp.iot.greenhouse.basic.SensorListBean;
import javax.ws.rs.QueryParam;
import java.util.List;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.ResponseBuilder;

/**
 * Date that describes a sensor. When for instance 10 idientical sensors
 * are being used, there is only one single entry in the SENSOR table.
 */
//DA-START:function.SensorResource:DA-START
//DA-ELSE:function.SensorResource:DA-ELSE
@Path(value="/sensor")
@Produces(value={"application/xml", "application/json"})
@Consumes(value={"application/xml", "application/json"})
@RequestScoped
//DA-END:function.SensorResource:DA-END

public class SensorResource { // start of class

    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.uriInfo:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.uriInfo:DA-ELSE
    @Context
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.uriInfo:DA-END
    private UriInfo uriInfo;
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.httpHeaders:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.httpHeaders:DA-ELSE
    @Context
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.httpHeaders:DA-END
    private HttpHeaders httpHeaders;
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.securityContext:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.securityContext:DA-ELSE
    @Context
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.securityContext:DA-END
    private SecurityContext securityContext;
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.request:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.request:DA-ELSE
    @Context
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.request:DA-END
    private Request request;
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.providers:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.providers:DA-ELSE
    @Context
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.providers:DA-END
    private Providers providers;
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.sensorBean:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.sensorBean:DA-ELSE
    @EJB
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.sensorBean:DA-END
    private SensorEJB sensorBean;
    
    /**
     * 
     * @param id
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.delete.long.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.delete.long.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.delete.long.annotations:DA-END
    public void delete(@PathParam(value="id") long id) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.delete.long:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.delete.long:DA-ELSE
        sensorBean.delete(id);
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @param mediaType the media type
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.deleteByType.long.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.deleteByType.long.String.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.deleteByType.long.String.annotations:DA-END
    public void deleteByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.deleteByType.long.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.deleteByType.long.String:DA-ELSE
        sensorBean.delete(id);
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.deleteByType.long.String:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.read.long.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.read.long.annotations:DA-ELSE
    @GET
    @Path(value="/{id}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.read.long.annotations:DA-END
    public SensorBean read(@PathParam(value="id") long id) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.read.long.SensorBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.read.long.SensorBean:DA-ELSE
        SensorBean result = sensorBean.read(id);
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.read.long.SensorBean:DA-END
    }
    /**
     * 
     * @param id
     * @param mediaType the media type
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.readByType.long.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.readByType.long.String.annotations:DA-ELSE
    @GET
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.readByType.long.String.annotations:DA-END
    public Response readByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.readByType.long.Response.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.readByType.long.Response.String:DA-ELSE
        SensorBean result = sensorBean.read(id);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.readByType.long.Response.String:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.readList.int.int.List.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.readList.int.int.List.annotations:DA-ELSE
    @GET
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.readList.int.int.List.annotations:DA-END
    public SensorListBean readList(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.readList.int.int.List.SensorListBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.readList.int.int.List.SensorListBean:DA-ELSE
        SensorListBean result = sensorBean.readList(offset, limit, ids);
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.readList.int.int.List.SensorListBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @param mediaType the media type
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.readListByType.int.int.List.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.readListByType.int.int.List.String.annotations:DA-ELSE
    @GET
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.readListByType.int.int.List.String.annotations:DA-END
    public Response readListByType(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids, @PathParam(value="mediaType") String mediaType) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.readListByType.int.int.List.Response.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.readListByType.int.int.List.Response.String:DA-ELSE
        SensorListBean result = sensorBean.readList(offset, limit, ids);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.readListByType.int.int.List.Response.String:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.update.long.SensorBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.update.long.SensorBean.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.update.long.SensorBean.annotations:DA-END
    public SensorBean update(@PathParam(value="id") long id, SensorBean bean) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.update.long.SensorBean.SensorBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.update.long.SensorBean.SensorBean:DA-ELSE
        SensorBean result = sensorBean.update(id, bean);
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.update.long.SensorBean.SensorBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.updateByType.long.SensorBean.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.updateByType.long.SensorBean.String.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.updateByType.long.SensorBean.String.annotations:DA-END
    public Response updateByType(@PathParam(value="id") long id, SensorBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.updateByType.long.SensorBean.Response.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.updateByType.long.SensorBean.Response.String:DA-ELSE
        SensorBean result = sensorBean.update(id, bean);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.updateByType.long.SensorBean.Response.String:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.create.SensorBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.create.SensorBean.annotations:DA-ELSE
    @POST
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.create.SensorBean.annotations:DA-END
    public Response create(SensorBean bean) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.create.SensorBean.Response:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.create.SensorBean.Response:DA-ELSE
        SensorBean result = sensorBean.create(bean);
        String mediaType = httpHeaders.getRequestHeader(HttpHeaders.ACCEPT).get(0);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.create.SensorBean.Response:DA-END
    }
    /**
     * 
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.createByType.SensorBean.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.createByType.SensorBean.String.annotations:DA-ELSE
    @POST
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.createByType.SensorBean.String.annotations:DA-END
    public Response createByType(SensorBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.createByType.SensorBean.Response.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.createByType.SensorBean.Response.String:DA-ELSE
        SensorBean result = sensorBean.create(bean);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.createByType.SensorBean.Response.String:DA-END
    }
    /**
     * setter for the field uriInfo
     * 
     * null
     * 
     * @param uriInfo  the uriInfo
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.setUriInfo.UriInfo.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.setUriInfo.UriInfo.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.setUriInfo.UriInfo.annotations:DA-END
    public void setUriInfo(UriInfo uriInfo) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.setUriInfo.UriInfo:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.setUriInfo.UriInfo:DA-ELSE
        this.uriInfo = uriInfo;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.setUriInfo.UriInfo:DA-END
    }
    /**
     * setter for the field httpHeaders
     * 
     * null
     * 
     * @param httpHeaders  the httpHeaders
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.setHttpHeaders.HttpHeaders.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.setHttpHeaders.HttpHeaders.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.setHttpHeaders.HttpHeaders.annotations:DA-END
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.setHttpHeaders.HttpHeaders:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.setHttpHeaders.HttpHeaders:DA-ELSE
        this.httpHeaders = httpHeaders;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.setHttpHeaders.HttpHeaders:DA-END
    }
    /**
     * setter for the field securityContext
     * 
     * null
     * 
     * @param securityContext  the securityContext
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.setSecurityContext.SecurityContext.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.setSecurityContext.SecurityContext.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.setSecurityContext.SecurityContext.annotations:DA-END
    public void setSecurityContext(SecurityContext securityContext) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.setSecurityContext.SecurityContext:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.setSecurityContext.SecurityContext:DA-ELSE
        this.securityContext = securityContext;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.setSecurityContext.SecurityContext:DA-END
    }
    /**
     * setter for the field request
     * 
     * null
     * 
     * @param request  the request
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.setRequest.Request.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.setRequest.Request.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.setRequest.Request.annotations:DA-END
    public void setRequest(Request request) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.setRequest.Request:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.setRequest.Request:DA-ELSE
        this.request = request;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.setRequest.Request:DA-END
    }
    /**
     * setter for the field providers
     * 
     * null
     * 
     * @param providers  the providers
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.setProviders.Providers.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.setProviders.Providers.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.setProviders.Providers.annotations:DA-END
    public void setProviders(Providers providers) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.setProviders.Providers:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.setProviders.Providers:DA-ELSE
        this.providers = providers;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.setProviders.Providers:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorResource.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorResource.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorResource.additional.elements.in.type:DA-END
} // end of java type