package com.gs.gapp.iot.greenhouse.function;


import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Request;
import javax.ws.rs.ext.Providers;
import javax.ejb.EJB;
import com.gs.gapp.iot.greenhouse.function.UnitEJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import java.lang.String;
import javax.ws.rs.GET;
import com.gs.gapp.iot.greenhouse.basic.UnitBean;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.WebApplicationException;
import com.gs.gapp.iot.greenhouse.basic.UnitListBean;
import javax.ws.rs.QueryParam;
import java.util.List;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.ResponseBuilder;

//DA-START:function.UnitResource:DA-START
//DA-ELSE:function.UnitResource:DA-ELSE
@Path(value="/unit")
@Produces(value={"application/xml", "application/json"})
@Consumes(value={"application/xml", "application/json"})
@RequestScoped
//DA-END:function.UnitResource:DA-END

public class UnitResource { // start of class

    
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.uriInfo:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.uriInfo:DA-ELSE
    @Context
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.uriInfo:DA-END
    private UriInfo uriInfo;
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.httpHeaders:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.httpHeaders:DA-ELSE
    @Context
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.httpHeaders:DA-END
    private HttpHeaders httpHeaders;
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.securityContext:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.securityContext:DA-ELSE
    @Context
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.securityContext:DA-END
    private SecurityContext securityContext;
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.request:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.request:DA-ELSE
    @Context
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.request:DA-END
    private Request request;
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.providers:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.providers:DA-ELSE
    @Context
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.providers:DA-END
    private Providers providers;
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.unitBean:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.unitBean:DA-ELSE
    @EJB
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.unitBean:DA-END
    private UnitEJB unitBean;
    
    /**
     * 
     * @param id
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.delete.long.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.delete.long.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.delete.long.annotations:DA-END
    public void delete(@PathParam(value="id") long id) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.delete.long:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.delete.long:DA-ELSE
        unitBean.delete(id);
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @param mediaType the media type
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.deleteByType.long.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.deleteByType.long.String.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.deleteByType.long.String.annotations:DA-END
    public void deleteByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.deleteByType.long.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.deleteByType.long.String:DA-ELSE
        unitBean.delete(id);
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.deleteByType.long.String:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.read.long.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.read.long.annotations:DA-ELSE
    @GET
    @Path(value="/{id}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.read.long.annotations:DA-END
    public UnitBean read(@PathParam(value="id") long id) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.read.long.UnitBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.read.long.UnitBean:DA-ELSE
        UnitBean result = unitBean.read(id);
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.read.long.UnitBean:DA-END
    }
    /**
     * 
     * @param id
     * @param mediaType the media type
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.readByType.long.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.readByType.long.String.annotations:DA-ELSE
    @GET
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.readByType.long.String.annotations:DA-END
    public Response readByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.readByType.long.Response.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.readByType.long.Response.String:DA-ELSE
        UnitBean result = unitBean.read(id);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.readByType.long.Response.String:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.readList.int.int.List.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.readList.int.int.List.annotations:DA-ELSE
    @GET
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.readList.int.int.List.annotations:DA-END
    public UnitListBean readList(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.readList.int.int.List.UnitListBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.readList.int.int.List.UnitListBean:DA-ELSE
        UnitListBean result = unitBean.readList(offset, limit, ids);
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.readList.int.int.List.UnitListBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @param mediaType the media type
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.readListByType.int.int.List.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.readListByType.int.int.List.String.annotations:DA-ELSE
    @GET
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.readListByType.int.int.List.String.annotations:DA-END
    public Response readListByType(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids, @PathParam(value="mediaType") String mediaType) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.readListByType.int.int.List.Response.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.readListByType.int.int.List.Response.String:DA-ELSE
        UnitListBean result = unitBean.readList(offset, limit, ids);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.readListByType.int.int.List.Response.String:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.update.long.UnitBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.update.long.UnitBean.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.update.long.UnitBean.annotations:DA-END
    public UnitBean update(@PathParam(value="id") long id, UnitBean bean) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.update.long.UnitBean.UnitBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.update.long.UnitBean.UnitBean:DA-ELSE
        UnitBean result = unitBean.update(id, bean);
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.update.long.UnitBean.UnitBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.updateByType.long.UnitBean.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.updateByType.long.UnitBean.String.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.updateByType.long.UnitBean.String.annotations:DA-END
    public Response updateByType(@PathParam(value="id") long id, UnitBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.updateByType.long.UnitBean.Response.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.updateByType.long.UnitBean.Response.String:DA-ELSE
        UnitBean result = unitBean.update(id, bean);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.updateByType.long.UnitBean.Response.String:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.create.UnitBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.create.UnitBean.annotations:DA-ELSE
    @POST
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.create.UnitBean.annotations:DA-END
    public Response create(UnitBean bean) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.create.UnitBean.Response:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.create.UnitBean.Response:DA-ELSE
        UnitBean result = unitBean.create(bean);
        String mediaType = httpHeaders.getRequestHeader(HttpHeaders.ACCEPT).get(0);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.create.UnitBean.Response:DA-END
    }
    /**
     * 
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.createByType.UnitBean.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.createByType.UnitBean.String.annotations:DA-ELSE
    @POST
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.createByType.UnitBean.String.annotations:DA-END
    public Response createByType(UnitBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.createByType.UnitBean.Response.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.createByType.UnitBean.Response.String:DA-ELSE
        UnitBean result = unitBean.create(bean);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.createByType.UnitBean.Response.String:DA-END
    }
    /**
     * setter for the field uriInfo
     * 
     * null
     * 
     * @param uriInfo  the uriInfo
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.setUriInfo.UriInfo.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.setUriInfo.UriInfo.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.setUriInfo.UriInfo.annotations:DA-END
    public void setUriInfo(UriInfo uriInfo) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.setUriInfo.UriInfo:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.setUriInfo.UriInfo:DA-ELSE
        this.uriInfo = uriInfo;
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.setUriInfo.UriInfo:DA-END
    }
    /**
     * setter for the field httpHeaders
     * 
     * null
     * 
     * @param httpHeaders  the httpHeaders
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.setHttpHeaders.HttpHeaders.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.setHttpHeaders.HttpHeaders.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.setHttpHeaders.HttpHeaders.annotations:DA-END
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.setHttpHeaders.HttpHeaders:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.setHttpHeaders.HttpHeaders:DA-ELSE
        this.httpHeaders = httpHeaders;
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.setHttpHeaders.HttpHeaders:DA-END
    }
    /**
     * setter for the field securityContext
     * 
     * null
     * 
     * @param securityContext  the securityContext
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.setSecurityContext.SecurityContext.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.setSecurityContext.SecurityContext.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.setSecurityContext.SecurityContext.annotations:DA-END
    public void setSecurityContext(SecurityContext securityContext) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.setSecurityContext.SecurityContext:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.setSecurityContext.SecurityContext:DA-ELSE
        this.securityContext = securityContext;
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.setSecurityContext.SecurityContext:DA-END
    }
    /**
     * setter for the field request
     * 
     * null
     * 
     * @param request  the request
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.setRequest.Request.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.setRequest.Request.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.setRequest.Request.annotations:DA-END
    public void setRequest(Request request) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.setRequest.Request:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.setRequest.Request:DA-ELSE
        this.request = request;
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.setRequest.Request:DA-END
    }
    /**
     * setter for the field providers
     * 
     * null
     * 
     * @param providers  the providers
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.setProviders.Providers.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.setProviders.Providers.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.setProviders.Providers.annotations:DA-END
    public void setProviders(Providers providers) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.setProviders.Providers:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.setProviders.Providers:DA-ELSE
        this.providers = providers;
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.setProviders.Providers:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitResource.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitResource.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitResource.additional.elements.in.type:DA-END
} // end of java type