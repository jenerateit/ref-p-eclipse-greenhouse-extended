package com.gs.gapp.iot.greenhouse.function;


import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.Request;
import javax.ws.rs.ext.Providers;
import javax.ejb.EJB;
import com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import java.lang.String;
import javax.ws.rs.GET;
import com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.WebApplicationException;
import com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean;
import javax.ws.rs.QueryParam;
import java.util.List;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.Response.ResponseBuilder;
/*
 * Imports from last generation
 */
import com.gs.gapp.iot.greenhouse.basic.SensorDataListBean;


/**
 * A sensor instance relates to a real, physical sensor.
 * When for instance 10 identical sensors are being used,
 * there will be 10 entries for them in the SENSORINSTANCE table.
 */
//DA-START:function.SensorInstanceResource:DA-START
//DA-ELSE:function.SensorInstanceResource:DA-ELSE
@Path(value="/sensorinstance")
@Produces(value={"application/xml", "application/json"})
@Consumes(value={"application/xml", "application/json"})
@RequestScoped
//DA-END:function.SensorInstanceResource:DA-END

public class SensorInstanceResource { // start of class

    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.uriInfo:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.uriInfo:DA-ELSE
    @Context
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.uriInfo:DA-END
    private UriInfo uriInfo;
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.httpHeaders:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.httpHeaders:DA-ELSE
    @Context
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.httpHeaders:DA-END
    private HttpHeaders httpHeaders;
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.securityContext:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.securityContext:DA-ELSE
    @Context
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.securityContext:DA-END
    private SecurityContext securityContext;
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.request:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.request:DA-ELSE
    @Context
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.request:DA-END
    private Request request;
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.providers:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.providers:DA-ELSE
    @Context
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.providers:DA-END
    private Providers providers;
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.sensorInstanceBean:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.sensorInstanceBean:DA-ELSE
    @EJB
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.sensorInstanceBean:DA-END
    private SensorInstanceEJB sensorInstanceBean;
    
    /**
     * 
     * @param id
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.delete.long.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.delete.long.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.delete.long.annotations:DA-END
    public void delete(@PathParam(value="id") long id) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.delete.long:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.delete.long:DA-ELSE
        sensorInstanceBean.delete(id);
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @param mediaType the media type
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.deleteByType.long.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.deleteByType.long.String.annotations:DA-ELSE
    @DELETE
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.deleteByType.long.String.annotations:DA-END
    public void deleteByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.deleteByType.long.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.deleteByType.long.String:DA-ELSE
        sensorInstanceBean.delete(id);
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.deleteByType.long.String:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.read.long.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.read.long.annotations:DA-ELSE
    @GET
    @Path(value="/{id}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.read.long.annotations:DA-END
    public SensorInstanceBean read(@PathParam(value="id") long id) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.read.long.SensorInstanceBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.read.long.SensorInstanceBean:DA-ELSE
        SensorInstanceBean result = sensorInstanceBean.read(id);
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.read.long.SensorInstanceBean:DA-END
    }
    /**
     * 
     * @param id
     * @param mediaType the media type
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readByType.long.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readByType.long.String.annotations:DA-ELSE
    @GET
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readByType.long.String.annotations:DA-END
    public Response readByType(@PathParam(value="id") long id, @PathParam(value="mediaType") String mediaType) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readByType.long.Response.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readByType.long.Response.String:DA-ELSE
        SensorInstanceBean result = sensorInstanceBean.read(id);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readByType.long.Response.String:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readList.int.int.List.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readList.int.int.List.annotations:DA-ELSE
    @GET
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readList.int.int.List.annotations:DA-END
    public SensorInstanceListBean readList(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readList.int.int.List.SensorInstanceListBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readList.int.int.List.SensorInstanceListBean:DA-ELSE
        SensorInstanceListBean result = sensorInstanceBean.readList(offset, limit, ids);
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readList.int.int.List.SensorInstanceListBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @param mediaType the media type
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readListByType.int.int.List.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readListByType.int.int.List.String.annotations:DA-ELSE
    @GET
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readListByType.int.int.List.String.annotations:DA-END
    public Response readListByType(@QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids, @PathParam(value="mediaType") String mediaType) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readListByType.int.int.List.Response.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readListByType.int.int.List.Response.String:DA-ELSE
        SensorInstanceListBean result = sensorInstanceBean.readList(offset, limit, ids);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.readListByType.int.int.List.Response.String:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.update.long.SensorInstanceBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.update.long.SensorInstanceBean.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.update.long.SensorInstanceBean.annotations:DA-END
    public SensorInstanceBean update(@PathParam(value="id") long id, SensorInstanceBean bean) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.update.long.SensorInstanceBean.SensorInstanceBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.update.long.SensorInstanceBean.SensorInstanceBean:DA-ELSE
        SensorInstanceBean result = sensorInstanceBean.update(id, bean);
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.update.long.SensorInstanceBean.SensorInstanceBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.updateByType.long.SensorInstanceBean.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.updateByType.long.SensorInstanceBean.String.annotations:DA-ELSE
    @PUT
    @Path(value="/{id}.{mediaType : (json|xml)?}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.updateByType.long.SensorInstanceBean.String.annotations:DA-END
    public Response updateByType(@PathParam(value="id") long id, SensorInstanceBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.updateByType.long.SensorInstanceBean.Response.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.updateByType.long.SensorInstanceBean.Response.String:DA-ELSE
        SensorInstanceBean result = sensorInstanceBean.update(id, bean);
        if ("json".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } else if ("xml".equals(mediaType)) {
            return Response.ok(result, MediaType.APPLICATION_XML).build();
        } else {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.updateByType.long.SensorInstanceBean.Response.String:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.create.SensorInstanceBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.create.SensorInstanceBean.annotations:DA-ELSE
    @POST
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.create.SensorInstanceBean.annotations:DA-END
    public Response create(SensorInstanceBean bean) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.create.SensorInstanceBean.Response:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.create.SensorInstanceBean.Response:DA-ELSE
        SensorInstanceBean result = sensorInstanceBean.create(bean);
        String mediaType = httpHeaders.getRequestHeader(HttpHeaders.ACCEPT).get(0);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.create.SensorInstanceBean.Response:DA-END
    }
    /**
     * 
     * @param bean
     * @param mediaType the media type
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.createByType.SensorInstanceBean.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.createByType.SensorInstanceBean.String.annotations:DA-ELSE
    @POST
    @Path(value="/.{mediaType : (json|xml)?}")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.createByType.SensorInstanceBean.String.annotations:DA-END
    public Response createByType(SensorInstanceBean bean, @PathParam(value="mediaType") String mediaType) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.createByType.SensorInstanceBean.Response.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.createByType.SensorInstanceBean.Response.String:DA-ELSE
        SensorInstanceBean result = sensorInstanceBean.create(bean);
        if (!MediaType.APPLICATION_JSON.equals(mediaType) && !MediaType.APPLICATION_XML.equals(mediaType)) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        ResponseBuilder responseBuilder = Response.created(uriBuilder.build()).type(mediaType).entity(result);
        Response response = responseBuilder.build();
        
        return response;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.createByType.SensorInstanceBean.Response.String:DA-END
    }
    /**
     * setter for the field uriInfo
     * 
     * null
     * 
     * @param uriInfo  the uriInfo
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setUriInfo.UriInfo.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setUriInfo.UriInfo.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setUriInfo.UriInfo.annotations:DA-END
    public void setUriInfo(UriInfo uriInfo) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setUriInfo.UriInfo:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setUriInfo.UriInfo:DA-ELSE
        this.uriInfo = uriInfo;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setUriInfo.UriInfo:DA-END
    }
    /**
     * setter for the field httpHeaders
     * 
     * null
     * 
     * @param httpHeaders  the httpHeaders
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setHttpHeaders.HttpHeaders.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setHttpHeaders.HttpHeaders.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setHttpHeaders.HttpHeaders.annotations:DA-END
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setHttpHeaders.HttpHeaders:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setHttpHeaders.HttpHeaders:DA-ELSE
        this.httpHeaders = httpHeaders;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setHttpHeaders.HttpHeaders:DA-END
    }
    /**
     * setter for the field securityContext
     * 
     * null
     * 
     * @param securityContext  the securityContext
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setSecurityContext.SecurityContext.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setSecurityContext.SecurityContext.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setSecurityContext.SecurityContext.annotations:DA-END
    public void setSecurityContext(SecurityContext securityContext) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setSecurityContext.SecurityContext:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setSecurityContext.SecurityContext:DA-ELSE
        this.securityContext = securityContext;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setSecurityContext.SecurityContext:DA-END
    }
    /**
     * setter for the field request
     * 
     * null
     * 
     * @param request  the request
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setRequest.Request.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setRequest.Request.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setRequest.Request.annotations:DA-END
    public void setRequest(Request request) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setRequest.Request:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setRequest.Request:DA-ELSE
        this.request = request;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setRequest.Request:DA-END
    }
    /**
     * setter for the field providers
     * 
     * null
     * 
     * @param providers  the providers
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setProviders.Providers.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setProviders.Providers.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setProviders.Providers.annotations:DA-END
    public void setProviders(Providers providers) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setProviders.Providers:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setProviders.Providers:DA-ELSE
        this.providers = providers;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.setProviders.Providers:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.additional.elements.in.type:DA-START
    @EJB
    private SensorDataEJB sensorDataBean;
    
    /**
     * @param sensorInstanceId
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    @GET
    @Path(value="/{id}/sensordata")
    public SensorDataListBean readListSensorData(@PathParam(value="id") String sensorInstanceId, @QueryParam(value="offset") int offset, @QueryParam(value="limit") int limit, @QueryParam(value="ids") List<String> ids) {
        SensorDataListBean result = sensorDataBean.readListBySensorInstanceId(sensorInstanceId, offset, limit, ids);
        return result;
    }
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceResource.additional.elements.in.type:DA-END
} // end of java type