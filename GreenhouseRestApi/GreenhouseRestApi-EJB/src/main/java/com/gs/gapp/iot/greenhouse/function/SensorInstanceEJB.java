package com.gs.gapp.iot.greenhouse.function;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import com.gs.gapp.iot.greenhouse.SensorInstanceDAO;
import com.gs.gapp.iot.greenhouse.SensorInstance;
import com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean;
import com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean;
import java.util.List;
import java.lang.String;
import com.gs.gapp.iot.greenhouse.Sensor;
import com.gs.gapp.iot.greenhouse.basic.SensorBean;
import java.util.Map;
import java.lang.Object;
import java.util.LinkedHashMap;

/**
 * A sensor instance relates to a real, physical sensor.
 * When for instance 10 identical sensors are being used,
 * there will be 10 entries for them in the SENSORINSTANCE table.
 */
//DA-START:function.SensorInstanceEJB:DA-START
//DA-ELSE:function.SensorInstanceEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.SensorInstanceEJB:DA-END

public class SensorInstanceEJB { // start of class

    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.entityManager:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="GREENHOUSE")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of SensorInstanceEJB
     */
    public SensorInstanceEJB() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB:DA-END
    }
    
    /**
     * 
     * @param id
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.delete.long.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.delete.long.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.delete.long:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.delete.long:DA-ELSE
        SensorInstanceDAO dao = new SensorInstanceDAO();
        SensorInstance entity = dao.get(entityManager, id);
        if (entity != null) {
            dao.delete(entityManager, entity);
        }
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.read.long.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.read.long.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.read.long.annotations:DA-END
    public SensorInstanceBean read(long id) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.read.long.SensorInstanceBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.read.long.SensorInstanceBean:DA-ELSE
        SensorInstanceDAO dao = new SensorInstanceDAO();
        SensorInstance entity = dao.get(entityManager, id);
        SensorInstanceBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.read.long.SensorInstanceBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.readList.int.int.List.annotations:DA-END
    public SensorInstanceListBean readList(int offset, int limit, List<String> ids) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.readList.int.int.List.SensorInstanceListBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.readList.int.int.List.SensorInstanceListBean:DA-ELSE
        SensorInstanceListBean result = new SensorInstanceListBean();
        for (SensorInstance entity : new SensorInstanceDAO().getAll(entityManager)) {
            SensorInstanceBean bean = convertFromEntityToBean(entity, null, null);
            result.addSensorInstanceList(bean);
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.readList.int.int.List.SensorInstanceListBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.update.long.SensorInstanceBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.update.long.SensorInstanceBean.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.update.long.SensorInstanceBean.annotations:DA-END
    public SensorInstanceBean update(long id, SensorInstanceBean bean) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.update.long.SensorInstanceBean.SensorInstanceBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.update.long.SensorInstanceBean.SensorInstanceBean:DA-ELSE
        SensorInstanceDAO dao = new SensorInstanceDAO();
        SensorInstance updatedEntity = dao.get(entityManager, bean.getPk());
        SensorInstanceBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = dao.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.update.long.SensorInstanceBean.SensorInstanceBean:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.create.SensorInstanceBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.create.SensorInstanceBean.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.create.SensorInstanceBean.annotations:DA-END
    public SensorInstanceBean create(SensorInstanceBean bean) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.create.SensorInstanceBean.SensorInstanceBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.create.SensorInstanceBean.SensorInstanceBean:DA-ELSE
        SensorInstanceDAO dao = new SensorInstanceDAO();
        SensorInstance createdEntity = null;
        SensorInstanceBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null,entityManager);
        if (createdEntity != null) {
            createdEntity = dao.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.create.SensorInstanceBean.SensorInstanceBean:DA-END
    }
    /**
     * 
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.convertFromBeanToEntity.SensorInstanceBean.SensorInstance.EntityManager.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.convertFromBeanToEntity.SensorInstanceBean.SensorInstance.EntityManager.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.convertFromBeanToEntity.SensorInstanceBean.SensorInstance.EntityManager.annotations:DA-END
    public static SensorInstance convertFromBeanToEntity(SensorInstanceBean bean, SensorInstance entity, EntityManager em) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.convertFromBeanToEntity.SensorInstanceBean.SensorInstance.EntityManager.SensorInstance:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.convertFromBeanToEntity.SensorInstanceBean.SensorInstance.EntityManager.SensorInstance:DA-ELSE
        SensorInstance result = entity;
        if (result == null) {
            result = new SensorInstance();
        }
        result.setId(bean.getId());
        result.setName(bean.getName());
        result.setPk(bean.getPk());
        SensorBean sensorBean = bean.getSensor();
        if (sensorBean == null) {
            result.setSensor(null);
        } else {
            Sensor sensorEntity = em.find(Sensor.class, sensorBean.getPk());
            result.setSensor(sensorEntity);
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.convertFromBeanToEntity.SensorInstanceBean.SensorInstance.EntityManager.SensorInstance:DA-END
    }
    /**
     * 
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.convertFromEntityToBean.SensorInstance.SensorInstanceBean.Map.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.convertFromEntityToBean.SensorInstance.SensorInstanceBean.Map.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.convertFromEntityToBean.SensorInstance.SensorInstanceBean.Map.annotations:DA-END
    public static SensorInstanceBean convertFromEntityToBean(SensorInstance entity, SensorInstanceBean bean, Map<Object, Object> existingBeans) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.convertFromEntityToBean.SensorInstance.SensorInstanceBean.Map.SensorInstanceBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.convertFromEntityToBean.SensorInstance.SensorInstanceBean.Map.SensorInstanceBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        SensorInstanceBean result = bean;
        if (result == null) {
            result = new SensorInstanceBean();
        }
        result.setId(entity.getId());
        result.setName(entity.getName());
        result.setPk(entity.getPk());
        Sensor sensorEntity = entity.getSensor();
        SensorBean newSensorBeanForFieldsensor = new SensorBean();
        if (sensorEntity != null) {
            newSensorBeanForFieldsensor.setPk(sensorEntity.getPk());
            if (existingBeans.containsKey(newSensorBeanForFieldsensor)) {
                result.setSensor((SensorBean)existingBeans.get(newSensorBeanForFieldsensor));
            } else {
                existingBeans.put(newSensorBeanForFieldsensor, newSensorBeanForFieldsensor);
                newSensorBeanForFieldsensor = SensorEJB.convertFromEntityToBean(sensorEntity, newSensorBeanForFieldsensor, existingBeans);
                result.setSensor(newSensorBeanForFieldsensor);
            }
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.convertFromEntityToBean.SensorInstance.SensorInstanceBean.Map.SensorInstanceBean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorInstanceEJB.additional.elements.in.type:DA-END
} // end of java type