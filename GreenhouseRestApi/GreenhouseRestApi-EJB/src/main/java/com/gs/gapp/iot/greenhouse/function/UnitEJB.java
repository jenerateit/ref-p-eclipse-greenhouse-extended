package com.gs.gapp.iot.greenhouse.function;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import com.gs.gapp.iot.greenhouse.UnitDAO;
import com.gs.gapp.iot.greenhouse.Unit;
import com.gs.gapp.iot.greenhouse.basic.UnitBean;
import com.gs.gapp.iot.greenhouse.basic.UnitListBean;
import java.util.List;
import java.lang.String;
import java.util.Map;
import java.lang.Object;
import java.util.LinkedHashMap;

//DA-START:function.UnitEJB:DA-START
//DA-ELSE:function.UnitEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.UnitEJB:DA-END

public class UnitEJB { // start of class

    
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitEJB.entityManager:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="GREENHOUSE")
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of UnitEJB
     */
    public UnitEJB() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitEJB:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitEJB:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitEJB:DA-END
    }
    
    /**
     * 
     * @param id
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitEJB.delete.long.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitEJB.delete.long.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitEJB.delete.long:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitEJB.delete.long:DA-ELSE
        UnitDAO dao = new UnitDAO();
        Unit entity = dao.get(entityManager, id);
        if (entity != null) {
            dao.delete(entityManager, entity);
        }
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitEJB.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitEJB.read.long.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitEJB.read.long.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitEJB.read.long.annotations:DA-END
    public UnitBean read(long id) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitEJB.read.long.UnitBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitEJB.read.long.UnitBean:DA-ELSE
        UnitDAO dao = new UnitDAO();
        Unit entity = dao.get(entityManager, id);
        UnitBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitEJB.read.long.UnitBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitEJB.readList.int.int.List.annotations:DA-END
    public UnitListBean readList(int offset, int limit, List<String> ids) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitEJB.readList.int.int.List.UnitListBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitEJB.readList.int.int.List.UnitListBean:DA-ELSE
        UnitListBean result = new UnitListBean();
        for (Unit entity : new UnitDAO().getAll(entityManager)) {
            UnitBean bean = convertFromEntityToBean(entity, null, null);
            result.addUnitList(bean);
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitEJB.readList.int.int.List.UnitListBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitEJB.update.long.UnitBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitEJB.update.long.UnitBean.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitEJB.update.long.UnitBean.annotations:DA-END
    public UnitBean update(long id, UnitBean bean) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitEJB.update.long.UnitBean.UnitBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitEJB.update.long.UnitBean.UnitBean:DA-ELSE
        UnitDAO dao = new UnitDAO();
        Unit updatedEntity = dao.get(entityManager, bean.getPk());
        UnitBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = dao.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitEJB.update.long.UnitBean.UnitBean:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitEJB.create.UnitBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitEJB.create.UnitBean.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitEJB.create.UnitBean.annotations:DA-END
    public UnitBean create(UnitBean bean) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitEJB.create.UnitBean.UnitBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitEJB.create.UnitBean.UnitBean:DA-ELSE
        UnitDAO dao = new UnitDAO();
        Unit createdEntity = null;
        UnitBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null,entityManager);
        if (createdEntity != null) {
            createdEntity = dao.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitEJB.create.UnitBean.UnitBean:DA-END
    }
    /**
     * 
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitEJB.convertFromBeanToEntity.UnitBean.Unit.EntityManager.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitEJB.convertFromBeanToEntity.UnitBean.Unit.EntityManager.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitEJB.convertFromBeanToEntity.UnitBean.Unit.EntityManager.annotations:DA-END
    public static Unit convertFromBeanToEntity(UnitBean bean, Unit entity, EntityManager em) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitEJB.convertFromBeanToEntity.UnitBean.Unit.EntityManager.Unit:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitEJB.convertFromBeanToEntity.UnitBean.Unit.EntityManager.Unit:DA-ELSE
        Unit result = entity;
        if (result == null) {
            result = new Unit();
        }
        result.setName(bean.getName());
        result.setPk(bean.getPk());
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitEJB.convertFromBeanToEntity.UnitBean.Unit.EntityManager.Unit:DA-END
    }
    /**
     * 
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitEJB.convertFromEntityToBean.Unit.UnitBean.Map.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitEJB.convertFromEntityToBean.Unit.UnitBean.Map.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitEJB.convertFromEntityToBean.Unit.UnitBean.Map.annotations:DA-END
    public static UnitBean convertFromEntityToBean(Unit entity, UnitBean bean, Map<Object, Object> existingBeans) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.UnitEJB.convertFromEntityToBean.Unit.UnitBean.Map.UnitBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitEJB.convertFromEntityToBean.Unit.UnitBean.Map.UnitBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        UnitBean result = bean;
        if (result == null) {
            result = new UnitBean();
        }
        result.setName(entity.getName());
        result.setPk(entity.getPk());
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.UnitEJB.convertFromEntityToBean.Unit.UnitBean.Map.UnitBean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.UnitEJB.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.UnitEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.function.UnitEJB.additional.elements.in.type:DA-END
} // end of java type