package com.gs.gapp.iot.greenhouse.function;


import javax.inject.Named;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.ejb.Stateless;
import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;
import com.gs.gapp.iot.greenhouse.SensorDAO;
import com.gs.gapp.iot.greenhouse.Sensor;
import com.gs.gapp.iot.greenhouse.basic.SensorBean;
import com.gs.gapp.iot.greenhouse.basic.SensorListBean;
import java.util.List;
import java.lang.String;
import java.util.Map;
import java.lang.Object;
import java.util.LinkedHashMap;

/**
 * Date that describes a sensor. When for instance 10 idientical sensors
 * are being used, there is only one single entry in the SENSOR table.
 */
//DA-START:function.SensorEJB:DA-START
//DA-ELSE:function.SensorEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.SensorEJB:DA-END

public class SensorEJB { // start of class

    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorEJB.entityManager:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="GREENHOUSE")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of SensorEJB
     */
    public SensorEJB() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorEJB:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorEJB:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorEJB:DA-END
    }
    
    /**
     * 
     * @param id
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorEJB.delete.long.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorEJB.delete.long.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorEJB.delete.long:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorEJB.delete.long:DA-ELSE
        SensorDAO dao = new SensorDAO();
        Sensor entity = dao.get(entityManager, id);
        if (entity != null) {
            dao.delete(entityManager, entity);
        }
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorEJB.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorEJB.read.long.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorEJB.read.long.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorEJB.read.long.annotations:DA-END
    public SensorBean read(long id) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorEJB.read.long.SensorBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorEJB.read.long.SensorBean:DA-ELSE
        SensorDAO dao = new SensorDAO();
        Sensor entity = dao.get(entityManager, id);
        SensorBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorEJB.read.long.SensorBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorEJB.readList.int.int.List.annotations:DA-END
    public SensorListBean readList(int offset, int limit, List<String> ids) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorEJB.readList.int.int.List.SensorListBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorEJB.readList.int.int.List.SensorListBean:DA-ELSE
        SensorListBean result = new SensorListBean();
        for (Sensor entity : new SensorDAO().getAll(entityManager)) {
            SensorBean bean = convertFromEntityToBean(entity, null, null);
            result.addSensorList(bean);
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorEJB.readList.int.int.List.SensorListBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorEJB.update.long.SensorBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorEJB.update.long.SensorBean.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorEJB.update.long.SensorBean.annotations:DA-END
    public SensorBean update(long id, SensorBean bean) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorEJB.update.long.SensorBean.SensorBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorEJB.update.long.SensorBean.SensorBean:DA-ELSE
        SensorDAO dao = new SensorDAO();
        Sensor updatedEntity = dao.get(entityManager, bean.getPk());
        SensorBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = dao.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorEJB.update.long.SensorBean.SensorBean:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorEJB.create.SensorBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorEJB.create.SensorBean.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorEJB.create.SensorBean.annotations:DA-END
    public SensorBean create(SensorBean bean) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorEJB.create.SensorBean.SensorBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorEJB.create.SensorBean.SensorBean:DA-ELSE
        SensorDAO dao = new SensorDAO();
        Sensor createdEntity = null;
        SensorBean result = null;
        createdEntity = convertFromBeanToEntity(bean, null,entityManager);
        if (createdEntity != null) {
            createdEntity = dao.create(entityManager, createdEntity);
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorEJB.create.SensorBean.SensorBean:DA-END
    }
    /**
     * 
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorEJB.convertFromBeanToEntity.SensorBean.Sensor.EntityManager.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorEJB.convertFromBeanToEntity.SensorBean.Sensor.EntityManager.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorEJB.convertFromBeanToEntity.SensorBean.Sensor.EntityManager.annotations:DA-END
    public static Sensor convertFromBeanToEntity(SensorBean bean, Sensor entity, EntityManager em) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorEJB.convertFromBeanToEntity.SensorBean.Sensor.EntityManager.Sensor:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorEJB.convertFromBeanToEntity.SensorBean.Sensor.EntityManager.Sensor:DA-ELSE
        Sensor result = entity;
        if (result == null) {
            result = new Sensor();
        }
        result.setId(bean.getId());
        result.setName(bean.getName());
        result.setPk(bean.getPk());
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorEJB.convertFromBeanToEntity.SensorBean.Sensor.EntityManager.Sensor:DA-END
    }
    /**
     * 
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorEJB.convertFromEntityToBean.Sensor.SensorBean.Map.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorEJB.convertFromEntityToBean.Sensor.SensorBean.Map.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorEJB.convertFromEntityToBean.Sensor.SensorBean.Map.annotations:DA-END
    public static SensorBean convertFromEntityToBean(Sensor entity, SensorBean bean, Map<Object, Object> existingBeans) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorEJB.convertFromEntityToBean.Sensor.SensorBean.Map.SensorBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorEJB.convertFromEntityToBean.Sensor.SensorBean.Map.SensorBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        SensorBean result = bean;
        if (result == null) {
            result = new SensorBean();
        }
        result.setId(entity.getId());
        result.setName(entity.getName());
        result.setPk(entity.getPk());
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorEJB.convertFromEntityToBean.Sensor.SensorBean.Map.SensorBean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorEJB.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorEJB.additional.elements.in.type:DA-END
} // end of java type