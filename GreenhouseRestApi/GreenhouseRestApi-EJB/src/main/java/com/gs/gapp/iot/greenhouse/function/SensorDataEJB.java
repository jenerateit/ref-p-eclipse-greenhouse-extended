package com.gs.gapp.iot.greenhouse.function;


import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Named;
import javax.persistence.EntityManager;
/*
 * Imports from last generation
 */
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.gs.gapp.iot.greenhouse.GeoCoordinates;
import com.gs.gapp.iot.greenhouse.Sensor;
import com.gs.gapp.iot.greenhouse.SensorData;
import com.gs.gapp.iot.greenhouse.SensorDataDAO;
import com.gs.gapp.iot.greenhouse.SensorInstance;
import com.gs.gapp.iot.greenhouse.SensorInstanceDAO;
import com.gs.gapp.iot.greenhouse.Unit;
import com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean;
import com.gs.gapp.iot.greenhouse.basic.SensorBean;
import com.gs.gapp.iot.greenhouse.basic.SensorDataBean;
import com.gs.gapp.iot.greenhouse.basic.SensorDataListBean;
import com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean;
import com.gs.gapp.iot.greenhouse.basic.UnitBean;


/**
 * Holds all collected measurement data.
 */
//DA-START:function.SensorDataEJB:DA-START
//DA-ELSE:function.SensorDataEJB:DA-ELSE
@Named
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@Stateless
//DA-END:function.SensorDataEJB:DA-END

public class SensorDataEJB { // start of class

    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.entityManager:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.entityManager:DA-ELSE
    @PersistenceContext(unitName="GREENHOUSE")
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.entityManager:DA-END
    private EntityManager entityManager;
    
    /**
     * creates an instance of SensorDataEJB
     */
    public SensorDataEJB() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorDataEJB:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorDataEJB:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorDataEJB:DA-END
    }
    
    /**
     * 
     * @param id
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.delete.long.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.delete.long.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.delete.long.annotations:DA-END
    public void delete(long id) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.delete.long:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.delete.long:DA-ELSE
        SensorDataDAO dao = new SensorDataDAO();
        SensorData entity = dao.get(entityManager, id);
        if (entity != null) {
            dao.delete(entityManager, entity);
        }
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.delete.long:DA-END
    }
    /**
     * 
     * @param id
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.read.long.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.read.long.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.read.long.annotations:DA-END
    public SensorDataBean read(long id) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.read.long.SensorDataBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.read.long.SensorDataBean:DA-ELSE
        SensorDataDAO dao = new SensorDataDAO();
        SensorData entity = dao.get(entityManager, id);
        SensorDataBean result = null;
        if (entity != null) {
            result = convertFromEntityToBean(entity, null, null);
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.read.long.SensorDataBean:DA-END
    }
    /**
     * 
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.readList.int.int.List.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.readList.int.int.List.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.readList.int.int.List.annotations:DA-END
    public SensorDataListBean readList(int offset, int limit, List<String> ids) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.readList.int.int.List.SensorDataListBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.readList.int.int.List.SensorDataListBean:DA-ELSE
        SensorDataListBean result = new SensorDataListBean();
        for (SensorData entity : new SensorDataDAO().getAll(entityManager)) {
            SensorDataBean bean = convertFromEntityToBean(entity, null, null);
            result.addSensorDataList(bean);
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.readList.int.int.List.SensorDataListBean:DA-END
    }
    /**
     * 
     * @param id
     * @param bean
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.update.long.SensorDataBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.update.long.SensorDataBean.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.update.long.SensorDataBean.annotations:DA-END
    public SensorDataBean update(long id, SensorDataBean bean) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.update.long.SensorDataBean.SensorDataBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.update.long.SensorDataBean.SensorDataBean:DA-ELSE
        SensorDataDAO dao = new SensorDataDAO();
        SensorData updatedEntity = dao.get(entityManager, bean.getPk());
        SensorDataBean result = null;
        if (updatedEntity != null) {
            updatedEntity = convertFromBeanToEntity(bean, updatedEntity, entityManager);
            updatedEntity = dao.update(entityManager, updatedEntity);
            result = convertFromEntityToBean(updatedEntity, null, null);
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.update.long.SensorDataBean.SensorDataBean:DA-END
    }
    /**
     * 
     * @param bean
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.create.SensorDataBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.create.SensorDataBean.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.create.SensorDataBean.annotations:DA-END
    public SensorDataBean create(SensorDataBean bean) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.create.SensorDataBean.SensorDataBean:DA-START
    	SensorDataDAO dao = new SensorDataDAO();
        SensorData createdEntity = null;
        SensorDataBean result = null;
        
        if (bean.getTimeOfMeasurement() == null) {
        	bean.setTimeOfMeasurement(new Date());  // TODO temporary fix until managed to solve retrofit issue of not properly converting dates
        } else {
        	System.out.println("time of transferred measurement in SensorDataEJB:" + bean.getTimeOfMeasurement());
        }
        createdEntity = convertFromBeanToEntity(bean, null,entityManager);
        if (createdEntity != null) {
            createdEntity = dao.create(entityManager, createdEntity);
            
            SensorInstance existingSensorInstance = null;
            try {
            	synchronized (entityManager) {
		            Query sensorInstanceQuery = entityManager.createQuery("SELECT o FROM SensorInstance o where o.id = :id");
		            sensorInstanceQuery.setParameter("id", bean.getResourceId());
		        	existingSensorInstance = (SensorInstance) sensorInstanceQuery.getSingleResult();
            	}
            } catch (NoResultException ex) {
            	existingSensorInstance = new SensorInstance();
            	existingSensorInstance.setId(bean.getResourceId());
            	existingSensorInstance = SensorInstanceDAO.create(entityManager, existingSensorInstance);
            }
            
        	createdEntity.setSensorInstance(existingSensorInstance);
            
            result = convertFromEntityToBean(createdEntity, null, null);
        }
        return result;
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.create.SensorDataBean.SensorDataBean:DA-ELSE
        //SensorDataDAO dao = new SensorDataDAO();
        //SensorData createdEntity = null;
        //SensorDataBean result = null;
        //createdEntity = convertFromBeanToEntity(bean, null,entityManager);
        //if (createdEntity != null) {
            //createdEntity = dao.create(entityManager, createdEntity);
            //result = convertFromEntityToBean(createdEntity, null, null);
        //}
        //return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.create.SensorDataBean.SensorDataBean:DA-END
    }
    /**
     * 
     * @param bean  the bean
     * @param entity  the entity
     * @param em  the em
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.convertFromBeanToEntity.SensorDataBean.SensorData.EntityManager.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.convertFromBeanToEntity.SensorDataBean.SensorData.EntityManager.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.convertFromBeanToEntity.SensorDataBean.SensorData.EntityManager.annotations:DA-END
    public static SensorData convertFromBeanToEntity(SensorDataBean bean, SensorData entity, EntityManager em) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.convertFromBeanToEntity.SensorDataBean.SensorData.EntityManager.SensorData:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.convertFromBeanToEntity.SensorDataBean.SensorData.EntityManager.SensorData:DA-ELSE
        SensorData result = entity;
        if (result == null) {
            result = new SensorData();
        }
        result.setAccountName(bean.getAccountName());
        result.setClientId(bean.getClientId());
        result.setAppId(bean.getAppId());
        result.setResourceId(bean.getResourceId());
        result.setValue(bean.getValue());
        result.setTimeOfMeasurement(bean.getTimeOfMeasurement());
        result.setTimeZone(bean.getTimeZone());
        result.setPk(bean.getPk());
        UnitBean unitBean = bean.getUnit();
        if (unitBean == null) {
            result.setUnit(null);
        } else {
            Unit unitEntity = em.find(Unit.class, unitBean.getPk());
            result.setUnit(unitEntity);
        }
        if (bean.getGeocoordinates() != null) {
            GeoCoordinates geocoordinatesObj = new GeoCoordinates();
            geocoordinatesObj.setLng(bean.getGeocoordinates().getLng());
            geocoordinatesObj.setLat(bean.getGeocoordinates().getLat());
            geocoordinatesObj.setAlt(bean.getGeocoordinates().getAlt());
            result.setGeocoordinates(geocoordinatesObj);
        }
        SensorBean sensorBean = bean.getSensor();
        if (sensorBean == null) {
            result.setSensor(null);
        } else {
            Sensor sensorEntity = em.find(Sensor.class, sensorBean.getPk());
            result.setSensor(sensorEntity);
        }
        SensorInstanceBean sensorInstanceBean = bean.getSensorInstance();
        if (sensorInstanceBean == null) {
            result.setSensorInstance(null);
        } else {
            SensorInstance sensorInstanceEntity = em.find(SensorInstance.class, sensorInstanceBean.getPk());
            result.setSensorInstance(sensorInstanceEntity);
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.convertFromBeanToEntity.SensorDataBean.SensorData.EntityManager.SensorData:DA-END
    }
    /**
     * 
     * @param entity  the entity
     * @param bean  the bean
     * @param existingBeans  the existingBeans
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.convertFromEntityToBean.SensorData.SensorDataBean.Map.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.convertFromEntityToBean.SensorData.SensorDataBean.Map.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.convertFromEntityToBean.SensorData.SensorDataBean.Map.annotations:DA-END
    public static SensorDataBean convertFromEntityToBean(SensorData entity, SensorDataBean bean, Map<Object, Object> existingBeans) {
        //DA-START:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.convertFromEntityToBean.SensorData.SensorDataBean.Map.SensorDataBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.convertFromEntityToBean.SensorData.SensorDataBean.Map.SensorDataBean:DA-ELSE
        if (existingBeans == null) {
            existingBeans = new LinkedHashMap<Object, Object>();
        }
        SensorDataBean result = bean;
        if (result == null) {
            result = new SensorDataBean();
        }
        result.setAccountName(entity.getAccountName());
        result.setClientId(entity.getClientId());
        result.setAppId(entity.getAppId());
        result.setResourceId(entity.getResourceId());
        result.setValue(entity.getValue());
        result.setTimeOfMeasurement(entity.getTimeOfMeasurement());
        result.setTimeZone(entity.getTimeZone());
        result.setPk(entity.getPk());
        Unit unitEntity = entity.getUnit();
        UnitBean newUnitBeanForFieldunit = new UnitBean();
        if (unitEntity != null) {
            newUnitBeanForFieldunit.setPk(unitEntity.getPk());
            if (existingBeans.containsKey(newUnitBeanForFieldunit)) {
                result.setUnit((UnitBean)existingBeans.get(newUnitBeanForFieldunit));
            } else {
                existingBeans.put(newUnitBeanForFieldunit, newUnitBeanForFieldunit);
                newUnitBeanForFieldunit = UnitEJB.convertFromEntityToBean(unitEntity, newUnitBeanForFieldunit, existingBeans);
                result.setUnit(newUnitBeanForFieldunit);
            }
        }
        if (entity.getGeocoordinates() != null) {
            GeoCoordinatesBean geocoordinatesObj = new GeoCoordinatesBean();
            geocoordinatesObj.setLng(entity.getGeocoordinates().getLng());
            geocoordinatesObj.setLat(entity.getGeocoordinates().getLat());
            geocoordinatesObj.setAlt(entity.getGeocoordinates().getAlt());
            result.setGeocoordinates(geocoordinatesObj);
        }
        Sensor sensorEntity = entity.getSensor();
        SensorBean newSensorBeanForFieldsensor = new SensorBean();
        if (sensorEntity != null) {
            newSensorBeanForFieldsensor.setPk(sensorEntity.getPk());
            if (existingBeans.containsKey(newSensorBeanForFieldsensor)) {
                result.setSensor((SensorBean)existingBeans.get(newSensorBeanForFieldsensor));
            } else {
                existingBeans.put(newSensorBeanForFieldsensor, newSensorBeanForFieldsensor);
                newSensorBeanForFieldsensor = SensorEJB.convertFromEntityToBean(sensorEntity, newSensorBeanForFieldsensor, existingBeans);
                result.setSensor(newSensorBeanForFieldsensor);
            }
        }
        SensorInstance sensorInstanceEntity = entity.getSensorInstance();
        SensorInstanceBean newSensorInstanceBeanForFieldsensorInstance = new SensorInstanceBean();
        if (sensorInstanceEntity != null) {
            newSensorInstanceBeanForFieldsensorInstance.setPk(sensorInstanceEntity.getPk());
            if (existingBeans.containsKey(newSensorInstanceBeanForFieldsensorInstance)) {
                result.setSensorInstance((SensorInstanceBean)existingBeans.get(newSensorInstanceBeanForFieldsensorInstance));
            } else {
                existingBeans.put(newSensorInstanceBeanForFieldsensorInstance, newSensorInstanceBeanForFieldsensorInstance);
                newSensorInstanceBeanForFieldsensorInstance = SensorInstanceEJB.convertFromEntityToBean(sensorInstanceEntity, newSensorInstanceBeanForFieldsensorInstance, existingBeans);
                result.setSensorInstance(newSensorInstanceBeanForFieldsensorInstance);
            }
        }
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.convertFromEntityToBean.SensorData.SensorDataBean.Map.SensorDataBean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.additional.elements.in.type:DA-START
    /**
     * @param sensorInstanceId
     * @param offset
     * @param limit
     * @param ids
     * @return
     */
    public SensorDataListBean readListBySensorInstanceId(String sensorInstanceId, int offset, int limit, List<String> ids) {
        SensorDataListBean result = new SensorDataListBean();
        Long sensorInstancePk = new Long(sensorInstanceId);
        for (SensorData entity : new SensorDataDAO().getAll(entityManager)) {
        	if (entity.getSensorInstance() == null || entity.getSensorInstance().getPk().equals(sensorInstancePk) == false) continue;
        	
            SensorDataBean bean = convertFromEntityToBean(entity, null, null);
            result.addSensorDataList(bean);
        }
        return result;
    }
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.additional.elements.in.type:DA-ELSE
    //// add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.function.SensorDataEJB.additional.elements.in.type:DA-END
} // end of java type