package com.gs.gapp.iot.greenhouse.function;


import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

/**
 * Date that describes a sensor. When for instance 10 idientical sensors
 * are being used, there is only one single entry in the SENSOR table.
 */
//DA-START:function.TestSensorEJB:DA-START
//DA-ELSE:function.TestSensorEJB:DA-ELSE
//DA-END:function.TestSensorEJB:DA-END
public class TestSensorEJB { // start of class

    /**
     * creates an instance of TestSensorEJB
     */
    public TestSensorEJB() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB:DA-END
    }
    
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.setUpBeforeClass.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.setUpBeforeClass.annotations:DA-ELSE
    @BeforeClass
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.setUpBeforeClass.annotations:DA-END
    public static void setUpBeforeClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.setUpBeforeClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.setUpBeforeClass:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.setUpBeforeClass:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.tearDownAfterClass.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.tearDownAfterClass.annotations:DA-ELSE
    @AfterClass
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.tearDownAfterClass.annotations:DA-END
    public static void tearDownAfterClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.tearDownAfterClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.tearDownAfterClass:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.tearDownAfterClass:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.setUp.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.setUp.annotations:DA-ELSE
    @Before
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.setUp.annotations:DA-END
    public void setUp() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.setUp:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.setUp:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.setUp:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.tearDown.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.tearDown.annotations:DA-ELSE
    @After
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.tearDown.annotations:DA-END
    public void tearDown() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.tearDown:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.tearDown:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.tearDown:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testDelete.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testDelete.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testDelete.annotations:DA-END
    public void testDelete() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testDelete:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testDelete:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testDelete:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testRead.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testRead.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testRead.annotations:DA-END
    public void testRead() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testRead:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testRead:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testRead:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testReadList.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testReadList.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testReadList.annotations:DA-END
    public void testReadList() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testReadList:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testReadList:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testReadList:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testUpdate.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testUpdate.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testUpdate.annotations:DA-END
    public void testUpdate() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testUpdate:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testUpdate:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testUpdate:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testCreate.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testCreate.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testCreate.annotations:DA-END
    public void testCreate() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testCreate:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testCreate:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testCreate:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testConvertFromBeanToEntity.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testConvertFromBeanToEntity.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testConvertFromBeanToEntity.annotations:DA-END
    public void testConvertFromBeanToEntity() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testConvertFromBeanToEntity:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testConvertFromBeanToEntity:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testConvertFromBeanToEntity:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testConvertFromEntityToBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testConvertFromEntityToBean.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testConvertFromEntityToBean.annotations:DA-END
    public void testConvertFromEntityToBean() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testConvertFromEntityToBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testConvertFromEntityToBean:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.testConvertFromEntityToBean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorEJB.additional.elements.in.type:DA-END
} // end of java type