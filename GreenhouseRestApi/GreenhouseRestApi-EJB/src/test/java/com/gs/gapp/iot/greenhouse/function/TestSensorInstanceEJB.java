package com.gs.gapp.iot.greenhouse.function;


import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

/**
 * A sensor instance relates to a real, physical sensor.
 * When for instance 10 identical sensors are being used,
 * there will be 10 entries for them in the SENSORINSTANCE table.
 */
//DA-START:function.TestSensorInstanceEJB:DA-START
//DA-ELSE:function.TestSensorInstanceEJB:DA-ELSE
//DA-END:function.TestSensorInstanceEJB:DA-END
public class TestSensorInstanceEJB { // start of class

    /**
     * creates an instance of TestSensorInstanceEJB
     */
    public TestSensorInstanceEJB() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB:DA-END
    }
    
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.setUpBeforeClass.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.setUpBeforeClass.annotations:DA-ELSE
    @BeforeClass
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.setUpBeforeClass.annotations:DA-END
    public static void setUpBeforeClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.setUpBeforeClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.setUpBeforeClass:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.setUpBeforeClass:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.tearDownAfterClass.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.tearDownAfterClass.annotations:DA-ELSE
    @AfterClass
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.tearDownAfterClass.annotations:DA-END
    public static void tearDownAfterClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.tearDownAfterClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.tearDownAfterClass:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.tearDownAfterClass:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.setUp.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.setUp.annotations:DA-ELSE
    @Before
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.setUp.annotations:DA-END
    public void setUp() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.setUp:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.setUp:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.setUp:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.tearDown.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.tearDown.annotations:DA-ELSE
    @After
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.tearDown.annotations:DA-END
    public void tearDown() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.tearDown:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.tearDown:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.tearDown:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testDelete.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testDelete.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testDelete.annotations:DA-END
    public void testDelete() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testDelete:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testDelete:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testDelete:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testRead.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testRead.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testRead.annotations:DA-END
    public void testRead() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testRead:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testRead:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testRead:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testReadList.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testReadList.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testReadList.annotations:DA-END
    public void testReadList() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testReadList:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testReadList:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testReadList:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testUpdate.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testUpdate.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testUpdate.annotations:DA-END
    public void testUpdate() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testUpdate:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testUpdate:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testUpdate:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testCreate.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testCreate.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testCreate.annotations:DA-END
    public void testCreate() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testCreate:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testCreate:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testCreate:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testConvertFromBeanToEntity.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testConvertFromBeanToEntity.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testConvertFromBeanToEntity.annotations:DA-END
    public void testConvertFromBeanToEntity() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testConvertFromBeanToEntity:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testConvertFromBeanToEntity:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testConvertFromBeanToEntity:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testConvertFromEntityToBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testConvertFromEntityToBean.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testConvertFromEntityToBean.annotations:DA-END
    public void testConvertFromEntityToBean() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testConvertFromEntityToBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testConvertFromEntityToBean:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.testConvertFromEntityToBean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorInstanceEJB.additional.elements.in.type:DA-END
} // end of java type