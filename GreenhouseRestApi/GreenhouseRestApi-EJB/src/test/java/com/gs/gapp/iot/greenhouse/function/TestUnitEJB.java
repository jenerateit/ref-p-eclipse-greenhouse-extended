package com.gs.gapp.iot.greenhouse.function;


import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

//DA-START:function.TestUnitEJB:DA-START
//DA-ELSE:function.TestUnitEJB:DA-ELSE
//DA-END:function.TestUnitEJB:DA-END
public class TestUnitEJB { // start of class

    /**
     * creates an instance of TestUnitEJB
     */
    public TestUnitEJB() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB:DA-END
    }
    
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.setUpBeforeClass.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.setUpBeforeClass.annotations:DA-ELSE
    @BeforeClass
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.setUpBeforeClass.annotations:DA-END
    public static void setUpBeforeClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.setUpBeforeClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.setUpBeforeClass:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.setUpBeforeClass:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.tearDownAfterClass.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.tearDownAfterClass.annotations:DA-ELSE
    @AfterClass
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.tearDownAfterClass.annotations:DA-END
    public static void tearDownAfterClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.tearDownAfterClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.tearDownAfterClass:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.tearDownAfterClass:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.setUp.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.setUp.annotations:DA-ELSE
    @Before
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.setUp.annotations:DA-END
    public void setUp() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.setUp:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.setUp:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.setUp:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.tearDown.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.tearDown.annotations:DA-ELSE
    @After
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.tearDown.annotations:DA-END
    public void tearDown() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.tearDown:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.tearDown:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.tearDown:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testDelete.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testDelete.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testDelete.annotations:DA-END
    public void testDelete() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testDelete:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testDelete:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testDelete:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testRead.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testRead.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testRead.annotations:DA-END
    public void testRead() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testRead:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testRead:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testRead:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testReadList.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testReadList.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testReadList.annotations:DA-END
    public void testReadList() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testReadList:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testReadList:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testReadList:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testUpdate.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testUpdate.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testUpdate.annotations:DA-END
    public void testUpdate() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testUpdate:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testUpdate:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testUpdate:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testCreate.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testCreate.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testCreate.annotations:DA-END
    public void testCreate() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testCreate:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testCreate:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testCreate:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testConvertFromBeanToEntity.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testConvertFromBeanToEntity.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testConvertFromBeanToEntity.annotations:DA-END
    public void testConvertFromBeanToEntity() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testConvertFromBeanToEntity:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testConvertFromBeanToEntity:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testConvertFromBeanToEntity:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testConvertFromEntityToBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testConvertFromEntityToBean.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testConvertFromEntityToBean.annotations:DA-END
    public void testConvertFromEntityToBean() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testConvertFromEntityToBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testConvertFromEntityToBean:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.testConvertFromEntityToBean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestUnitEJB.additional.elements.in.type:DA-END
} // end of java type