package com.gs.gapp.iot.greenhouse.function;


import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

/**
 * Holds all collected measurement data.
 */
//DA-START:function.TestSensorDataEJB:DA-START
//DA-ELSE:function.TestSensorDataEJB:DA-ELSE
//DA-END:function.TestSensorDataEJB:DA-END
public class TestSensorDataEJB { // start of class

    /**
     * creates an instance of TestSensorDataEJB
     */
    public TestSensorDataEJB() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB:DA-END
    }
    
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.setUpBeforeClass.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.setUpBeforeClass.annotations:DA-ELSE
    @BeforeClass
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.setUpBeforeClass.annotations:DA-END
    public static void setUpBeforeClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.setUpBeforeClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.setUpBeforeClass:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.setUpBeforeClass:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.tearDownAfterClass.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.tearDownAfterClass.annotations:DA-ELSE
    @AfterClass
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.tearDownAfterClass.annotations:DA-END
    public static void tearDownAfterClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.tearDownAfterClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.tearDownAfterClass:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.tearDownAfterClass:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.setUp.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.setUp.annotations:DA-ELSE
    @Before
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.setUp.annotations:DA-END
    public void setUp() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.setUp:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.setUp:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.setUp:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.tearDown.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.tearDown.annotations:DA-ELSE
    @After
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.tearDown.annotations:DA-END
    public void tearDown() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.tearDown:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.tearDown:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.tearDown:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testDelete.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testDelete.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testDelete.annotations:DA-END
    public void testDelete() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testDelete:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testDelete:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testDelete:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testRead.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testRead.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testRead.annotations:DA-END
    public void testRead() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testRead:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testRead:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testRead:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testReadList.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testReadList.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testReadList.annotations:DA-END
    public void testReadList() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testReadList:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testReadList:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testReadList:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testUpdate.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testUpdate.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testUpdate.annotations:DA-END
    public void testUpdate() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testUpdate:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testUpdate:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testUpdate:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testCreate.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testCreate.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testCreate.annotations:DA-END
    public void testCreate() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testCreate:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testCreate:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testCreate:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testConvertFromBeanToEntity.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testConvertFromBeanToEntity.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testConvertFromBeanToEntity.annotations:DA-END
    public void testConvertFromBeanToEntity() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testConvertFromBeanToEntity:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testConvertFromBeanToEntity:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testConvertFromBeanToEntity:DA-END
    }
    /**
     */
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testConvertFromEntityToBean.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testConvertFromEntityToBean.annotations:DA-ELSE
    @Test
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testConvertFromEntityToBean.annotations:DA-END
    public void testConvertFromEntityToBean() {
        //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testConvertFromEntityToBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testConvertFromEntityToBean:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.testConvertFromEntityToBean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.function.TestSensorDataEJB.additional.elements.in.type:DA-END
} // end of java type