package com.gs.gapp.iot.greenhouse.function;

import com.gs.gapp.iot.greenhouse.basic.SensorDataBean;
import com.gs.gapp.iot.greenhouse.basic.SensorDataListBean;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Path;

public interface SensorDataService {

	@Headers( {"Accept: application/json"} )
	@POST("/sensordata")
	void createSensorData(@Body SensorDataBean sensorData, Callback<Object> callback);
	
	@Headers( {"Accept: application/json"} )
	@GET("/sensorinstance/{sensorInstanceId}/sensordata")
	SensorDataListBean getSensorData(@Path("sensorInstanceId") String sensorInstanceId);
}
