package com.gs.gapp.iot.greenhouse.function;

import com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean;

import retrofit.http.GET;
import retrofit.http.Headers;

public interface SensorInstanceService {

	/**
	 * Get all sensor instances in order to be able
	 * to display a list of them.
	 * 
	 * @param callback
	 * @return
	 */
	@Headers( {"Accept: application/json"} )
	@GET("/sensorinstance")
	SensorInstanceListBean getSensorInstances();
	
}
