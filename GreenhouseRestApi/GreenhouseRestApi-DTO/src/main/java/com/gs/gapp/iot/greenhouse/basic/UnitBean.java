package com.gs.gapp.iot.greenhouse.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.lang.String;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="com.gs.gapp.iot.greenhouse.basic")
public class UnitBean extends BaseEntityBean { // start of class

    private String name;
    
    /**
     * creates an instance of UnitBean
     */
    public UnitBean() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.UnitBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.UnitBean:DA-ELSE
        super();
        //DA-END:com.gs.gapp.iot.greenhouse.basic.UnitBean:DA-END
    }
    
    /**
     * getter for the field name
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="name")
    public String getName() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.UnitBean.getName.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.UnitBean.getName.String:DA-ELSE
        return this.name;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.UnitBean.getName.String:DA-END
    }
    /**
     * setter for the field name
     * 
     * null
     * 
     * @param name  the name
     */
    public void setName(String name) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.UnitBean.setName.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.UnitBean.setName.String:DA-ELSE
        this.name = name;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.UnitBean.setName.String:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.basic.UnitBean.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.UnitBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.basic.UnitBean.additional.elements.in.type:DA-END
} // end of java type