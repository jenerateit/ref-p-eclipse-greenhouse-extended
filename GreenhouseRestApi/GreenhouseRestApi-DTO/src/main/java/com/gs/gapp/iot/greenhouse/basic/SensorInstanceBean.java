package com.gs.gapp.iot.greenhouse.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.lang.String;
import javax.xml.bind.annotation.XmlElement;

/**
 * A sensor instance relates to a real, physical sensor.
 * When for instance 10 identical sensors are being used,
 * there will be 10 entries for them in the SENSORINSTANCE table.
 */
@XmlRootElement(namespace="com.gs.gapp.iot.greenhouse.basic")
public class SensorInstanceBean extends BaseEntityBean { // start of class

    private String id;
    
    private String name;
    
    private SensorBean sensor;
    
    /**
     * creates an instance of SensorInstanceBean
     */
    public SensorInstanceBean() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean:DA-ELSE
        super();
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean:DA-END
    }
    
    /**
     * getter for the field id
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="id")
    public String getId() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.getId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.getId.String:DA-ELSE
        return this.id;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.getId.String:DA-END
    }
    /**
     * setter for the field id
     * 
     * null
     * 
     * @param id  the id
     */
    public void setId(String id) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.setId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.setId.String:DA-ELSE
        this.id = id;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.setId.String:DA-END
    }
    /**
     * getter for the field name
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="name")
    public String getName() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.getName.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.getName.String:DA-ELSE
        return this.name;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.getName.String:DA-END
    }
    /**
     * setter for the field name
     * 
     * null
     * 
     * @param name  the name
     */
    public void setName(String name) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.setName.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.setName.String:DA-ELSE
        this.name = name;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.setName.String:DA-END
    }
    /**
     * getter for the field sensor
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="sensor")
    public SensorBean getSensor() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.getSensor.SensorBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.getSensor.SensorBean:DA-ELSE
        return this.sensor;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.getSensor.SensorBean:DA-END
    }
    /**
     * setter for the field sensor
     * 
     * null
     * 
     * @param sensor  the sensor
     */
    public void setSensor(SensorBean sensor) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.setSensor.SensorBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.setSensor.SensorBean:DA-ELSE
        this.sensor = sensor;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.setSensor.SensorBean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorInstanceBean.additional.elements.in.type:DA-END
} // end of java type