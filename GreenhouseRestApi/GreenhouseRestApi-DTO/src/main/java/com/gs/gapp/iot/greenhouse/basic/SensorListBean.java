package com.gs.gapp.iot.greenhouse.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

/**
 * Date that describes a sensor. When for instance 10 idientical sensors
 * are being used, there is only one single entry in the SENSOR table.
 */
@XmlRootElement(namespace="com.gs.gapp.iot.greenhouse.basic")
public class SensorListBean { // start of class

    private List<SensorBean> sensorList = new ArrayList<SensorBean>();
    
    /**
     * creates an instance of SensorListBean
     */
    public SensorListBean() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorListBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorListBean:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorListBean:DA-END
    }
    
    /**
     * getter for the field sensorList
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="sensorList")
    public List<SensorBean> getSensorList() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorListBean.getSensorList.List:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorListBean.getSensorList.List:DA-ELSE
        return this.sensorList;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorListBean.getSensorList.List:DA-END
    }
    /**
     * adder for the field sensorList
     * 
     * null
     * 
     * @param element  the element
     */
    public void addSensorList(SensorBean element) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorListBean.addSensorList.SensorBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorListBean.addSensorList.SensorBean:DA-ELSE
        this.sensorList.add(element);
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorListBean.addSensorList.SensorBean:DA-END
    }
    /**
     * remover for the field sensorList
     * 
     * null
     * 
     * @param element  the element
     */
    public void removeSensorList(SensorBean element) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorListBean.removeSensorList.SensorBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorListBean.removeSensorList.SensorBean:DA-ELSE
        this.sensorList.remove(element);
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorListBean.removeSensorList.SensorBean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorListBean.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorListBean.additional.elements.in.type:DA-END
} // end of java type