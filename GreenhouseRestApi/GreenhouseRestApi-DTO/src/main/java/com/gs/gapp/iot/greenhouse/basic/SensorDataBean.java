package com.gs.gapp.iot.greenhouse.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.lang.String;
import java.util.Date;
import javax.xml.bind.annotation.XmlElement;

/**
 * Holds all collected measurement data.
 */
@XmlRootElement(namespace="com.gs.gapp.iot.greenhouse.basic")
public class SensorDataBean extends BaseEntityBean { // start of class

    /**
     * Identifies a group of devices and users. It can be seen
     * as partition of the MQTT topic namespace.
     */
    private String accountName;
    
    /**
     * Identifies a single gateway device within an
     * account (typically the MAC address of a
     * gateway???s primary network interface).
     * The client_id maps to the Client Identifier (Client ID)
     * as defined in the MQTT specifications.
     */
    private String clientId;
    
    /**
     * Identifies an application running on the gateway device.
     * To support multiple versions of the application, it is recommended
     * that a version number be assigned with the app_id (e.g., ???CONF-V1???,
     * ???CONF-V2???, etc.)
     */
    private String appId;
    
    /**
     * Identifies a resource(s) that is owned and managed by a particular application.
     * Management of resources (e.g., sensors, actuators, local files, or configuration options)
     * includes listing them, reading the latest value, or updating them to a new value.
     * A resource_id may be a hierarchical topic, where, for example, ???sensors/temp??? may
     * identify a temperature sensor and ???sensor/hum??? a humidity sensor.
     */
    private String resourceId;
    
    /**
     * The measured value in the unit.
     */
    private String value;
    
    private UnitBean unit;
    
    /**
     * The time, when the measurement took place. Note that
     * this is not the time when the database entry has been
     * made.
     */
    private Date timeOfMeasurement;
    
    private String timeZone;
    
    /**
     * The location, where the measaurement took place (optional).
     */
    private GeoCoordinatesBean geocoordinates;
    
    private SensorBean sensor;
    
    private SensorInstanceBean sensorInstance;
    
    /**
     * creates an instance of SensorDataBean
     */
    public SensorDataBean() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean:DA-ELSE
        super();
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean:DA-END
    }
    
    /**
     * getter for the field accountName
     * 
     * 
     * Identifies a group of devices and users. It can be seen
     * as partition of the MQTT topic namespace.
     * 
     * 
     * @return
     */
    @XmlElement(name="accountName")
    public String getAccountName() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getAccountName.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getAccountName.String:DA-ELSE
        return this.accountName;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getAccountName.String:DA-END
    }
    /**
     * setter for the field accountName
     * 
     * 
     * Identifies a group of devices and users. It can be seen
     * as partition of the MQTT topic namespace.
     * 
     * 
     * @param accountName  the accountName
     */
    public void setAccountName(String accountName) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setAccountName.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setAccountName.String:DA-ELSE
        this.accountName = accountName;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setAccountName.String:DA-END
    }
    /**
     * getter for the field clientId
     * 
     * 
     * Identifies a single gateway device within an
     * account (typically the MAC address of a
     * gateway???s primary network interface).
     * The client_id maps to the Client Identifier (Client ID)
     * as defined in the MQTT specifications.
     * 
     * 
     * @return
     */
    @XmlElement(name="clientId")
    public String getClientId() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getClientId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getClientId.String:DA-ELSE
        return this.clientId;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getClientId.String:DA-END
    }
    /**
     * setter for the field clientId
     * 
     * 
     * Identifies a single gateway device within an
     * account (typically the MAC address of a
     * gateway???s primary network interface).
     * The client_id maps to the Client Identifier (Client ID)
     * as defined in the MQTT specifications.
     * 
     * 
     * @param clientId  the clientId
     */
    public void setClientId(String clientId) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setClientId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setClientId.String:DA-ELSE
        this.clientId = clientId;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setClientId.String:DA-END
    }
    /**
     * getter for the field appId
     * 
     * 
     * Identifies an application running on the gateway device.
     * To support multiple versions of the application, it is recommended
     * that a version number be assigned with the app_id (e.g., ???CONF-V1???,
     * ???CONF-V2???, etc.)
     * 
     * 
     * @return
     */
    @XmlElement(name="appId")
    public String getAppId() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getAppId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getAppId.String:DA-ELSE
        return this.appId;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getAppId.String:DA-END
    }
    /**
     * setter for the field appId
     * 
     * 
     * Identifies an application running on the gateway device.
     * To support multiple versions of the application, it is recommended
     * that a version number be assigned with the app_id (e.g., ???CONF-V1???,
     * ???CONF-V2???, etc.)
     * 
     * 
     * @param appId  the appId
     */
    public void setAppId(String appId) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setAppId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setAppId.String:DA-ELSE
        this.appId = appId;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setAppId.String:DA-END
    }
    /**
     * getter for the field resourceId
     * 
     * 
     * Identifies a resource(s) that is owned and managed by a particular application.
     * Management of resources (e.g., sensors, actuators, local files, or configuration options)
     * includes listing them, reading the latest value, or updating them to a new value.
     * A resource_id may be a hierarchical topic, where, for example, ???sensors/temp??? may
     * identify a temperature sensor and ???sensor/hum??? a humidity sensor.
     * 
     * 
     * @return
     */
    @XmlElement(name="resourceId")
    public String getResourceId() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getResourceId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getResourceId.String:DA-ELSE
        return this.resourceId;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getResourceId.String:DA-END
    }
    /**
     * setter for the field resourceId
     * 
     * 
     * Identifies a resource(s) that is owned and managed by a particular application.
     * Management of resources (e.g., sensors, actuators, local files, or configuration options)
     * includes listing them, reading the latest value, or updating them to a new value.
     * A resource_id may be a hierarchical topic, where, for example, ???sensors/temp??? may
     * identify a temperature sensor and ???sensor/hum??? a humidity sensor.
     * 
     * 
     * @param resourceId  the resourceId
     */
    public void setResourceId(String resourceId) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setResourceId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setResourceId.String:DA-ELSE
        this.resourceId = resourceId;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setResourceId.String:DA-END
    }
    /**
     * getter for the field value
     * 
     * 
     * The measured value in the unit.
     * 
     * 
     * @return
     */
    @XmlElement(name="value")
    public String getValue() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getValue.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getValue.String:DA-ELSE
        return this.value;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getValue.String:DA-END
    }
    /**
     * setter for the field value
     * 
     * 
     * The measured value in the unit.
     * 
     * 
     * @param value  the value
     */
    public void setValue(String value) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setValue.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setValue.String:DA-ELSE
        this.value = value;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setValue.String:DA-END
    }
    /**
     * getter for the field unit
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="unit")
    public UnitBean getUnit() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getUnit.UnitBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getUnit.UnitBean:DA-ELSE
        return this.unit;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getUnit.UnitBean:DA-END
    }
    /**
     * setter for the field unit
     * 
     * null
     * 
     * @param unit  the unit
     */
    public void setUnit(UnitBean unit) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setUnit.UnitBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setUnit.UnitBean:DA-ELSE
        this.unit = unit;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setUnit.UnitBean:DA-END
    }
    /**
     * getter for the field timeOfMeasurement
     * 
     * 
     * The time, when the measurement took place. Note that
     * this is not the time when the database entry has been
     * made.
     * 
     * 
     * @return
     */
    @XmlElement(name="timeOfMeasurement")
    public Date getTimeOfMeasurement() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getTimeOfMeasurement.Date:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getTimeOfMeasurement.Date:DA-ELSE
        return this.timeOfMeasurement;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getTimeOfMeasurement.Date:DA-END
    }
    /**
     * setter for the field timeOfMeasurement
     * 
     * 
     * The time, when the measurement took place. Note that
     * this is not the time when the database entry has been
     * made.
     * 
     * 
     * @param timeOfMeasurement  the timeOfMeasurement
     */
    public void setTimeOfMeasurement(Date timeOfMeasurement) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setTimeOfMeasurement.Date:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setTimeOfMeasurement.Date:DA-ELSE
        this.timeOfMeasurement = timeOfMeasurement;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setTimeOfMeasurement.Date:DA-END
    }
    /**
     * getter for the field timeZone
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="timeZone")
    public String getTimeZone() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getTimeZone.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getTimeZone.String:DA-ELSE
        return this.timeZone;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getTimeZone.String:DA-END
    }
    /**
     * setter for the field timeZone
     * 
     * null
     * 
     * @param timeZone  the timeZone
     */
    public void setTimeZone(String timeZone) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setTimeZone.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setTimeZone.String:DA-ELSE
        this.timeZone = timeZone;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setTimeZone.String:DA-END
    }
    /**
     * getter for the field geocoordinates
     * 
     * 
     * The location, where the measaurement took place (optional).
     * 
     * 
     * @return
     */
    @XmlElement(name="geocoordinates")
    public GeoCoordinatesBean getGeocoordinates() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getGeocoordinates.GeoCoordinatesBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getGeocoordinates.GeoCoordinatesBean:DA-ELSE
        return this.geocoordinates;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getGeocoordinates.GeoCoordinatesBean:DA-END
    }
    /**
     * setter for the field geocoordinates
     * 
     * 
     * The location, where the measaurement took place (optional).
     * 
     * 
     * @param geocoordinates  the geocoordinates
     */
    public void setGeocoordinates(GeoCoordinatesBean geocoordinates) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setGeocoordinates.GeoCoordinatesBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setGeocoordinates.GeoCoordinatesBean:DA-ELSE
        this.geocoordinates = geocoordinates;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setGeocoordinates.GeoCoordinatesBean:DA-END
    }
    /**
     * getter for the field sensor
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="sensor")
    public SensorBean getSensor() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getSensor.SensorBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getSensor.SensorBean:DA-ELSE
        return this.sensor;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getSensor.SensorBean:DA-END
    }
    /**
     * setter for the field sensor
     * 
     * null
     * 
     * @param sensor  the sensor
     */
    public void setSensor(SensorBean sensor) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setSensor.SensorBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setSensor.SensorBean:DA-ELSE
        this.sensor = sensor;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setSensor.SensorBean:DA-END
    }
    /**
     * getter for the field sensorInstance
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="sensorInstance")
    public SensorInstanceBean getSensorInstance() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getSensorInstance.SensorInstanceBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getSensorInstance.SensorInstanceBean:DA-ELSE
        return this.sensorInstance;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.getSensorInstance.SensorInstanceBean:DA-END
    }
    /**
     * setter for the field sensorInstance
     * 
     * null
     * 
     * @param sensorInstance  the sensorInstance
     */
    public void setSensorInstance(SensorInstanceBean sensorInstance) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setSensorInstance.SensorInstanceBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setSensorInstance.SensorInstanceBean:DA-ELSE
        this.sensorInstance = sensorInstance;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.setSensorInstance.SensorInstanceBean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataBean.additional.elements.in.type:DA-END
} // end of java type