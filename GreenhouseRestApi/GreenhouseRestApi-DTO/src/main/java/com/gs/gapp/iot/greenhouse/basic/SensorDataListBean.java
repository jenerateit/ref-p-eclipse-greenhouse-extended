package com.gs.gapp.iot.greenhouse.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

/**
 * Holds all collected measurement data.
 */
@XmlRootElement(namespace="com.gs.gapp.iot.greenhouse.basic")
public class SensorDataListBean { // start of class

    private List<SensorDataBean> sensorDataList = new ArrayList<SensorDataBean>();
    
    /**
     * creates an instance of SensorDataListBean
     */
    public SensorDataListBean() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataListBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataListBean:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataListBean:DA-END
    }
    
    /**
     * getter for the field sensorDataList
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="sensorDataList")
    public List<SensorDataBean> getSensorDataList() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataListBean.getSensorDataList.List:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataListBean.getSensorDataList.List:DA-ELSE
        return this.sensorDataList;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataListBean.getSensorDataList.List:DA-END
    }
    /**
     * adder for the field sensorDataList
     * 
     * null
     * 
     * @param element  the element
     */
    public void addSensorDataList(SensorDataBean element) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataListBean.addSensorDataList.SensorDataBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataListBean.addSensorDataList.SensorDataBean:DA-ELSE
        this.sensorDataList.add(element);
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataListBean.addSensorDataList.SensorDataBean:DA-END
    }
    /**
     * remover for the field sensorDataList
     * 
     * null
     * 
     * @param element  the element
     */
    public void removeSensorDataList(SensorDataBean element) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataListBean.removeSensorDataList.SensorDataBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataListBean.removeSensorDataList.SensorDataBean:DA-ELSE
        this.sensorDataList.remove(element);
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataListBean.removeSensorDataList.SensorDataBean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorDataListBean.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorDataListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorDataListBean.additional.elements.in.type:DA-END
} // end of java type