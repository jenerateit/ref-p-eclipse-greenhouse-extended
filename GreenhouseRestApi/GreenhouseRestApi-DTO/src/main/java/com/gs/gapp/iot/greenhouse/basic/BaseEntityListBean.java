package com.gs.gapp.iot.greenhouse.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

/**
 * This entity is the parent of all entities that are defined for this sample.
 * It defines fields that all entities need. In our case here it is the primary key.
 */
@XmlRootElement(namespace="com.gs.gapp.iot.greenhouse.basic")
public class BaseEntityListBean { // start of class

    private List<BaseEntityBean> baseEntityList = new ArrayList<BaseEntityBean>();
    
    /**
     * creates an instance of BaseEntityListBean
     */
    public BaseEntityListBean() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.BaseEntityListBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.BaseEntityListBean:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.basic.BaseEntityListBean:DA-END
    }
    
    /**
     * getter for the field baseEntityList
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="baseEntityList")
    public List<BaseEntityBean> getBaseEntityList() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.BaseEntityListBean.getBaseEntityList.List:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.BaseEntityListBean.getBaseEntityList.List:DA-ELSE
        return this.baseEntityList;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.BaseEntityListBean.getBaseEntityList.List:DA-END
    }
    /**
     * adder for the field baseEntityList
     * 
     * null
     * 
     * @param element  the element
     */
    public void addBaseEntityList(BaseEntityBean element) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.BaseEntityListBean.addBaseEntityList.BaseEntityBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.BaseEntityListBean.addBaseEntityList.BaseEntityBean:DA-ELSE
        this.baseEntityList.add(element);
        //DA-END:com.gs.gapp.iot.greenhouse.basic.BaseEntityListBean.addBaseEntityList.BaseEntityBean:DA-END
    }
    /**
     * remover for the field baseEntityList
     * 
     * null
     * 
     * @param element  the element
     */
    public void removeBaseEntityList(BaseEntityBean element) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.BaseEntityListBean.removeBaseEntityList.BaseEntityBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.BaseEntityListBean.removeBaseEntityList.BaseEntityBean:DA-ELSE
        this.baseEntityList.remove(element);
        //DA-END:com.gs.gapp.iot.greenhouse.basic.BaseEntityListBean.removeBaseEntityList.BaseEntityBean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.basic.BaseEntityListBean.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.BaseEntityListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.basic.BaseEntityListBean.additional.elements.in.type:DA-END
} // end of java type