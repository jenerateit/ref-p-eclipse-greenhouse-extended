package com.gs.gapp.iot.greenhouse.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

/**
 * A sensor instance relates to a real, physical sensor.
 * When for instance 10 identical sensors are being used,
 * there will be 10 entries for them in the SENSORINSTANCE table.
 */
@XmlRootElement(namespace="com.gs.gapp.iot.greenhouse.basic")
public class SensorInstanceListBean { // start of class

    private List<SensorInstanceBean> sensorInstanceList = new ArrayList<SensorInstanceBean>();
    
    /**
     * creates an instance of SensorInstanceListBean
     */
    public SensorInstanceListBean() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean:DA-END
    }
    
    /**
     * getter for the field sensorInstanceList
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="sensorInstanceList")
    public List<SensorInstanceBean> getSensorInstanceList() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean.getSensorInstanceList.List:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean.getSensorInstanceList.List:DA-ELSE
        return this.sensorInstanceList;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean.getSensorInstanceList.List:DA-END
    }
    /**
     * adder for the field sensorInstanceList
     * 
     * null
     * 
     * @param element  the element
     */
    public void addSensorInstanceList(SensorInstanceBean element) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean.addSensorInstanceList.SensorInstanceBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean.addSensorInstanceList.SensorInstanceBean:DA-ELSE
        this.sensorInstanceList.add(element);
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean.addSensorInstanceList.SensorInstanceBean:DA-END
    }
    /**
     * remover for the field sensorInstanceList
     * 
     * null
     * 
     * @param element  the element
     */
    public void removeSensorInstanceList(SensorInstanceBean element) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean.removeSensorInstanceList.SensorInstanceBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean.removeSensorInstanceList.SensorInstanceBean:DA-ELSE
        this.sensorInstanceList.remove(element);
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean.removeSensorInstanceList.SensorInstanceBean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorInstanceListBean.additional.elements.in.type:DA-END
} // end of java type