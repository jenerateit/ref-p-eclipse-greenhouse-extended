package com.gs.gapp.iot.greenhouse.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="com.gs.gapp.iot.greenhouse.basic")
public class UnitListBean { // start of class

    private List<UnitBean> unitList = new ArrayList<UnitBean>();
    
    /**
     * creates an instance of UnitListBean
     */
    public UnitListBean() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.UnitListBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.UnitListBean:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.basic.UnitListBean:DA-END
    }
    
    /**
     * getter for the field unitList
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="unitList")
    public List<UnitBean> getUnitList() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.UnitListBean.getUnitList.List:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.UnitListBean.getUnitList.List:DA-ELSE
        return this.unitList;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.UnitListBean.getUnitList.List:DA-END
    }
    /**
     * adder for the field unitList
     * 
     * null
     * 
     * @param element  the element
     */
    public void addUnitList(UnitBean element) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.UnitListBean.addUnitList.UnitBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.UnitListBean.addUnitList.UnitBean:DA-ELSE
        this.unitList.add(element);
        //DA-END:com.gs.gapp.iot.greenhouse.basic.UnitListBean.addUnitList.UnitBean:DA-END
    }
    /**
     * remover for the field unitList
     * 
     * null
     * 
     * @param element  the element
     */
    public void removeUnitList(UnitBean element) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.UnitListBean.removeUnitList.UnitBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.UnitListBean.removeUnitList.UnitBean:DA-ELSE
        this.unitList.remove(element);
        //DA-END:com.gs.gapp.iot.greenhouse.basic.UnitListBean.removeUnitList.UnitBean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.basic.UnitListBean.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.UnitListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.basic.UnitListBean.additional.elements.in.type:DA-END
} // end of java type