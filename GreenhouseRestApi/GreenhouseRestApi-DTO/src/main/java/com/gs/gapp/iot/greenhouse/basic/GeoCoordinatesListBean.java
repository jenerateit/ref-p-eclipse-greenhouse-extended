package com.gs.gapp.iot.greenhouse.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="com.gs.gapp.iot.greenhouse.basic")
public class GeoCoordinatesListBean { // start of class

    private List<GeoCoordinatesBean> geoCoordinatesList = new ArrayList<GeoCoordinatesBean>();
    
    /**
     * creates an instance of GeoCoordinatesListBean
     */
    public GeoCoordinatesListBean() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesListBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesListBean:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesListBean:DA-END
    }
    
    /**
     * getter for the field geoCoordinatesList
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="geoCoordinatesList")
    public List<GeoCoordinatesBean> getGeoCoordinatesList() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesListBean.getGeoCoordinatesList.List:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesListBean.getGeoCoordinatesList.List:DA-ELSE
        return this.geoCoordinatesList;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesListBean.getGeoCoordinatesList.List:DA-END
    }
    /**
     * adder for the field geoCoordinatesList
     * 
     * null
     * 
     * @param element  the element
     */
    public void addGeoCoordinatesList(GeoCoordinatesBean element) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesListBean.addGeoCoordinatesList.GeoCoordinatesBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesListBean.addGeoCoordinatesList.GeoCoordinatesBean:DA-ELSE
        this.geoCoordinatesList.add(element);
        //DA-END:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesListBean.addGeoCoordinatesList.GeoCoordinatesBean:DA-END
    }
    /**
     * remover for the field geoCoordinatesList
     * 
     * null
     * 
     * @param element  the element
     */
    public void removeGeoCoordinatesList(GeoCoordinatesBean element) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesListBean.removeGeoCoordinatesList.GeoCoordinatesBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesListBean.removeGeoCoordinatesList.GeoCoordinatesBean:DA-ELSE
        this.geoCoordinatesList.remove(element);
        //DA-END:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesListBean.removeGeoCoordinatesList.GeoCoordinatesBean:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesListBean.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesListBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesListBean.additional.elements.in.type:DA-END
} // end of java type