package com.gs.gapp.iot.greenhouse.basic;


import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

@XmlRootElement(namespace="com.gs.gapp.iot.greenhouse.basic")
public class GeoCoordinatesBean { // start of class

    private int lng;
    
    private int lat;
    
    private int alt;
    
    /**
     * creates an instance of GeoCoordinatesBean
     */
    public GeoCoordinatesBean() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean:DA-END
    }
    
    /**
     * getter for the field lng
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="lng")
    public int getLng() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.getLng.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.getLng.int:DA-ELSE
        return this.lng;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.getLng.int:DA-END
    }
    /**
     * setter for the field lng
     * 
     * null
     * 
     * @param lng  the lng
     */
    public void setLng(int lng) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.setLng.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.setLng.int:DA-ELSE
        this.lng = lng;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.setLng.int:DA-END
    }
    /**
     * getter for the field lat
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="lat")
    public int getLat() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.getLat.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.getLat.int:DA-ELSE
        return this.lat;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.getLat.int:DA-END
    }
    /**
     * setter for the field lat
     * 
     * null
     * 
     * @param lat  the lat
     */
    public void setLat(int lat) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.setLat.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.setLat.int:DA-ELSE
        this.lat = lat;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.setLat.int:DA-END
    }
    /**
     * getter for the field alt
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="alt")
    public int getAlt() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.getAlt.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.getAlt.int:DA-ELSE
        return this.alt;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.getAlt.int:DA-END
    }
    /**
     * setter for the field alt
     * 
     * null
     * 
     * @param alt  the alt
     */
    public void setAlt(int alt) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.setAlt.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.setAlt.int:DA-ELSE
        this.alt = alt;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.setAlt.int:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.basic.GeoCoordinatesBean.additional.elements.in.type:DA-END
} // end of java type