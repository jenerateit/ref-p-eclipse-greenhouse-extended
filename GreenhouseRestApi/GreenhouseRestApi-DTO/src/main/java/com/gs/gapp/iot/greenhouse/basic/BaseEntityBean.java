package com.gs.gapp.iot.greenhouse.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.lang.Long;
import javax.xml.bind.annotation.XmlElement;

/**
 * This entity is the parent of all entities that are defined for this sample.
 * It defines fields that all entities need. In our case here it is the primary key.
 */
@XmlRootElement(namespace="com.gs.gapp.iot.greenhouse.basic")
public class BaseEntityBean { // start of class

    private Long pk;
    
    /**
     * creates an instance of BaseEntityBean
     */
    public BaseEntityBean() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.BaseEntityBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.BaseEntityBean:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.basic.BaseEntityBean:DA-END
    }
    
    /**
     * getter for the field pk
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="pk")
    public Long getPk() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.BaseEntityBean.getPk.Long:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.BaseEntityBean.getPk.Long:DA-ELSE
        return this.pk;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.BaseEntityBean.getPk.Long:DA-END
    }
    /**
     * setter for the field pk
     * 
     * null
     * 
     * @param pk  the pk
     */
    public void setPk(Long pk) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.BaseEntityBean.setPk.Long:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.BaseEntityBean.setPk.Long:DA-ELSE
        this.pk = pk;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.BaseEntityBean.setPk.Long:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.basic.BaseEntityBean.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.BaseEntityBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.basic.BaseEntityBean.additional.elements.in.type:DA-END
} // end of java type