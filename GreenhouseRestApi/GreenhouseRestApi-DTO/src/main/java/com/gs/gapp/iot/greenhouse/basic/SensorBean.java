package com.gs.gapp.iot.greenhouse.basic;


import javax.xml.bind.annotation.XmlRootElement;
import java.lang.String;
import javax.xml.bind.annotation.XmlElement;

/**
 * Date that describes a sensor. When for instance 10 idientical sensors
 * are being used, there is only one single entry in the SENSOR table.
 */
@XmlRootElement(namespace="com.gs.gapp.iot.greenhouse.basic")
public class SensorBean extends BaseEntityBean { // start of class

    private String id;
    
    private String name;
    
    /**
     * creates an instance of SensorBean
     */
    public SensorBean() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorBean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorBean:DA-ELSE
        super();
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorBean:DA-END
    }
    
    /**
     * getter for the field id
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="id")
    public String getId() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorBean.getId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorBean.getId.String:DA-ELSE
        return this.id;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorBean.getId.String:DA-END
    }
    /**
     * setter for the field id
     * 
     * null
     * 
     * @param id  the id
     */
    public void setId(String id) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorBean.setId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorBean.setId.String:DA-ELSE
        this.id = id;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorBean.setId.String:DA-END
    }
    /**
     * getter for the field name
     * 
     * null
     * 
     * @return
     */
    @XmlElement(name="name")
    public String getName() {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorBean.getName.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorBean.getName.String:DA-ELSE
        return this.name;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorBean.getName.String:DA-END
    }
    /**
     * setter for the field name
     * 
     * null
     * 
     * @param name  the name
     */
    public void setName(String name) {
        //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorBean.setName.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorBean.setName.String:DA-ELSE
        this.name = name;
        //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorBean.setName.String:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.basic.SensorBean.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.basic.SensorBean.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.basic.SensorBean.additional.elements.in.type:DA-END
} // end of java type