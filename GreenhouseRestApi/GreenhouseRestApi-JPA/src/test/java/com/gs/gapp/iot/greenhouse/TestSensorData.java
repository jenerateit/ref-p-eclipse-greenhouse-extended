package com.gs.gapp.iot.greenhouse;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.BeforeClass;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

/**
 * Holds all collected measurement data.
 */
public class TestSensorData { // start of class

    private EntityManager em;
    
    private static EntityManagerFactory emf;
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorData.setUpBeforeClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorData.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("GREENHOUSE");
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorData.setUpBeforeClass:DA-END
    }
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorData.tearDownAfterClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorData.tearDownAfterClass:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorData.tearDownAfterClass:DA-END
    }
    /**
     */
    @Before
    public void setUp() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorData.setUp:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorData.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorData.setUp:DA-END
    }
    /**
     */
    @After
    public void tearDown() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorData.tearDown:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorData.tearDown:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorData.tearDown:DA-END
    }
    /**
     */
    @Test
    public void testFind() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorData.testFind:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorData.testFind:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorData.testFind:DA-END
    }
    /**
     */
    @Test
    public void testPersist() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorData.testPersist:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorData.testPersist:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorData.testPersist:DA-END
    }
    /**
     */
    @Test
    public void testMerge() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorData.testMerge:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorData.testMerge:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorData.testMerge:DA-END
    }
    /**
     */
    @Test
    public void testRemove() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorData.testRemove:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorData.testRemove:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorData.testRemove:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.TestSensorData.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorData.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.TestSensorData.additional.elements.in.type:DA-END
} // end of java type