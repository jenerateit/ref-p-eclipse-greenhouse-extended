package com.gs.gapp.iot.greenhouse;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.BeforeClass;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class TestUnitDAO { // start of class

    private EntityManager em;
    
    private static EntityManagerFactory emf;
    
    private static UnitDAO dao;
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestUnitDAO.setUpBeforeClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnitDAO.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("GREENHOUSE");
        dao = new UnitDAO();
        //DA-END:com.gs.gapp.iot.greenhouse.TestUnitDAO.setUpBeforeClass:DA-END
    }
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestUnitDAO.tearDownAfterClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnitDAO.tearDownAfterClass:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestUnitDAO.tearDownAfterClass:DA-END
    }
    /**
     */
    @Before
    public void setUp() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestUnitDAO.setUp:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnitDAO.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:com.gs.gapp.iot.greenhouse.TestUnitDAO.setUp:DA-END
    }
    /**
     */
    @After
    public void tearDown() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestUnitDAO.tearDown:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnitDAO.tearDown:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestUnitDAO.tearDown:DA-END
    }
    /**
     */
    @Test
    public void testGet() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestUnitDAO.testGet:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnitDAO.testGet:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestUnitDAO.testGet:DA-END
    }
    /**
     */
    @Test
    public void testGetAll() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestUnitDAO.testGetAll:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnitDAO.testGetAll:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestUnitDAO.testGetAll:DA-END
    }
    /**
     */
    @Test
    public void testCreate() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestUnitDAO.testCreate:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnitDAO.testCreate:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestUnitDAO.testCreate:DA-END
    }
    /**
     */
    @Test
    public void testUpdate() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestUnitDAO.testUpdate:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnitDAO.testUpdate:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestUnitDAO.testUpdate:DA-END
    }
    /**
     */
    @Test
    public void testDelete() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestUnitDAO.testDelete:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnitDAO.testDelete:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestUnitDAO.testDelete:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.TestUnitDAO.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnitDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.TestUnitDAO.additional.elements.in.type:DA-END
} // end of java type