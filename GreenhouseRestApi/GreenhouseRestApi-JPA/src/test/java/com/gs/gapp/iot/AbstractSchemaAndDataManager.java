package com.gs.gapp.iot;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.RollbackException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

/**
 * @author mmt
 *
 */
public abstract class AbstractSchemaAndDataManager {
	
	private EntityManagerFactory entityManagerFactory;
	private EntityManager entityManager;
	private final String persistenceXmlFileName;
	
	/**
	 * @param persistenceXmlFileName
	 */
	protected AbstractSchemaAndDataManager(String persistenceXmlFileName, Map<String, Object> props) {
		this.persistenceXmlFileName = persistenceXmlFileName;
		initEntityManager(props);
	}
	
	/**
	 * 
	 */
	protected AbstractSchemaAndDataManager() {
		this.persistenceXmlFileName = null;
	}

	/**
	 * Init entity manager factory and entity manager from a special persistence xml file for testing purposes.
	 */
	protected void initEntityManager(Map<String, Object> props) {
		if (this.persistenceXmlFileName != null && props != null) {
			Map<String, Object> effectiveProps = new HashMap<>(props);
	    	effectiveProps.put("eclipselink.persistencexml", this.persistenceXmlFileName);
	    	
	    	this.entityManagerFactory = Persistence.createEntityManagerFactory("GREENHOUSE", effectiveProps);
	    	this.entityManager = this.entityManagerFactory.createEntityManager();
		}
	}
	
	public void showConstraintViolations(ConstraintViolation<Throwable> th) {
		System.err.println(th.getMessage());     
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}
	
	/**
	 * If the entity manger got created by code within this class, we must handle transactions ourselves.
	 * 
	 * @return
	 */
	protected boolean isEntityManagerCreatedHere() {
		return getEntityManagerFactory() != null;
	}
	
	/**
	 * @param th
	 */
	protected void checkForConstraintViolationException(Throwable th) {
		ConstraintViolationException constraintViolationException = null;
		
		if (th instanceof RollbackException) {
			RollbackException rollbackException = (RollbackException) th;
			if (rollbackException.getCause() instanceof ConstraintViolationException) {
				constraintViolationException = (ConstraintViolationException) rollbackException.getCause();
			}
		} else if (th instanceof ConstraintViolationException) {
			constraintViolationException = (ConstraintViolationException) th;
		}
		
		if (constraintViolationException != null) {
			for (ConstraintViolation<?> violation : constraintViolationException.getConstraintViolations()) {
				System.out.println("Violated constraint '" + violation.getConstraintDescriptor().getAnnotation() + "' on entity " +
					violation.getLeafBean() + ", property-path:" + violation.getPropertyPath() +", message:" +  violation.getMessage());
			}
		}
	}

	/**
	 * @param args
	 * @param argsList
	 * @param optsList
	 * @param doubleOptsList
	 */
	protected static void fillOptionLists(String[] args, List<String> argsList, List<Option> optsList, List<String> doubleOptsList) {

        for (int i = 0; i < args.length; i++) {
            switch (args[i].charAt(0)) {
            case '-':
                if (args[i].length() < 2) {
                    throw new IllegalArgumentException("Not a valid argument: " + args[i]);
                }
                if (args[i].charAt(1) == '-') {
                    if (args[i].length() < 3) {
                        throw new IllegalArgumentException("Not a valid argument: " + args[i]);
                    }
                    // --opt
                    doubleOptsList.add(args[i].substring(2, args[i].length()));
                } else {
                    if (args.length-1 == i) {
                        throw new IllegalArgumentException("Expected arg after: " + args[i]);
                    }
                    // -opt
                    optsList.add(new Option(args[i], args[i+1]));
                    i++;
                }
                break;
            default:
                // arg
                argsList.add(args[i]);
                break;
            }
        }
	}
	
	/**
     * @author mmt
     *
     */
    protected static class Option {
        private final String flag;
        private final String opt;
        
        public Option(String flag, String opt) { this.flag = flag; this.opt = opt; }
        
		public String getFlag() {
			return flag;
		}
		public String getOpt() {
			return opt;
		}
    }
    
    /**
     * @author mmt
     *
     */
    public static class Options {
    	
    	private final List<String> argsList = new ArrayList<>();  
        private final List<Option> optsList = new ArrayList<>();
        private final List<String> doubleOptsList = new ArrayList<>();
        
        public Options(List<String> argsList, List<Option> optsList, List<String> doubleOptsList) {
        	if (argsList != null) this.argsList.addAll(argsList);
        	if (optsList != null) this.optsList.addAll(optsList);
        	if (doubleOptsList != null) this.doubleOptsList.addAll(doubleOptsList);
        }

		protected List<String> getArgsList() {
			return argsList;
		}

		protected List<Option> getOptsList() {
			return optsList;
		}

		protected List<String> getDoubleOptsList() {
			return doubleOptsList;
		}
        
		/**
		 * @param key
		 * @return
		 */
		protected String getOpt(String key) {
			String keyWithDash = "-" + key;
			for (Option option : getOptsList()) {
				if (option.getFlag().equalsIgnoreCase(keyWithDash)) {
					return option.getOpt();
				}
			}
			
			return null;
		}
    }

    /**
     * @param fileName
     * @return
     */
    @SuppressWarnings("resource")
	protected String getFirstLineOfFile(String fileName) {
    	BufferedReader br = null;
    	
        String line = null;
        
		try {
			br = new BufferedReader(new FileReader(fileName));
			line = br.readLine();
		} catch (FileNotFoundException e) {
			// intentionally ignoring this
		} catch (IOException e) {
			// intentionally ignoring this
		}
		
        return line;
    }
    
    /**
     * @param fileName
     * @return
     */
    @SuppressWarnings("resource")
    protected List<String> getFileContent(String fileName) {
    	BufferedReader br = null;
    	List<String> result = new ArrayList<>();
    	
        String line = null;
        
		try {
			br = new BufferedReader(new FileReader(fileName));
			line = br.readLine();
			
			while (line != null) {
				result.add(line);
				line = br.readLine();
			}
		} catch (FileNotFoundException e) {
			// intentionally ignoring this
		} catch (IOException e) {
			// intentionally ignoring this
		}
		
        return result;
    }
    
    /**
     * @param fileName
     * @return
     */
    protected byte[] getBytesForFile(String fileName) {
    	try {
    		File file = new File(fileName);
    		DataInputStream inputStream = new DataInputStream(new FileInputStream(file));
    		int read = 0;
            int numRead = 0;
            byte[] bytes = new byte[(int) file.length()];
            while (read < bytes.length && (numRead = inputStream.read(bytes, read,
                    bytes.length - read)) >= 0) {
                read = read + numRead;
            }
            inputStream.close();
            return bytes;
    	} catch (IOException ex) {
    		ex.printStackTrace();
    		throw new RuntimeException("failed to read file '" + fileName + "'", ex);
    	}
    }
    
    /**
     * @param fileName
     * @return
     */
    protected byte[] getBytesForImage(String fileName) {
    	try {
    	    BufferedImage bufferedImage = ImageIO.read(new File(fileName));
    	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	    ImageIO.write(bufferedImage, "png", baos);
            return baos.toByteArray();
    	} catch (IOException ex) {
    		ex.printStackTrace();
    		throw new RuntimeException("failed to read file '" + fileName + "'", ex);
    	}
    }
    
    /**
     * @param file
     * @return
     */
    protected String getDocumentType(File file) {
    	if (file.getName().endsWith("pdf")) {
    		return "application/pdf";
    	} else if (file.getName().endsWith("html")) {
    		return "text/html";
    	} else if (file.getName().endsWith("zip")) {
    		return "application/zip";
    	}
    	
    	return null;
    }
    
}
