package com.gs.gapp.iot.greenhouse;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.BeforeClass;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

/**
 * Date that describes a sensor. When for instance 10 idientical sensors
 * are being used, there is only one single entry in the SENSOR table.
 */
public class TestSensorDAO { // start of class

    private EntityManager em;
    
    private static EntityManagerFactory emf;
    
    private static SensorDAO dao;
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDAO.setUpBeforeClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDAO.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("GREENHOUSE");
        dao = new SensorDAO();
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDAO.setUpBeforeClass:DA-END
    }
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDAO.tearDownAfterClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDAO.tearDownAfterClass:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDAO.tearDownAfterClass:DA-END
    }
    /**
     */
    @Before
    public void setUp() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDAO.setUp:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDAO.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDAO.setUp:DA-END
    }
    /**
     */
    @After
    public void tearDown() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDAO.tearDown:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDAO.tearDown:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDAO.tearDown:DA-END
    }
    /**
     */
    @Test
    public void testGet() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDAO.testGet:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDAO.testGet:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDAO.testGet:DA-END
    }
    /**
     */
    @Test
    public void testGetAll() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDAO.testGetAll:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDAO.testGetAll:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDAO.testGetAll:DA-END
    }
    /**
     */
    @Test
    public void testCreate() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDAO.testCreate:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDAO.testCreate:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDAO.testCreate:DA-END
    }
    /**
     */
    @Test
    public void testUpdate() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDAO.testUpdate:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDAO.testUpdate:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDAO.testUpdate:DA-END
    }
    /**
     */
    @Test
    public void testDelete() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDAO.testDelete:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDAO.testDelete:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDAO.testDelete:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDAO.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDAO.additional.elements.in.type:DA-END
} // end of java type