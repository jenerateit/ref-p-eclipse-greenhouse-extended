package com.gs.gapp.iot.greenhouse;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.BeforeClass;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

/**
 * A sensor instance relates to a real, physical sensor.
 * When for instance 10 identical sensors are being used,
 * there will be 10 entries for them in the SENSORINSTANCE table.
 */
public class TestSensorInstanceDAO { // start of class

    private EntityManager em;
    
    private static EntityManagerFactory emf;
    
    private static SensorInstanceDAO dao;
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.setUpBeforeClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("GREENHOUSE");
        dao = new SensorInstanceDAO();
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.setUpBeforeClass:DA-END
    }
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.tearDownAfterClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.tearDownAfterClass:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.tearDownAfterClass:DA-END
    }
    /**
     */
    @Before
    public void setUp() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.setUp:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.setUp:DA-END
    }
    /**
     */
    @After
    public void tearDown() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.tearDown:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.tearDown:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.tearDown:DA-END
    }
    /**
     */
    @Test
    public void testGet() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.testGet:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.testGet:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.testGet:DA-END
    }
    /**
     */
    @Test
    public void testGetAll() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.testGetAll:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.testGetAll:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.testGetAll:DA-END
    }
    /**
     */
    @Test
    public void testCreate() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.testCreate:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.testCreate:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.testCreate:DA-END
    }
    /**
     */
    @Test
    public void testUpdate() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.testUpdate:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.testUpdate:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.testUpdate:DA-END
    }
    /**
     */
    @Test
    public void testDelete() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.testDelete:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.testDelete:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.testDelete:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstanceDAO.additional.elements.in.type:DA-END
} // end of java type