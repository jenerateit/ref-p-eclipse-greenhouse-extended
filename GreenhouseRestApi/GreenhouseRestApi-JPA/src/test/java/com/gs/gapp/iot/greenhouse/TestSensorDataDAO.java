package com.gs.gapp.iot.greenhouse;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.BeforeClass;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

/**
 * Holds all collected measurement data.
 */
public class TestSensorDataDAO { // start of class

    private EntityManager em;
    
    private static EntityManagerFactory emf;
    
    private static SensorDataDAO dao;
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.setUpBeforeClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("GREENHOUSE");
        dao = new SensorDataDAO();
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.setUpBeforeClass:DA-END
    }
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.tearDownAfterClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.tearDownAfterClass:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.tearDownAfterClass:DA-END
    }
    /**
     */
    @Before
    public void setUp() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.setUp:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.setUp:DA-END
    }
    /**
     */
    @After
    public void tearDown() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.tearDown:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.tearDown:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.tearDown:DA-END
    }
    /**
     */
    @Test
    public void testGet() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.testGet:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.testGet:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.testGet:DA-END
    }
    /**
     */
    @Test
    public void testGetAll() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.testGetAll:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.testGetAll:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.testGetAll:DA-END
    }
    /**
     */
    @Test
    public void testCreate() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.testCreate:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.testCreate:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.testCreate:DA-END
    }
    /**
     */
    @Test
    public void testUpdate() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.testUpdate:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.testUpdate:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.testUpdate:DA-END
    }
    /**
     */
    @Test
    public void testDelete() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.testDelete:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.testDelete:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.testDelete:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.TestSensorDataDAO.additional.elements.in.type:DA-END
} // end of java type