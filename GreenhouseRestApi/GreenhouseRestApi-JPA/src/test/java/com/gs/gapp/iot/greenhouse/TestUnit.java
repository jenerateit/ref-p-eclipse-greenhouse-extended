package com.gs.gapp.iot.greenhouse;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.BeforeClass;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class TestUnit { // start of class

    private EntityManager em;
    
    private static EntityManagerFactory emf;
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestUnit.setUpBeforeClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnit.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("GREENHOUSE");
        //DA-END:com.gs.gapp.iot.greenhouse.TestUnit.setUpBeforeClass:DA-END
    }
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestUnit.tearDownAfterClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnit.tearDownAfterClass:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestUnit.tearDownAfterClass:DA-END
    }
    /**
     */
    @Before
    public void setUp() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestUnit.setUp:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnit.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:com.gs.gapp.iot.greenhouse.TestUnit.setUp:DA-END
    }
    /**
     */
    @After
    public void tearDown() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestUnit.tearDown:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnit.tearDown:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestUnit.tearDown:DA-END
    }
    /**
     */
    @Test
    public void testFind() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestUnit.testFind:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnit.testFind:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestUnit.testFind:DA-END
    }
    /**
     */
    @Test
    public void testPersist() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestUnit.testPersist:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnit.testPersist:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestUnit.testPersist:DA-END
    }
    /**
     */
    @Test
    public void testMerge() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestUnit.testMerge:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnit.testMerge:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestUnit.testMerge:DA-END
    }
    /**
     */
    @Test
    public void testRemove() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestUnit.testRemove:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnit.testRemove:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestUnit.testRemove:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.TestUnit.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.TestUnit.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.TestUnit.additional.elements.in.type:DA-END
} // end of java type