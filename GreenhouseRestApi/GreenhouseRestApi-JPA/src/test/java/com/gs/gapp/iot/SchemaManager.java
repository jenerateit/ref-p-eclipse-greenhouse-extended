package com.gs.gapp.iot;
/*
 * Copyright (c) 2008 Generative Software http://www.generative-software.com
 * All rights reserved.
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.persistence.config.PersistenceUnitProperties;

/**
 * @author mmt
 *
 */
public class SchemaManager extends AbstractSchemaAndDataManager { // start of class

    @SuppressWarnings("unused")
	private RecreateSchemaOptions options;
    
    /*
     * @param options
     * @param persistenceXmlFileName
     */
    private SchemaManager(RecreateSchemaOptions options, String persistenceXmlFileName) {
    	super(persistenceXmlFileName, getEntityFactoryProps(options));
    	this.options = options;
    }
    
    /**
	 * @return
	 */
	private static Map<String, Object> getEntityFactoryProps(RecreateSchemaOptions options) {
		Map<String, Object> props = new HashMap<>();
		if (options.dropAndCreateTables()) {
    		props.put(PersistenceUnitProperties.DDL_GENERATION, PersistenceUnitProperties.DROP_AND_CREATE);
    		props.put(PersistenceUnitProperties.DDL_GENERATION_MODE, PersistenceUnitProperties.DDL_BOTH_GENERATION);
    		props.put(PersistenceUnitProperties.CREATE_JDBC_DDL_FILE, "database/createDDL.sql");
    		props.put(PersistenceUnitProperties.DROP_JDBC_DDL_FILE, "database/dropDDL.sql");
    	}
		return props;
	}
    
    /**
     * @param args
     */
    public static void main(String [] args) {
    	
    	List<String> argsList = new ArrayList<>();
        List<Option> optsList = new ArrayList<>();
        List<String> doubleOptsList = new ArrayList<>();
        
        AbstractSchemaAndDataManager.fillOptionLists(args, argsList, optsList, doubleOptsList);
        
        RecreateSchemaOptions schemaAndDataManagerOptions = new RecreateSchemaOptions(argsList, optsList, doubleOptsList);
    	SchemaManager schemaAndDataManager = new SchemaManager(schemaAndDataManagerOptions, schemaAndDataManagerOptions.getPersistenceXmlFileName());
        schemaAndDataManager.manageSchema();
    }
    
    /**
     * 
     */
    public void manageSchema() {
    	
    }
    
	/**
     * @author mmt
     *
     */
    public static class RecreateSchemaOptions extends Options {
    	
    	public static final String DROP_AND_CREATE_TABLES = "dropAndCreateTables";
    	public static final String OPT_PERSISTENCE_XML = "persistenceXml";
    	

		/**
		 * @param argsList
		 * @param optsList
		 * @param doubleOptsList
		 */
		public RecreateSchemaOptions(List<String> argsList, List<Option> optsList, List<String> doubleOptsList) {
			super(argsList, optsList, doubleOptsList);
		}

		/**
		 * @return
		 */
		public String getPersistenceXmlFileName() {
			return getOpt(OPT_PERSISTENCE_XML);
		}
		
		/**
		 * @return
		 */
		public boolean dropAndCreateTables() {
			return getDoubleOptsList().contains(DROP_AND_CREATE_TABLES);
		}
	}
    
} // end of java type
