package com.gs.gapp.iot.greenhouse;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.BeforeClass;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

/**
 * Date that describes a sensor. When for instance 10 idientical sensors
 * are being used, there is only one single entry in the SENSOR table.
 */
public class TestSensor { // start of class

    private EntityManager em;
    
    private static EntityManagerFactory emf;
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensor.setUpBeforeClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensor.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("GREENHOUSE");
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensor.setUpBeforeClass:DA-END
    }
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensor.tearDownAfterClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensor.tearDownAfterClass:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensor.tearDownAfterClass:DA-END
    }
    /**
     */
    @Before
    public void setUp() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensor.setUp:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensor.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensor.setUp:DA-END
    }
    /**
     */
    @After
    public void tearDown() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensor.tearDown:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensor.tearDown:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensor.tearDown:DA-END
    }
    /**
     */
    @Test
    public void testFind() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensor.testFind:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensor.testFind:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensor.testFind:DA-END
    }
    /**
     */
    @Test
    public void testPersist() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensor.testPersist:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensor.testPersist:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensor.testPersist:DA-END
    }
    /**
     */
    @Test
    public void testMerge() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensor.testMerge:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensor.testMerge:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensor.testMerge:DA-END
    }
    /**
     */
    @Test
    public void testRemove() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensor.testRemove:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensor.testRemove:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensor.testRemove:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.TestSensor.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.TestSensor.additional.elements.in.type:DA-END
} // end of java type