package com.gs.gapp.iot.greenhouse;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.junit.BeforeClass;
import javax.persistence.Persistence;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

/**
 * A sensor instance relates to a real, physical sensor.
 * When for instance 10 identical sensors are being used,
 * there will be 10 entries for them in the SENSORINSTANCE table.
 */
public class TestSensorInstance { // start of class

    private EntityManager em;
    
    private static EntityManagerFactory emf;
    
    /**
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstance.setUpBeforeClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstance.setUpBeforeClass:DA-ELSE
        emf = Persistence.createEntityManagerFactory("GREENHOUSE");
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstance.setUpBeforeClass:DA-END
    }
    /**
     */
    @AfterClass
    public static void tearDownAfterClass() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstance.tearDownAfterClass:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstance.tearDownAfterClass:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstance.tearDownAfterClass:DA-END
    }
    /**
     */
    @Before
    public void setUp() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstance.setUp:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstance.setUp:DA-ELSE
        em = emf.createEntityManager();
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstance.setUp:DA-END
    }
    /**
     */
    @After
    public void tearDown() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstance.tearDown:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstance.tearDown:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstance.tearDown:DA-END
    }
    /**
     */
    @Test
    public void testFind() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstance.testFind:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstance.testFind:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstance.testFind:DA-END
    }
    /**
     */
    @Test
    public void testPersist() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstance.testPersist:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstance.testPersist:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstance.testPersist:DA-END
    }
    /**
     */
    @Test
    public void testMerge() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstance.testMerge:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstance.testMerge:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstance.testMerge:DA-END
    }
    /**
     */
    @Test
    public void testRemove() {
        //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstance.testRemove:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstance.testRemove:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstance.testRemove:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.TestSensorInstance.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.TestSensorInstance.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.TestSensorInstance.additional.elements.in.type:DA-END
} // end of java type