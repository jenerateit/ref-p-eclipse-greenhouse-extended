
import javax.persistence.EntityManager;

/**
 * the module, name has to be same as file name
 */
public class PersistenceTestDataGenerator { // start of class

    /**
     * 
     * @param entityManager
     */
    public void createTestData(EntityManager entityManager) {
        //DA-START:PersistenceTestDataGenerator.createTestData.EntityManager:DA-START
        //DA-ELSE:PersistenceTestDataGenerator.createTestData.EntityManager:DA-ELSE
        return;
        //DA-END:PersistenceTestDataGenerator.createTestData.EntityManager:DA-END
    }
    
    //DA-START:PersistenceTestDataGenerator.additional.elements.in.type:DA-START
    //DA-ELSE:PersistenceTestDataGenerator.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:PersistenceTestDataGenerator.additional.elements.in.type:DA-END
} // end of java type