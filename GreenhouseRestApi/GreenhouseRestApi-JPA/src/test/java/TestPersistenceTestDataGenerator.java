
import org.junit.Test;

/**
 * the module, name has to be same as file name
 */
public class TestPersistenceTestDataGenerator { // start of class

    /**
     */
    @Test
    public void testCreateTestData() {
        //DA-START:TestPersistenceTestDataGenerator.testCreateTestData:DA-START
        //DA-ELSE:TestPersistenceTestDataGenerator.testCreateTestData:DA-ELSE
        //DA-END:TestPersistenceTestDataGenerator.testCreateTestData:DA-END
    }
    
    //DA-START:TestPersistenceTestDataGenerator.additional.elements.in.type:DA-START
    //DA-ELSE:TestPersistenceTestDataGenerator.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:TestPersistenceTestDataGenerator.additional.elements.in.type:DA-END
} // end of java type