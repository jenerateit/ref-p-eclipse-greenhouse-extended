package com.gs.gapp.iot.greenhouse;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;
import java.lang.String;

/**
 * JPA Metamodel description for {@link com.gs.gapp.iot.greenhouse.Sensor}.
 * 
 * Date that describes a sensor. When for instance 10 idientical sensors
 * are being used, there is only one single entry in the SENSOR table.
 * 
 * @see com.gs.gapp.iot.greenhouse.Sensor
 */
@StaticMetamodel(value=com.gs.gapp.iot.greenhouse.Sensor.class)
public class Sensor_ extends BaseEntity_ { // start of class

    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.Sensor#getId()}.
     * 
     * @see com.gs.gapp.iot.greenhouse.Sensor#getId()
     */
    public static volatile SingularAttribute<Sensor, String> id;
    
    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.Sensor#getName()}.
     * 
     * @see com.gs.gapp.iot.greenhouse.Sensor#getName()
     */
    public static volatile SingularAttribute<Sensor, String> name;
    
    
    //DA-START:com.gs.gapp.iot.greenhouse.Sensor_.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.Sensor_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.Sensor_.additional.elements.in.type:DA-END
} // end of java type