package com.gs.gapp.iot.greenhouse;


import javax.persistence.EntityManager;
import java.lang.Object;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

/**
 * Date that describes a sensor. When for instance 10 idientical sensors
 * are being used, there is only one single entry in the SENSOR table.
 */
public class SensorDAO { // start of class

    /**
     * 
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static Sensor get(EntityManager entityManager, Object id) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorDAO.get.EntityManager.Object.Sensor:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDAO.get.EntityManager.Object.Sensor:DA-ELSE
        return entityManager.find(Sensor.class, id);
        //DA-END:com.gs.gapp.iot.greenhouse.SensorDAO.get.EntityManager.Object.Sensor:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<Sensor> getAll(EntityManager entityManager) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<Sensor> cq = cb.createQuery(Sensor.class);
        cq.from(Sensor.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:com.gs.gapp.iot.greenhouse.SensorDAO.getAll.EntityManager.Collection:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static Sensor create(EntityManager entityManager, Sensor entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorDAO.create.EntityManager.Sensor.Sensor:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDAO.create.EntityManager.Sensor.Sensor:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorDAO.create.EntityManager.Sensor.Sensor:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static Sensor update(EntityManager entityManager, Sensor entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorDAO.update.EntityManager.Sensor.Sensor:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDAO.update.EntityManager.Sensor.Sensor:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:com.gs.gapp.iot.greenhouse.SensorDAO.update.EntityManager.Sensor.Sensor:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, Sensor entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorDAO.delete.EntityManager.Sensor:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDAO.delete.EntityManager.Sensor:DA-ELSE
        entityManager.remove(entity);
        //DA-END:com.gs.gapp.iot.greenhouse.SensorDAO.delete.EntityManager.Sensor:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.SensorDAO.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.SensorDAO.additional.elements.in.type:DA-END
} // end of java type