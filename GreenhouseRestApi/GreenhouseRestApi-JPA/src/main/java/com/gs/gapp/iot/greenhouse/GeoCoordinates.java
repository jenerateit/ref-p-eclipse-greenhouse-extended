package com.gs.gapp.iot.greenhouse;


import javax.persistence.Embeddable;
import java.io.Serializable;
import java.lang.Object;
import javax.persistence.Basic;
import javax.persistence.Column;

@Embeddable
public class GeoCoordinates implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private int lng;
    
    private int lat;
    
    private int alt;
    
    /**
     * 
     * @return
     */
    public int hashCode() {
        //DA-START:com.gs.gapp.iot.greenhouse.GeoCoordinates.hashCode.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.GeoCoordinates.hashCode.int:DA-ELSE
        return 0;
        //DA-END:com.gs.gapp.iot.greenhouse.GeoCoordinates.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    public boolean equals(Object arg0) {
        //DA-START:com.gs.gapp.iot.greenhouse.GeoCoordinates.equals.Object.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.GeoCoordinates.equals.Object.boolean:DA-ELSE
        return true;
        //DA-END:com.gs.gapp.iot.greenhouse.GeoCoordinates.equals.Object.boolean:DA-END
    }
    /**
     * getter for the field lng
     * 
     * null
     * 
     * @return
     */
    @Basic
    @Column(name="lng", unique=false, nullable=true)
    public int getLng() {
        //DA-START:com.gs.gapp.iot.greenhouse.GeoCoordinates.getLng.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.GeoCoordinates.getLng.int:DA-ELSE
        return this.lng;
        //DA-END:com.gs.gapp.iot.greenhouse.GeoCoordinates.getLng.int:DA-END
    }
    /**
     * setter for the field lng
     * 
     * null
     * 
     * @param lng  the lng
     */
    public void setLng(int lng) {
        //DA-START:com.gs.gapp.iot.greenhouse.GeoCoordinates.setLng.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.GeoCoordinates.setLng.int:DA-ELSE
        this.lng = lng;
        //DA-END:com.gs.gapp.iot.greenhouse.GeoCoordinates.setLng.int:DA-END
    }
    /**
     * getter for the field lat
     * 
     * null
     * 
     * @return
     */
    @Basic
    @Column(name="lat", unique=false, nullable=true)
    public int getLat() {
        //DA-START:com.gs.gapp.iot.greenhouse.GeoCoordinates.getLat.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.GeoCoordinates.getLat.int:DA-ELSE
        return this.lat;
        //DA-END:com.gs.gapp.iot.greenhouse.GeoCoordinates.getLat.int:DA-END
    }
    /**
     * setter for the field lat
     * 
     * null
     * 
     * @param lat  the lat
     */
    public void setLat(int lat) {
        //DA-START:com.gs.gapp.iot.greenhouse.GeoCoordinates.setLat.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.GeoCoordinates.setLat.int:DA-ELSE
        this.lat = lat;
        //DA-END:com.gs.gapp.iot.greenhouse.GeoCoordinates.setLat.int:DA-END
    }
    /**
     * getter for the field alt
     * 
     * null
     * 
     * @return
     */
    @Basic
    @Column(name="alt", unique=false, nullable=true)
    public int getAlt() {
        //DA-START:com.gs.gapp.iot.greenhouse.GeoCoordinates.getAlt.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.GeoCoordinates.getAlt.int:DA-ELSE
        return this.alt;
        //DA-END:com.gs.gapp.iot.greenhouse.GeoCoordinates.getAlt.int:DA-END
    }
    /**
     * setter for the field alt
     * 
     * null
     * 
     * @param alt  the alt
     */
    public void setAlt(int alt) {
        //DA-START:com.gs.gapp.iot.greenhouse.GeoCoordinates.setAlt.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.GeoCoordinates.setAlt.int:DA-ELSE
        this.alt = alt;
        //DA-END:com.gs.gapp.iot.greenhouse.GeoCoordinates.setAlt.int:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.GeoCoordinates.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.GeoCoordinates.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.GeoCoordinates.additional.elements.in.type:DA-END
} // end of java type