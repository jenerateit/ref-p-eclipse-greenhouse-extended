package com.gs.gapp.iot.greenhouse;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;
import java.lang.Long;

/**
 * JPA Metamodel description for {@link com.gs.gapp.iot.greenhouse.BaseEntity}.
 * 
 * This entity is the parent of all entities that are defined for this sample.
 * It defines fields that all entities need. In our case here it is the primary key.
 * 
 * @see com.gs.gapp.iot.greenhouse.BaseEntity
 */
@StaticMetamodel(value=com.gs.gapp.iot.greenhouse.BaseEntity.class)
public class BaseEntity_ { // start of class

    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.BaseEntity#getPk()}.
     * 
     * @see com.gs.gapp.iot.greenhouse.BaseEntity#getPk()
     */
    public static volatile SingularAttribute<BaseEntity, Long> pk;
    
    
    //DA-START:com.gs.gapp.iot.greenhouse.BaseEntity_.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntity_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.BaseEntity_.additional.elements.in.type:DA-END
} // end of java type