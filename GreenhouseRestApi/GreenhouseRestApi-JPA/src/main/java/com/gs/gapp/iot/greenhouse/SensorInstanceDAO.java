package com.gs.gapp.iot.greenhouse;


import javax.persistence.EntityManager;
import java.lang.Object;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

/**
 * A sensor instance relates to a real, physical sensor.
 * When for instance 10 identical sensors are being used,
 * there will be 10 entries for them in the SENSORINSTANCE table.
 */
public class SensorInstanceDAO { // start of class

    /**
     * 
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static SensorInstance get(EntityManager entityManager, Object id) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.get.EntityManager.Object.SensorInstance:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.get.EntityManager.Object.SensorInstance:DA-ELSE
        return entityManager.find(SensorInstance.class, id);
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.get.EntityManager.Object.SensorInstance:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<SensorInstance> getAll(EntityManager entityManager) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<SensorInstance> cq = cb.createQuery(SensorInstance.class);
        cq.from(SensorInstance.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.getAll.EntityManager.Collection:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static SensorInstance create(EntityManager entityManager, SensorInstance entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.create.EntityManager.SensorInstance.SensorInstance:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.create.EntityManager.SensorInstance.SensorInstance:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.create.EntityManager.SensorInstance.SensorInstance:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static SensorInstance update(EntityManager entityManager, SensorInstance entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.update.EntityManager.SensorInstance.SensorInstance:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.update.EntityManager.SensorInstance.SensorInstance:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.update.EntityManager.SensorInstance.SensorInstance:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, SensorInstance entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.delete.EntityManager.SensorInstance:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.delete.EntityManager.SensorInstance:DA-ELSE
        entityManager.remove(entity);
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.delete.EntityManager.SensorInstance:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.SensorInstanceDAO.additional.elements.in.type:DA-END
} // end of java type