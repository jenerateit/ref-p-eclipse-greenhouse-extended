package com.gs.gapp.iot.greenhouse;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;
import java.lang.Integer;

/**
 * JPA Metamodel description for {@link com.gs.gapp.iot.greenhouse.GeoCoordinates}.
 * 
 * @see com.gs.gapp.iot.greenhouse.GeoCoordinates
 */
@StaticMetamodel(value=com.gs.gapp.iot.greenhouse.GeoCoordinates.class)
public class GeoCoordinates_ { // start of class

    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.GeoCoordinates#getLng()}.
     * 
     * @see com.gs.gapp.iot.greenhouse.GeoCoordinates#getLng()
     */
    public static volatile SingularAttribute<GeoCoordinates, Integer> lng;
    
    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.GeoCoordinates#getLat()}.
     * 
     * @see com.gs.gapp.iot.greenhouse.GeoCoordinates#getLat()
     */
    public static volatile SingularAttribute<GeoCoordinates, Integer> lat;
    
    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.GeoCoordinates#getAlt()}.
     * 
     * @see com.gs.gapp.iot.greenhouse.GeoCoordinates#getAlt()
     */
    public static volatile SingularAttribute<GeoCoordinates, Integer> alt;
    
    
    //DA-START:com.gs.gapp.iot.greenhouse.GeoCoordinates_.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.GeoCoordinates_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.GeoCoordinates_.additional.elements.in.type:DA-END
} // end of java type