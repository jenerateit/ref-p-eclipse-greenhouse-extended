package com.gs.gapp.iot.greenhouse;


import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EntityListeners;
import java.io.Serializable;
import java.lang.String;
import java.lang.Object;
import java.lang.Override;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import java.lang.Long;
import javax.persistence.Basic;
import javax.persistence.Column;

/**
 * Date that describes a sensor. When for instance 10 idientical sensors
 * are being used, there is only one single entry in the SENSOR table.
 */
//DA-START:greenhouse.Sensor:DA-START
//DA-ELSE:greenhouse.Sensor:DA-ELSE
@Entity
@Table(name="sensor")
@EntityListeners(value={com.gs.gapp.iot.greenhouse.SensorLifecycleListener.class})
//DA-END:greenhouse.Sensor:DA-END

public class Sensor extends BaseEntity implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private String id;
    
    private String name;
    
    /**
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.Sensor.hashCode.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.Sensor.hashCode.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.Sensor.hashCode.annotations:DA-END
    public int hashCode() {
        //DA-START:com.gs.gapp.iot.greenhouse.Sensor.hashCode.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.Sensor.hashCode.int:DA-ELSE
        int result = super.hashCode();
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.Sensor.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.Sensor.equals.Object.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.Sensor.equals.Object.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.Sensor.equals.Object.annotations:DA-END
    public boolean equals(Object arg0) {
        //DA-START:com.gs.gapp.iot.greenhouse.Sensor.equals.Object.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.Sensor.equals.Object.boolean:DA-ELSE
        if (this == arg0)
            return true;
        if (!super.equals(arg0))
            return false;
        if (getClass() != arg0.getClass())
            return false;
        return true;
        //DA-END:com.gs.gapp.iot.greenhouse.Sensor.equals.Object.boolean:DA-END
    }
    /**
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.Sensor.getPk.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.Sensor.getPk.annotations:DA-ELSE
    @Override
    @Id
    @GeneratedValue(generator="sensor_seq", strategy=GenerationType.AUTO)
    @SequenceGenerator(name="sensor_seq", sequenceName="sensor_seq", initialValue=1, allocationSize=1)
    //DA-END:com.gs.gapp.iot.greenhouse.Sensor.getPk.annotations:DA-END
    public Long getPk() {
        //DA-START:com.gs.gapp.iot.greenhouse.Sensor.getPk.Long:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.Sensor.getPk.Long:DA-ELSE
        return super.getPk();
        //DA-END:com.gs.gapp.iot.greenhouse.Sensor.getPk.Long:DA-END
    }
    /**
     * getter for the field id
     * 
     * null
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.Sensor.getId.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.Sensor.getId.annotations:DA-ELSE
    @Basic
    @Column(name="id", unique=false, nullable=true)
    //DA-END:com.gs.gapp.iot.greenhouse.Sensor.getId.annotations:DA-END
    public String getId() {
        //DA-START:com.gs.gapp.iot.greenhouse.Sensor.getId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.Sensor.getId.String:DA-ELSE
        return this.id;
        //DA-END:com.gs.gapp.iot.greenhouse.Sensor.getId.String:DA-END
    }
    /**
     * setter for the field id
     * 
     * null
     * 
     * @param id  the id
     */
    //DA-START:com.gs.gapp.iot.greenhouse.Sensor.setId.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.Sensor.setId.String.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.Sensor.setId.String.annotations:DA-END
    public void setId(String id) {
        //DA-START:com.gs.gapp.iot.greenhouse.Sensor.setId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.Sensor.setId.String:DA-ELSE
        this.id = id;
        //DA-END:com.gs.gapp.iot.greenhouse.Sensor.setId.String:DA-END
    }
    /**
     * getter for the field name
     * 
     * null
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.Sensor.getName.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.Sensor.getName.annotations:DA-ELSE
    @Basic
    @Column(name="name", unique=false, nullable=true)
    //DA-END:com.gs.gapp.iot.greenhouse.Sensor.getName.annotations:DA-END
    public String getName() {
        //DA-START:com.gs.gapp.iot.greenhouse.Sensor.getName.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.Sensor.getName.String:DA-ELSE
        return this.name;
        //DA-END:com.gs.gapp.iot.greenhouse.Sensor.getName.String:DA-END
    }
    /**
     * setter for the field name
     * 
     * null
     * 
     * @param name  the name
     */
    //DA-START:com.gs.gapp.iot.greenhouse.Sensor.setName.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.Sensor.setName.String.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.Sensor.setName.String.annotations:DA-END
    public void setName(String name) {
        //DA-START:com.gs.gapp.iot.greenhouse.Sensor.setName.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.Sensor.setName.String:DA-ELSE
        this.name = name;
        //DA-END:com.gs.gapp.iot.greenhouse.Sensor.setName.String:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.Sensor.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.Sensor.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.Sensor.additional.elements.in.type:DA-END
} // end of java type