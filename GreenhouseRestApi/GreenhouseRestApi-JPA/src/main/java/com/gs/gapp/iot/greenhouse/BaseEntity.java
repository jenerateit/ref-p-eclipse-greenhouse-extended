package com.gs.gapp.iot.greenhouse;


import javax.persistence.MappedSuperclass;
import javax.persistence.EntityListeners;
import java.io.Serializable;
import java.lang.Long;
import java.lang.Object;
import javax.persistence.Basic;
import javax.persistence.Id;
import javax.persistence.Column;

/**
 * This entity is the parent of all entities that are defined for this sample.
 * It defines fields that all entities need. In our case here it is the primary key.
 */
//DA-START:greenhouse.BaseEntity:DA-START
//DA-ELSE:greenhouse.BaseEntity:DA-ELSE
@MappedSuperclass
@EntityListeners(value={com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.class})
//DA-END:greenhouse.BaseEntity:DA-END

public class BaseEntity implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private Long pk;
    
    /**
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.BaseEntity.hashCode.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntity.hashCode.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.BaseEntity.hashCode.annotations:DA-END
    public int hashCode() {
        //DA-START:com.gs.gapp.iot.greenhouse.BaseEntity.hashCode.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntity.hashCode.int:DA-ELSE
        int result = super.hashCode();
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.BaseEntity.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.BaseEntity.equals.Object.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntity.equals.Object.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.BaseEntity.equals.Object.annotations:DA-END
    public boolean equals(Object arg0) {
        //DA-START:com.gs.gapp.iot.greenhouse.BaseEntity.equals.Object.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntity.equals.Object.boolean:DA-ELSE
        if (this == arg0)
            return true;
        if (getClass() != arg0.getClass())
            return false;
        return true;
        //DA-END:com.gs.gapp.iot.greenhouse.BaseEntity.equals.Object.boolean:DA-END
    }
    /**
     * getter for the field pk
     * 
     * null
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.BaseEntity.getPk.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntity.getPk.annotations:DA-ELSE
    @Basic
    @Id
    @Column(name="pk", nullable=false)
    //DA-END:com.gs.gapp.iot.greenhouse.BaseEntity.getPk.annotations:DA-END
    public Long getPk() {
        //DA-START:com.gs.gapp.iot.greenhouse.BaseEntity.getPk.Long:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntity.getPk.Long:DA-ELSE
        return this.pk;
        //DA-END:com.gs.gapp.iot.greenhouse.BaseEntity.getPk.Long:DA-END
    }
    /**
     * setter for the field pk
     * 
     * null
     * 
     * @param pk  the pk
     */
    //DA-START:com.gs.gapp.iot.greenhouse.BaseEntity.setPk.Long.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntity.setPk.Long.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.BaseEntity.setPk.Long.annotations:DA-END
    public void setPk(Long pk) {
        //DA-START:com.gs.gapp.iot.greenhouse.BaseEntity.setPk.Long:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntity.setPk.Long:DA-ELSE
        this.pk = pk;
        //DA-END:com.gs.gapp.iot.greenhouse.BaseEntity.setPk.Long:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.BaseEntity.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntity.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.BaseEntity.additional.elements.in.type:DA-END
} // end of java type