package com.gs.gapp.iot.greenhouse;


import javax.persistence.PreUpdate;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PostRemove;
import javax.persistence.PostLoad;

public class UnitLifecycleListener { // start of class

    /**
     * 
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(Unit entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.preUpdate.Unit:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.preUpdate.Unit:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.preUpdate.Unit:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(Unit entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.postUpdate.Unit:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.postUpdate.Unit:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.postUpdate.Unit:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(Unit entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.prePersist.Unit:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.prePersist.Unit:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.prePersist.Unit:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(Unit entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.postPersist.Unit:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.postPersist.Unit:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.postPersist.Unit:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(Unit entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.preRemove.Unit:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.preRemove.Unit:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.preRemove.Unit:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(Unit entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.postRemove.Unit:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.postRemove.Unit:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.postRemove.Unit:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(Unit entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.postLoad.Unit:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.postLoad.Unit:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.postLoad.Unit:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.UnitLifecycleListener.additional.elements.in.type:DA-END
} // end of java type