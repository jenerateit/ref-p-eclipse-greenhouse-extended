package com.gs.gapp.iot.greenhouse;


import javax.persistence.PreUpdate;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PostRemove;
import javax.persistence.PostLoad;

/**
 * Holds all collected measurement data.
 */
public class SensorDataLifecycleListener { // start of class

    /**
     * 
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(SensorData entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.preUpdate.SensorData:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.preUpdate.SensorData:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.preUpdate.SensorData:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(SensorData entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.postUpdate.SensorData:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.postUpdate.SensorData:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.postUpdate.SensorData:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(SensorData entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.prePersist.SensorData:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.prePersist.SensorData:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.prePersist.SensorData:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(SensorData entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.postPersist.SensorData:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.postPersist.SensorData:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.postPersist.SensorData:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(SensorData entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.preRemove.SensorData:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.preRemove.SensorData:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.preRemove.SensorData:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(SensorData entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.postRemove.SensorData:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.postRemove.SensorData:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.postRemove.SensorData:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(SensorData entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.postLoad.SensorData:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.postLoad.SensorData:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.postLoad.SensorData:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.additional.elements.in.type:DA-END
} // end of java type