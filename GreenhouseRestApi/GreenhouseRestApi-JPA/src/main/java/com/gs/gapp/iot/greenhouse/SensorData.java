package com.gs.gapp.iot.greenhouse;


import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EntityListeners;
import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.lang.Object;
import java.lang.Override;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import java.lang.Long;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Embedded;

/**
 * Holds all collected measurement data.
 */
//DA-START:greenhouse.SensorData:DA-START
//DA-ELSE:greenhouse.SensorData:DA-ELSE
@Entity
@Table(name="sensordata")
@EntityListeners(value={com.gs.gapp.iot.greenhouse.SensorDataLifecycleListener.class})
//DA-END:greenhouse.SensorData:DA-END

public class SensorData extends BaseEntity implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    /**
     * Identifies a group of devices and users. It can be seen
     * as partition of the MQTT topic namespace.
     */
    private String accountName;
    
    /**
     * Identifies a single gateway device within an
     * account (typically the MAC address of a
     * gateway???s primary network interface).
     * The client_id maps to the Client Identifier (Client ID)
     * as defined in the MQTT specifications.
     */
    private String clientId;
    
    /**
     * Identifies an application running on the gateway device.
     * To support multiple versions of the application, it is recommended
     * that a version number be assigned with the app_id (e.g., ???CONF-V1???,
     * ???CONF-V2???, etc.)
     */
    private String appId;
    
    /**
     * Identifies a resource(s) that is owned and managed by a particular application.
     * Management of resources (e.g., sensors, actuators, local files, or configuration options)
     * includes listing them, reading the latest value, or updating them to a new value.
     * A resource_id may be a hierarchical topic, where, for example, ???sensors/temp??? may
     * identify a temperature sensor and ???sensor/hum??? a humidity sensor.
     */
    private String resourceId;
    
    /**
     * The measured value in the unit.
     */
    private String value;
    
    private Unit unit;
    
    /**
     * The time, when the measurement took place. Note that
     * this is not the time when the database entry has been
     * made.
     */
    private Date timeOfMeasurement;
    
    private String timeZone;
    
    /**
     * The location, where the measaurement took place (optional).
     */
    private GeoCoordinates geocoordinates;
    
    private Sensor sensor;
    
    private SensorInstance sensorInstance;
    
    /**
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.hashCode.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.hashCode.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.hashCode.annotations:DA-END
    public int hashCode() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.hashCode.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.hashCode.int:DA-ELSE
        int result = super.hashCode();
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.equals.Object.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.equals.Object.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.equals.Object.annotations:DA-END
    public boolean equals(Object arg0) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.equals.Object.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.equals.Object.boolean:DA-ELSE
        if (this == arg0)
            return true;
        if (!super.equals(arg0))
            return false;
        if (getClass() != arg0.getClass())
            return false;
        return true;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.equals.Object.boolean:DA-END
    }
    /**
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getPk.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getPk.annotations:DA-ELSE
    @Override
    @Id
    @GeneratedValue(generator="sensordata_seq", strategy=GenerationType.AUTO)
    @SequenceGenerator(name="sensordata_seq", sequenceName="sensordata_seq", initialValue=1, allocationSize=1)
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getPk.annotations:DA-END
    public Long getPk() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getPk.Long:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getPk.Long:DA-ELSE
        return super.getPk();
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getPk.Long:DA-END
    }
    /**
     * getter for the field accountName
     * 
     * 
     * Identifies a group of devices and users. It can be seen
     * as partition of the MQTT topic namespace.
     * 
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getAccountName.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getAccountName.annotations:DA-ELSE
    @Basic
    @Column(name="accountname", unique=false, nullable=true)
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getAccountName.annotations:DA-END
    public String getAccountName() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getAccountName.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getAccountName.String:DA-ELSE
        return this.accountName;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getAccountName.String:DA-END
    }
    /**
     * setter for the field accountName
     * 
     * 
     * Identifies a group of devices and users. It can be seen
     * as partition of the MQTT topic namespace.
     * 
     * 
     * @param accountName  the accountName
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setAccountName.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setAccountName.String.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setAccountName.String.annotations:DA-END
    public void setAccountName(String accountName) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setAccountName.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setAccountName.String:DA-ELSE
        this.accountName = accountName;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setAccountName.String:DA-END
    }
    /**
     * getter for the field clientId
     * 
     * 
     * Identifies a single gateway device within an
     * account (typically the MAC address of a
     * gateway???s primary network interface).
     * The client_id maps to the Client Identifier (Client ID)
     * as defined in the MQTT specifications.
     * 
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getClientId.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getClientId.annotations:DA-ELSE
    @Basic
    @Column(name="clientid", unique=false, nullable=true)
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getClientId.annotations:DA-END
    public String getClientId() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getClientId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getClientId.String:DA-ELSE
        return this.clientId;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getClientId.String:DA-END
    }
    /**
     * setter for the field clientId
     * 
     * 
     * Identifies a single gateway device within an
     * account (typically the MAC address of a
     * gateway???s primary network interface).
     * The client_id maps to the Client Identifier (Client ID)
     * as defined in the MQTT specifications.
     * 
     * 
     * @param clientId  the clientId
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setClientId.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setClientId.String.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setClientId.String.annotations:DA-END
    public void setClientId(String clientId) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setClientId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setClientId.String:DA-ELSE
        this.clientId = clientId;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setClientId.String:DA-END
    }
    /**
     * getter for the field appId
     * 
     * 
     * Identifies an application running on the gateway device.
     * To support multiple versions of the application, it is recommended
     * that a version number be assigned with the app_id (e.g., ???CONF-V1???,
     * ???CONF-V2???, etc.)
     * 
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getAppId.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getAppId.annotations:DA-ELSE
    @Basic
    @Column(name="appid", unique=false, nullable=true)
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getAppId.annotations:DA-END
    public String getAppId() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getAppId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getAppId.String:DA-ELSE
        return this.appId;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getAppId.String:DA-END
    }
    /**
     * setter for the field appId
     * 
     * 
     * Identifies an application running on the gateway device.
     * To support multiple versions of the application, it is recommended
     * that a version number be assigned with the app_id (e.g., ???CONF-V1???,
     * ???CONF-V2???, etc.)
     * 
     * 
     * @param appId  the appId
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setAppId.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setAppId.String.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setAppId.String.annotations:DA-END
    public void setAppId(String appId) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setAppId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setAppId.String:DA-ELSE
        this.appId = appId;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setAppId.String:DA-END
    }
    /**
     * getter for the field resourceId
     * 
     * 
     * Identifies a resource(s) that is owned and managed by a particular application.
     * Management of resources (e.g., sensors, actuators, local files, or configuration options)
     * includes listing them, reading the latest value, or updating them to a new value.
     * A resource_id may be a hierarchical topic, where, for example, ???sensors/temp??? may
     * identify a temperature sensor and ???sensor/hum??? a humidity sensor.
     * 
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getResourceId.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getResourceId.annotations:DA-ELSE
    @Basic
    @Column(name="resourceid", unique=false, nullable=true)
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getResourceId.annotations:DA-END
    public String getResourceId() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getResourceId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getResourceId.String:DA-ELSE
        return this.resourceId;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getResourceId.String:DA-END
    }
    /**
     * setter for the field resourceId
     * 
     * 
     * Identifies a resource(s) that is owned and managed by a particular application.
     * Management of resources (e.g., sensors, actuators, local files, or configuration options)
     * includes listing them, reading the latest value, or updating them to a new value.
     * A resource_id may be a hierarchical topic, where, for example, ???sensors/temp??? may
     * identify a temperature sensor and ???sensor/hum??? a humidity sensor.
     * 
     * 
     * @param resourceId  the resourceId
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setResourceId.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setResourceId.String.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setResourceId.String.annotations:DA-END
    public void setResourceId(String resourceId) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setResourceId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setResourceId.String:DA-ELSE
        this.resourceId = resourceId;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setResourceId.String:DA-END
    }
    /**
     * getter for the field value
     * 
     * 
     * The measured value in the unit.
     * 
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getValue.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getValue.annotations:DA-ELSE
    @Basic
    @Column(name="value", unique=false, nullable=true)
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getValue.annotations:DA-END
    public String getValue() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getValue.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getValue.String:DA-ELSE
        return this.value;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getValue.String:DA-END
    }
    /**
     * setter for the field value
     * 
     * 
     * The measured value in the unit.
     * 
     * 
     * @param value  the value
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setValue.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setValue.String.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setValue.String.annotations:DA-END
    public void setValue(String value) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setValue.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setValue.String:DA-ELSE
        this.value = value;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setValue.String:DA-END
    }
    /**
     * getter for the field unit
     * 
     * null
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getUnit.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getUnit.annotations:DA-ELSE
    @ManyToOne(fetch=FetchType.LAZY, optional=true)
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getUnit.annotations:DA-END
    public Unit getUnit() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getUnit.Unit:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getUnit.Unit:DA-ELSE
        return this.unit;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getUnit.Unit:DA-END
    }
    /**
     * setter for the field unit
     * 
     * null
     * 
     * @param unit  the unit
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setUnit.Unit.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setUnit.Unit.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setUnit.Unit.annotations:DA-END
    public void setUnit(Unit unit) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setUnit.Unit:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setUnit.Unit:DA-ELSE
        this.unit = unit;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setUnit.Unit:DA-END
    }
    /**
     * getter for the field timeOfMeasurement
     * 
     * 
     * The time, when the measurement took place. Note that
     * this is not the time when the database entry has been
     * made.
     * 
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getTimeOfMeasurement.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getTimeOfMeasurement.annotations:DA-ELSE
    @Basic
    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name="timeofmeasurement", unique=false, nullable=true)
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getTimeOfMeasurement.annotations:DA-END
    public Date getTimeOfMeasurement() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getTimeOfMeasurement.Date:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getTimeOfMeasurement.Date:DA-ELSE
        return this.timeOfMeasurement;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getTimeOfMeasurement.Date:DA-END
    }
    /**
     * setter for the field timeOfMeasurement
     * 
     * 
     * The time, when the measurement took place. Note that
     * this is not the time when the database entry has been
     * made.
     * 
     * 
     * @param timeOfMeasurement  the timeOfMeasurement
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setTimeOfMeasurement.Date.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setTimeOfMeasurement.Date.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setTimeOfMeasurement.Date.annotations:DA-END
    public void setTimeOfMeasurement(Date timeOfMeasurement) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setTimeOfMeasurement.Date:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setTimeOfMeasurement.Date:DA-ELSE
        this.timeOfMeasurement = timeOfMeasurement;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setTimeOfMeasurement.Date:DA-END
    }
    /**
     * getter for the field timeZone
     * 
     * null
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getTimeZone.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getTimeZone.annotations:DA-ELSE
    @Basic
    @Column(name="timezone", unique=false, nullable=true)
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getTimeZone.annotations:DA-END
    public String getTimeZone() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getTimeZone.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getTimeZone.String:DA-ELSE
        return this.timeZone;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getTimeZone.String:DA-END
    }
    /**
     * setter for the field timeZone
     * 
     * null
     * 
     * @param timeZone  the timeZone
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setTimeZone.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setTimeZone.String.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setTimeZone.String.annotations:DA-END
    public void setTimeZone(String timeZone) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setTimeZone.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setTimeZone.String:DA-ELSE
        this.timeZone = timeZone;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setTimeZone.String:DA-END
    }
    /**
     * getter for the field geocoordinates
     * 
     * 
     * The location, where the measaurement took place (optional).
     * 
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getGeocoordinates.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getGeocoordinates.annotations:DA-ELSE
    @Embedded
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getGeocoordinates.annotations:DA-END
    public GeoCoordinates getGeocoordinates() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getGeocoordinates.GeoCoordinates:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getGeocoordinates.GeoCoordinates:DA-ELSE
        return this.geocoordinates;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getGeocoordinates.GeoCoordinates:DA-END
    }
    /**
     * setter for the field geocoordinates
     * 
     * 
     * The location, where the measaurement took place (optional).
     * 
     * 
     * @param geocoordinates  the geocoordinates
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setGeocoordinates.GeoCoordinates.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setGeocoordinates.GeoCoordinates.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setGeocoordinates.GeoCoordinates.annotations:DA-END
    public void setGeocoordinates(GeoCoordinates geocoordinates) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setGeocoordinates.GeoCoordinates:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setGeocoordinates.GeoCoordinates:DA-ELSE
        this.geocoordinates = geocoordinates;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setGeocoordinates.GeoCoordinates:DA-END
    }
    /**
     * getter for the field sensor
     * 
     * null
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getSensor.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getSensor.annotations:DA-ELSE
    @ManyToOne(fetch=FetchType.LAZY, optional=true)
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getSensor.annotations:DA-END
    public Sensor getSensor() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getSensor.Sensor:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getSensor.Sensor:DA-ELSE
        return this.sensor;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getSensor.Sensor:DA-END
    }
    /**
     * setter for the field sensor
     * 
     * null
     * 
     * @param sensor  the sensor
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setSensor.Sensor.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setSensor.Sensor.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setSensor.Sensor.annotations:DA-END
    public void setSensor(Sensor sensor) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setSensor.Sensor:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setSensor.Sensor:DA-ELSE
        this.sensor = sensor;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setSensor.Sensor:DA-END
    }
    /**
     * getter for the field sensorInstance
     * 
     * null
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getSensorInstance.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getSensorInstance.annotations:DA-ELSE
    @ManyToOne(fetch=FetchType.LAZY, optional=true)
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getSensorInstance.annotations:DA-END
    public SensorInstance getSensorInstance() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.getSensorInstance.SensorInstance:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.getSensorInstance.SensorInstance:DA-ELSE
        return this.sensorInstance;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.getSensorInstance.SensorInstance:DA-END
    }
    /**
     * setter for the field sensorInstance
     * 
     * null
     * 
     * @param sensorInstance  the sensorInstance
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setSensorInstance.SensorInstance.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setSensorInstance.SensorInstance.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setSensorInstance.SensorInstance.annotations:DA-END
    public void setSensorInstance(SensorInstance sensorInstance) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorData.setSensorInstance.SensorInstance:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.setSensorInstance.SensorInstance:DA-ELSE
        this.sensorInstance = sensorInstance;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorData.setSensorInstance.SensorInstance:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData.additional.elements.in.type:DA-END
} // end of java type