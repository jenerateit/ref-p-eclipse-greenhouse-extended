package com.gs.gapp.iot.greenhouse;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;
import java.lang.String;

/**
 * JPA Metamodel description for {@link com.gs.gapp.iot.greenhouse.SensorInstance}.
 * 
 * A sensor instance relates to a real, physical sensor.
 * When for instance 10 identical sensors are being used,
 * there will be 10 entries for them in the SENSORINSTANCE table.
 * 
 * @see com.gs.gapp.iot.greenhouse.SensorInstance
 */
@StaticMetamodel(value=com.gs.gapp.iot.greenhouse.SensorInstance.class)
public class SensorInstance_ extends BaseEntity_ { // start of class

    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.SensorInstance#getId()}.
     * 
     * @see com.gs.gapp.iot.greenhouse.SensorInstance#getId()
     */
    public static volatile SingularAttribute<SensorInstance, String> id;
    
    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.SensorInstance#getName()}.
     * 
     * @see com.gs.gapp.iot.greenhouse.SensorInstance#getName()
     */
    public static volatile SingularAttribute<SensorInstance, String> name;
    
    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.SensorInstance#getSensor()}.
     * 
     * @see com.gs.gapp.iot.greenhouse.SensorInstance#getSensor()
     */
    public static volatile SingularAttribute<SensorInstance, Sensor> sensor;
    
    
    //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance_.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance_.additional.elements.in.type:DA-END
} // end of java type