package com.gs.gapp.iot.greenhouse;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;
import java.lang.String;
import java.util.Date;

/**
 * JPA Metamodel description for {@link com.gs.gapp.iot.greenhouse.SensorData}.
 * 
 * Holds all collected measurement data.
 * 
 * @see com.gs.gapp.iot.greenhouse.SensorData
 */
@StaticMetamodel(value=com.gs.gapp.iot.greenhouse.SensorData.class)
public class SensorData_ extends BaseEntity_ { // start of class

    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.SensorData#getAccountName()}.
     * 
     * Identifies a group of devices and users. It can be seen
     * as partition of the MQTT topic namespace.
     * 
     * @see com.gs.gapp.iot.greenhouse.SensorData#getAccountName()
     */
    public static volatile SingularAttribute<SensorData, String> accountName;
    
    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.SensorData#getClientId()}.
     * 
     * Identifies a single gateway device within an
     * account (typically the MAC address of a
     * gateway???s primary network interface).
     * The client_id maps to the Client Identifier (Client ID)
     * as defined in the MQTT specifications.
     * 
     * @see com.gs.gapp.iot.greenhouse.SensorData#getClientId()
     */
    public static volatile SingularAttribute<SensorData, String> clientId;
    
    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.SensorData#getAppId()}.
     * 
     * Identifies an application running on the gateway device.
     * To support multiple versions of the application, it is recommended
     * that a version number be assigned with the app_id (e.g., ???CONF-V1???,
     * ???CONF-V2???, etc.)
     * 
     * @see com.gs.gapp.iot.greenhouse.SensorData#getAppId()
     */
    public static volatile SingularAttribute<SensorData, String> appId;
    
    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.SensorData#getResourceId()}.
     * 
     * Identifies a resource(s) that is owned and managed by a particular application.
     * Management of resources (e.g., sensors, actuators, local files, or configuration options)
     * includes listing them, reading the latest value, or updating them to a new value.
     * A resource_id may be a hierarchical topic, where, for example, ???sensors/temp??? may
     * identify a temperature sensor and ???sensor/hum??? a humidity sensor.
     * 
     * @see com.gs.gapp.iot.greenhouse.SensorData#getResourceId()
     */
    public static volatile SingularAttribute<SensorData, String> resourceId;
    
    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.SensorData#getValue()}.
     * 
     * The measured value in the unit.
     * 
     * @see com.gs.gapp.iot.greenhouse.SensorData#getValue()
     */
    public static volatile SingularAttribute<SensorData, String> value;
    
    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.SensorData#getUnit()}.
     * 
     * @see com.gs.gapp.iot.greenhouse.SensorData#getUnit()
     */
    public static volatile SingularAttribute<SensorData, Unit> unit;
    
    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.SensorData#getTimeOfMeasurement()}.
     * 
     * The time, when the measurement took place. Note that
     * this is not the time when the database entry has been
     * made.
     * 
     * @see com.gs.gapp.iot.greenhouse.SensorData#getTimeOfMeasurement()
     */
    public static volatile SingularAttribute<SensorData, Date> timeOfMeasurement;
    
    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.SensorData#getTimeZone()}.
     * 
     * @see com.gs.gapp.iot.greenhouse.SensorData#getTimeZone()
     */
    public static volatile SingularAttribute<SensorData, String> timeZone;
    
    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.SensorData#getGeocoordinates()}.
     * 
     * The location, where the measaurement took place (optional).
     * 
     * @see com.gs.gapp.iot.greenhouse.SensorData#getGeocoordinates()
     */
    public static volatile SingularAttribute<SensorData, GeoCoordinates> geocoordinates;
    
    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.SensorData#getSensor()}.
     * 
     * @see com.gs.gapp.iot.greenhouse.SensorData#getSensor()
     */
    public static volatile SingularAttribute<SensorData, Sensor> sensor;
    
    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.SensorData#getSensorInstance()}.
     * 
     * @see com.gs.gapp.iot.greenhouse.SensorData#getSensorInstance()
     */
    public static volatile SingularAttribute<SensorData, SensorInstance> sensorInstance;
    
    
    //DA-START:com.gs.gapp.iot.greenhouse.SensorData_.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorData_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.SensorData_.additional.elements.in.type:DA-END
} // end of java type