package com.gs.gapp.iot.greenhouse;


import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EntityListeners;
import java.io.Serializable;
import java.lang.String;
import java.lang.Object;
import java.lang.Override;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import java.lang.Long;
import javax.persistence.Basic;
import javax.persistence.Column;

//DA-START:greenhouse.Unit:DA-START
//DA-ELSE:greenhouse.Unit:DA-ELSE
@Entity
@Table(name="unit")
@EntityListeners(value={com.gs.gapp.iot.greenhouse.UnitLifecycleListener.class})
//DA-END:greenhouse.Unit:DA-END

public class Unit extends BaseEntity implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private String name;
    
    /**
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.Unit.hashCode.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.Unit.hashCode.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.Unit.hashCode.annotations:DA-END
    public int hashCode() {
        //DA-START:com.gs.gapp.iot.greenhouse.Unit.hashCode.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.Unit.hashCode.int:DA-ELSE
        int result = super.hashCode();
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.Unit.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.Unit.equals.Object.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.Unit.equals.Object.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.Unit.equals.Object.annotations:DA-END
    public boolean equals(Object arg0) {
        //DA-START:com.gs.gapp.iot.greenhouse.Unit.equals.Object.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.Unit.equals.Object.boolean:DA-ELSE
        if (this == arg0)
            return true;
        if (!super.equals(arg0))
            return false;
        if (getClass() != arg0.getClass())
            return false;
        return true;
        //DA-END:com.gs.gapp.iot.greenhouse.Unit.equals.Object.boolean:DA-END
    }
    /**
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.Unit.getPk.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.Unit.getPk.annotations:DA-ELSE
    @Override
    @Id
    @GeneratedValue(generator="unit_seq", strategy=GenerationType.AUTO)
    @SequenceGenerator(name="unit_seq", sequenceName="unit_seq", initialValue=1, allocationSize=1)
    //DA-END:com.gs.gapp.iot.greenhouse.Unit.getPk.annotations:DA-END
    public Long getPk() {
        //DA-START:com.gs.gapp.iot.greenhouse.Unit.getPk.Long:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.Unit.getPk.Long:DA-ELSE
        return super.getPk();
        //DA-END:com.gs.gapp.iot.greenhouse.Unit.getPk.Long:DA-END
    }
    /**
     * getter for the field name
     * 
     * null
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.Unit.getName.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.Unit.getName.annotations:DA-ELSE
    @Basic
    @Column(name="name", unique=false, nullable=true)
    //DA-END:com.gs.gapp.iot.greenhouse.Unit.getName.annotations:DA-END
    public String getName() {
        //DA-START:com.gs.gapp.iot.greenhouse.Unit.getName.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.Unit.getName.String:DA-ELSE
        return this.name;
        //DA-END:com.gs.gapp.iot.greenhouse.Unit.getName.String:DA-END
    }
    /**
     * setter for the field name
     * 
     * null
     * 
     * @param name  the name
     */
    //DA-START:com.gs.gapp.iot.greenhouse.Unit.setName.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.Unit.setName.String.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.Unit.setName.String.annotations:DA-END
    public void setName(String name) {
        //DA-START:com.gs.gapp.iot.greenhouse.Unit.setName.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.Unit.setName.String:DA-ELSE
        this.name = name;
        //DA-END:com.gs.gapp.iot.greenhouse.Unit.setName.String:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.Unit.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.Unit.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.Unit.additional.elements.in.type:DA-END
} // end of java type