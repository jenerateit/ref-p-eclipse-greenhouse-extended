package com.gs.gapp.iot.greenhouse;


import javax.persistence.PreUpdate;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PostRemove;
import javax.persistence.PostLoad;

/**
 * This entity is the parent of all entities that are defined for this sample.
 * It defines fields that all entities need. In our case here it is the primary key.
 */
public class BaseEntityLifecycleListener { // start of class

    /**
     * 
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(BaseEntity entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.preUpdate.BaseEntity:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.preUpdate.BaseEntity:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.preUpdate.BaseEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(BaseEntity entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.postUpdate.BaseEntity:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.postUpdate.BaseEntity:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.postUpdate.BaseEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(BaseEntity entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.prePersist.BaseEntity:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.prePersist.BaseEntity:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.prePersist.BaseEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(BaseEntity entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.postPersist.BaseEntity:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.postPersist.BaseEntity:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.postPersist.BaseEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(BaseEntity entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.preRemove.BaseEntity:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.preRemove.BaseEntity:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.preRemove.BaseEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(BaseEntity entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.postRemove.BaseEntity:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.postRemove.BaseEntity:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.postRemove.BaseEntity:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(BaseEntity entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.postLoad.BaseEntity:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.postLoad.BaseEntity:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.postLoad.BaseEntity:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.BaseEntityLifecycleListener.additional.elements.in.type:DA-END
} // end of java type