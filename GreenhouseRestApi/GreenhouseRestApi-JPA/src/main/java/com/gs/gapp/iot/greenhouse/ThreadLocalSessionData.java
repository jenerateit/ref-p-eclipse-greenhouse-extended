package com.gs.gapp.iot.greenhouse;


import java.lang.ThreadLocal;
import java.lang.String;

public class ThreadLocalSessionData { // start of class

    private static ThreadLocal<ThreadLocalSessionData.SessionData> sessionData = new ThreadLocal<ThreadLocalSessionData.SessionData>();
    
    /**
     * 
     * @param sessionData  the sessionData
     */
    public static void setSessionData(ThreadLocalSessionData.SessionData sessionData) {
        //DA-START:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.setSessionData.SessionData:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.setSessionData.SessionData:DA-ELSE
        ThreadLocalSessionData.sessionData.set(sessionData);
        //DA-END:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.setSessionData.SessionData:DA-END
    }
    /**
     * 
     * @return
     */
    public static ThreadLocalSessionData.SessionData getSessionData() {
        //DA-START:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.getSessionData.SessionData:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.getSessionData.SessionData:DA-ELSE
        return sessionData.get();
        //DA-END:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.getSessionData.SessionData:DA-END
    }
    
    public static class SessionData { // start of class
    
    
        private String id;
        
        
        
        /**
         * getter for the field id
         * 
         * null
         * 
         * @return
         */
        public String getId() {
            //DA-START:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.SessionData.getId.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.SessionData.getId.String:DA-ELSE
            return this.id;
            //DA-END:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.SessionData.getId.String:DA-END
        }
        /**
         * setter for the field id
         * 
         * null
         * 
         * @param id  the id
         */
        public void setId(String id) {
            //DA-START:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.SessionData.setId.String:DA-START
            //DA-ELSE:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.SessionData.setId.String:DA-ELSE
            this.id = id;
            //DA-END:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.SessionData.setId.String:DA-END
        }
        
        
        //DA-START:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.SessionData.additional.elements.in.type:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.SessionData.additional.elements.in.type:DA-ELSE
        // add any additional element in here that you need in your Java type but is not generated 
        //DA-END:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.SessionData.additional.elements.in.type:DA-END
    } // end of java type
    
    
    //DA-START:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.ThreadLocalSessionData.additional.elements.in.type:DA-END
} // end of java type