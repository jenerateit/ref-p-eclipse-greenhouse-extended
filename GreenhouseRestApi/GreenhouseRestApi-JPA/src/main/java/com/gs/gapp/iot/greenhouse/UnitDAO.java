package com.gs.gapp.iot.greenhouse;


import javax.persistence.EntityManager;
import java.lang.Object;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

public class UnitDAO { // start of class

    /**
     * 
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static Unit get(EntityManager entityManager, Object id) {
        //DA-START:com.gs.gapp.iot.greenhouse.UnitDAO.get.EntityManager.Object.Unit:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.UnitDAO.get.EntityManager.Object.Unit:DA-ELSE
        return entityManager.find(Unit.class, id);
        //DA-END:com.gs.gapp.iot.greenhouse.UnitDAO.get.EntityManager.Object.Unit:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<Unit> getAll(EntityManager entityManager) {
        //DA-START:com.gs.gapp.iot.greenhouse.UnitDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.UnitDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<Unit> cq = cb.createQuery(Unit.class);
        cq.from(Unit.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:com.gs.gapp.iot.greenhouse.UnitDAO.getAll.EntityManager.Collection:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static Unit create(EntityManager entityManager, Unit entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.UnitDAO.create.EntityManager.Unit.Unit:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.UnitDAO.create.EntityManager.Unit.Unit:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:com.gs.gapp.iot.greenhouse.UnitDAO.create.EntityManager.Unit.Unit:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static Unit update(EntityManager entityManager, Unit entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.UnitDAO.update.EntityManager.Unit.Unit:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.UnitDAO.update.EntityManager.Unit.Unit:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:com.gs.gapp.iot.greenhouse.UnitDAO.update.EntityManager.Unit.Unit:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, Unit entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.UnitDAO.delete.EntityManager.Unit:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.UnitDAO.delete.EntityManager.Unit:DA-ELSE
        entityManager.remove(entity);
        //DA-END:com.gs.gapp.iot.greenhouse.UnitDAO.delete.EntityManager.Unit:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.UnitDAO.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.UnitDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.UnitDAO.additional.elements.in.type:DA-END
} // end of java type