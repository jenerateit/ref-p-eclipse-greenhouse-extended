package com.gs.gapp.iot.greenhouse;


import javax.persistence.metamodel.StaticMetamodel;
import javax.persistence.metamodel.SingularAttribute;
import java.lang.String;

/**
 * JPA Metamodel description for {@link com.gs.gapp.iot.greenhouse.Unit}.
 * 
 * @see com.gs.gapp.iot.greenhouse.Unit
 */
@StaticMetamodel(value=com.gs.gapp.iot.greenhouse.Unit.class)
public class Unit_ extends BaseEntity_ { // start of class

    /**
     * JPA Metamodel mapping for {@link com.gs.gapp.iot.greenhouse.Unit#getName()}.
     * 
     * @see com.gs.gapp.iot.greenhouse.Unit#getName()
     */
    public static volatile SingularAttribute<Unit, String> name;
    
    
    //DA-START:com.gs.gapp.iot.greenhouse.Unit_.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.Unit_.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.Unit_.additional.elements.in.type:DA-END
} // end of java type