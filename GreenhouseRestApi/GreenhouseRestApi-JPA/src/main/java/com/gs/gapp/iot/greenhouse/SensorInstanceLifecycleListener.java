package com.gs.gapp.iot.greenhouse;


import javax.persistence.PreUpdate;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PostRemove;
import javax.persistence.PostLoad;

/**
 * A sensor instance relates to a real, physical sensor.
 * When for instance 10 identical sensors are being used,
 * there will be 10 entries for them in the SENSORINSTANCE table.
 */
public class SensorInstanceLifecycleListener { // start of class

    /**
     * 
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(SensorInstance entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.preUpdate.SensorInstance:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.preUpdate.SensorInstance:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.preUpdate.SensorInstance:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(SensorInstance entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.postUpdate.SensorInstance:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.postUpdate.SensorInstance:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.postUpdate.SensorInstance:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(SensorInstance entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.prePersist.SensorInstance:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.prePersist.SensorInstance:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.prePersist.SensorInstance:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(SensorInstance entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.postPersist.SensorInstance:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.postPersist.SensorInstance:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.postPersist.SensorInstance:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(SensorInstance entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.preRemove.SensorInstance:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.preRemove.SensorInstance:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.preRemove.SensorInstance:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(SensorInstance entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.postRemove.SensorInstance:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.postRemove.SensorInstance:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.postRemove.SensorInstance:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(SensorInstance entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.postLoad.SensorInstance:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.postLoad.SensorInstance:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.postLoad.SensorInstance:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.additional.elements.in.type:DA-END
} // end of java type