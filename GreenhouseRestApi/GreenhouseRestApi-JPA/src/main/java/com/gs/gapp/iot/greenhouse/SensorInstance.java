package com.gs.gapp.iot.greenhouse;


import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EntityListeners;
import java.io.Serializable;
import java.lang.String;
import java.lang.Object;
import java.lang.Override;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import java.lang.Long;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;

/**
 * A sensor instance relates to a real, physical sensor.
 * When for instance 10 identical sensors are being used,
 * there will be 10 entries for them in the SENSORINSTANCE table.
 */
//DA-START:greenhouse.SensorInstance:DA-START
//DA-ELSE:greenhouse.SensorInstance:DA-ELSE
@Entity
@Table(name="sensorinstance")
@EntityListeners(value={com.gs.gapp.iot.greenhouse.SensorInstanceLifecycleListener.class})
//DA-END:greenhouse.SensorInstance:DA-END

public class SensorInstance extends BaseEntity implements Serializable { // start of class

    private static final long serialVersionUID = 1L;
    
    private String id;
    
    private String name;
    
    private Sensor sensor;
    
    /**
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.hashCode.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.hashCode.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.hashCode.annotations:DA-END
    public int hashCode() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.hashCode.int:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.hashCode.int:DA-ELSE
        int result = super.hashCode();
        return result;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.hashCode.int:DA-END
    }
    /**
     * 
     * @param arg0  the arg0
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.equals.Object.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.equals.Object.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.equals.Object.annotations:DA-END
    public boolean equals(Object arg0) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.equals.Object.boolean:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.equals.Object.boolean:DA-ELSE
        if (this == arg0)
            return true;
        if (!super.equals(arg0))
            return false;
        if (getClass() != arg0.getClass())
            return false;
        return true;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.equals.Object.boolean:DA-END
    }
    /**
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.getPk.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.getPk.annotations:DA-ELSE
    @Override
    @Id
    @GeneratedValue(generator="sensorinstance_seq", strategy=GenerationType.AUTO)
    @SequenceGenerator(name="sensorinstance_seq", sequenceName="sensorinstance_seq", initialValue=1, allocationSize=1)
    //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.getPk.annotations:DA-END
    public Long getPk() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.getPk.Long:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.getPk.Long:DA-ELSE
        return super.getPk();
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.getPk.Long:DA-END
    }
    /**
     * getter for the field id
     * 
     * null
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.getId.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.getId.annotations:DA-ELSE
    @Basic
    @Column(name="id", unique=false, nullable=true)
    //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.getId.annotations:DA-END
    public String getId() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.getId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.getId.String:DA-ELSE
        return this.id;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.getId.String:DA-END
    }
    /**
     * setter for the field id
     * 
     * null
     * 
     * @param id  the id
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.setId.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.setId.String.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.setId.String.annotations:DA-END
    public void setId(String id) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.setId.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.setId.String:DA-ELSE
        this.id = id;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.setId.String:DA-END
    }
    /**
     * getter for the field name
     * 
     * null
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.getName.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.getName.annotations:DA-ELSE
    @Basic
    @Column(name="name", unique=false, nullable=true)
    //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.getName.annotations:DA-END
    public String getName() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.getName.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.getName.String:DA-ELSE
        return this.name;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.getName.String:DA-END
    }
    /**
     * setter for the field name
     * 
     * null
     * 
     * @param name  the name
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.setName.String.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.setName.String.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.setName.String.annotations:DA-END
    public void setName(String name) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.setName.String:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.setName.String:DA-ELSE
        this.name = name;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.setName.String:DA-END
    }
    /**
     * getter for the field sensor
     * 
     * null
     * 
     * @return
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.getSensor.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.getSensor.annotations:DA-ELSE
    @ManyToOne(fetch=FetchType.LAZY, optional=true)
    //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.getSensor.annotations:DA-END
    public Sensor getSensor() {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.getSensor.Sensor:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.getSensor.Sensor:DA-ELSE
        return this.sensor;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.getSensor.Sensor:DA-END
    }
    /**
     * setter for the field sensor
     * 
     * null
     * 
     * @param sensor  the sensor
     */
    //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.setSensor.Sensor.annotations:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.setSensor.Sensor.annotations:DA-ELSE
    //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.setSensor.Sensor.annotations:DA-END
    public void setSensor(Sensor sensor) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.setSensor.Sensor:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.setSensor.Sensor:DA-ELSE
        this.sensor = sensor;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.setSensor.Sensor:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.SensorInstance.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorInstance.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.SensorInstance.additional.elements.in.type:DA-END
} // end of java type