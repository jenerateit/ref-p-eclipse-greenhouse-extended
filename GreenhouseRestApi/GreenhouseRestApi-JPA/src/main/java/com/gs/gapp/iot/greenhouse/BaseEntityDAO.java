package com.gs.gapp.iot.greenhouse;


import javax.persistence.EntityManager;
import java.lang.Object;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

/**
 * This entity is the parent of all entities that are defined for this sample.
 * It defines fields that all entities need. In our case here it is the primary key.
 */
public class BaseEntityDAO { // start of class

    /**
     * 
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static BaseEntity get(EntityManager entityManager, Object id) {
        //DA-START:com.gs.gapp.iot.greenhouse.BaseEntityDAO.get.EntityManager.Object.BaseEntity:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntityDAO.get.EntityManager.Object.BaseEntity:DA-ELSE
        return entityManager.find(BaseEntity.class, id);
        //DA-END:com.gs.gapp.iot.greenhouse.BaseEntityDAO.get.EntityManager.Object.BaseEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<BaseEntity> getAll(EntityManager entityManager) {
        //DA-START:com.gs.gapp.iot.greenhouse.BaseEntityDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntityDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<BaseEntity> cq = cb.createQuery(BaseEntity.class);
        cq.from(BaseEntity.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:com.gs.gapp.iot.greenhouse.BaseEntityDAO.getAll.EntityManager.Collection:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static BaseEntity create(EntityManager entityManager, BaseEntity entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.BaseEntityDAO.create.EntityManager.BaseEntity.BaseEntity:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntityDAO.create.EntityManager.BaseEntity.BaseEntity:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:com.gs.gapp.iot.greenhouse.BaseEntityDAO.create.EntityManager.BaseEntity.BaseEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static BaseEntity update(EntityManager entityManager, BaseEntity entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.BaseEntityDAO.update.EntityManager.BaseEntity.BaseEntity:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntityDAO.update.EntityManager.BaseEntity.BaseEntity:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:com.gs.gapp.iot.greenhouse.BaseEntityDAO.update.EntityManager.BaseEntity.BaseEntity:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, BaseEntity entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.BaseEntityDAO.delete.EntityManager.BaseEntity:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntityDAO.delete.EntityManager.BaseEntity:DA-ELSE
        entityManager.remove(entity);
        //DA-END:com.gs.gapp.iot.greenhouse.BaseEntityDAO.delete.EntityManager.BaseEntity:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.BaseEntityDAO.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.BaseEntityDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.BaseEntityDAO.additional.elements.in.type:DA-END
} // end of java type