package com.gs.gapp.iot.greenhouse;


import javax.persistence.EntityManager;
import java.lang.Object;
import java.util.Collection;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

/**
 * Holds all collected measurement data.
 */
public class SensorDataDAO { // start of class

    /**
     * 
     * @param entityManager  the entityManager
     * @param id  the id
     * @return
     */
    public static SensorData get(EntityManager entityManager, Object id) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorDataDAO.get.EntityManager.Object.SensorData:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDataDAO.get.EntityManager.Object.SensorData:DA-ELSE
        return entityManager.find(SensorData.class, id);
        //DA-END:com.gs.gapp.iot.greenhouse.SensorDataDAO.get.EntityManager.Object.SensorData:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @return
     */
    public static Collection<SensorData> getAll(EntityManager entityManager) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorDataDAO.getAll.EntityManager.Collection:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDataDAO.getAll.EntityManager.Collection:DA-ELSE
        final CriteriaBuilder cb = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        final CriteriaQuery<SensorData> cq = cb.createQuery(SensorData.class);
        cq.from(SensorData.class);
        return entityManager.createQuery(cq).getResultList();
        //DA-END:com.gs.gapp.iot.greenhouse.SensorDataDAO.getAll.EntityManager.Collection:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static SensorData create(EntityManager entityManager, SensorData entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorDataDAO.create.EntityManager.SensorData.SensorData:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDataDAO.create.EntityManager.SensorData.SensorData:DA-ELSE
        entityManager.persist(entity);
        entityManager.flush();  // calling flush() in order to have an id available in the entity object, which is required in the same transaction when converting the entity to a dto
        return entity;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorDataDAO.create.EntityManager.SensorData.SensorData:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     * @return
     */
    public static SensorData update(EntityManager entityManager, SensorData entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorDataDAO.update.EntityManager.SensorData.SensorData:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDataDAO.update.EntityManager.SensorData.SensorData:DA-ELSE
        return entityManager.merge(entity);
        //DA-END:com.gs.gapp.iot.greenhouse.SensorDataDAO.update.EntityManager.SensorData.SensorData:DA-END
    }
    /**
     * 
     * @param entityManager  the entityManager
     * @param entity  the entity
     */
    public static void delete(EntityManager entityManager, SensorData entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorDataDAO.delete.EntityManager.SensorData:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDataDAO.delete.EntityManager.SensorData:DA-ELSE
        entityManager.remove(entity);
        //DA-END:com.gs.gapp.iot.greenhouse.SensorDataDAO.delete.EntityManager.SensorData:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.SensorDataDAO.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorDataDAO.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.SensorDataDAO.additional.elements.in.type:DA-END
} // end of java type