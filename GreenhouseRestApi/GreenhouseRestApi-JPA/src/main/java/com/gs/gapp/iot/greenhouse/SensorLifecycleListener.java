package com.gs.gapp.iot.greenhouse;


import javax.persistence.PreUpdate;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PostPersist;
import javax.persistence.PreRemove;
import javax.persistence.PostRemove;
import javax.persistence.PostLoad;

/**
 * Date that describes a sensor. When for instance 10 idientical sensors
 * are being used, there is only one single entry in the SENSOR table.
 */
public class SensorLifecycleListener { // start of class

    /**
     * 
     * @param entity  the entity
     */
    @PreUpdate
    public void preUpdate(Sensor entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.preUpdate.Sensor:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.preUpdate.Sensor:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.preUpdate.Sensor:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostUpdate
    public void postUpdate(Sensor entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.postUpdate.Sensor:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.postUpdate.Sensor:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.postUpdate.Sensor:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PrePersist
    public void prePersist(Sensor entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.prePersist.Sensor:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.prePersist.Sensor:DA-ELSE
        //DA-END:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.prePersist.Sensor:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostPersist
    public void postPersist(Sensor entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.postPersist.Sensor:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.postPersist.Sensor:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.postPersist.Sensor:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PreRemove
    public void preRemove(Sensor entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.preRemove.Sensor:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.preRemove.Sensor:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.preRemove.Sensor:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostRemove
    public void postRemove(Sensor entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.postRemove.Sensor:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.postRemove.Sensor:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.postRemove.Sensor:DA-END
    }
    /**
     * 
     * @param entity  the entity
     */
    @PostLoad
    public void postLoad(Sensor entity) {
        //DA-START:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.postLoad.Sensor:DA-START
        //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.postLoad.Sensor:DA-ELSE
        return;
        //DA-END:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.postLoad.Sensor:DA-END
    }
    
    //DA-START:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.additional.elements.in.type:DA-START
    //DA-ELSE:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.additional.elements.in.type:DA-ELSE
    // add any additional element in here that you need in your Java type but is not generated 
    //DA-END:com.gs.gapp.iot.greenhouse.SensorLifecycleListener.additional.elements.in.type:DA-END
} // end of java type