@echo off

SET GLASSFISH_HOME=D:\glassfish-4.1\glassfish4
SET PATH=%PATH%;%GLASSFISH_HOME%\bin
SET CREDENTIALS=--user admin --host localhost --port 4848

SET NAME_JDBC_RESOURCE=jdbc/GREENHOUSE
SET NAME_JDBC_RESOURCE_DESCRIPTION=Greenhouse JEE Datasource
SET NAME_JDBC_CONNECTION_POOL=DB_POOL
SET USER=APP
SET PASSWORD=APP
SET DB_NAME=greenhouse


SET DOMAIN=domain1


call asadmin verify-domain-xml %DOMAIN% || goto :error


REM ##########################
REM ###### DELETE ############
call asadmin %CREDENTIALS% delete-jdbc-resource %NAME_JDBC_RESOURCE%  || goto :notdeleted
call asadmin %CREDENTIALS% delete-jdbc-connection-pool %NAME_JDBC_CONNECTION_POOL%  || goto :notdeleted

:notdeleted

REM ##########################
REM ###### CREATE ############
call asadmin %CREDENTIALS% create-jdbc-connection-pool --datasourceclassname org.apache.derby.jdbc.ClientDataSource --restype javax.sql.XADataSource --property connectionAttributes=;create\=true:portNumber=1527:password=%PASSWORD%:user=%USER%:serverName=localhost:databaseName=%DB_NAME%:create=true %NAME_JDBC_CONNECTION_POOL%  || goto :error
call asadmin %CREDENTIALS% create-jdbc-resource --connectionpoolid %NAME_JDBC_CONNECTION_POOL% --description "%NAME_JDBC_RESOURCE_DESCRIPTION%" %NAME_JDBC_RESOURCE%  || goto :error

echo "completed configuration of glassfish"
pause
exit /B 0

:error
echo Failed with error #%errorlevel%.
exit /B %errorlevel%