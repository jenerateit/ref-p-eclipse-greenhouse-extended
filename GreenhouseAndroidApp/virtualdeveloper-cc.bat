@echo off
echo %1 %2

Setlocal EnableDelayedExpansion

set MODEL_DIR=
set FILE_LIST=
set PROJECT_NAME=Eclipse IoT Mobile - Android

for %%F in (%MODEL_DIR%\*.gapp) do (
   set FILE_LIST=!FILE_LIST! %%~dpnxF
   REM echo File-List:%FILE_LIST%
)

for /f "tokens=* delims= " %%a in ("%FILE_LIST%") do set FILE_LIST=%%a

set FILE_LIST=..\GreenhouseModel\TypeAliases.gapp ..\GreenhouseModel\Cockpit\CockpitApplication.gapp ..\GreenhouseModel\REST-API\Persistence.gapp

"%2\bin\java.exe" -jar virtualdeveloper-cc.jar -o "%1\app" -p "%PROJECT_NAME%" -s https://eu-de-001.virtual-developer.com:443 -u gsuser@generative-software.com -w 7c5301f7-f7e2-4e04-94e9-de06014aee3c %FILE_LIST%